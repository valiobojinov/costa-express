package com.estafet.costaexpress.test.tools;

import java.io.File;
import java.io.IOException;

import org.neo4j.graphdb.GraphDatabaseService;
import org.neo4j.graphdb.factory.GraphDatabaseFactory;
import org.neo4j.graphdb.factory.GraphDatabaseSettings;
import org.neo4j.io.fs.FileUtils;

import com.estafet.costaexpress.datasource.entity.IEntityDataSource;
import com.estafet.costaexpress.datasource.policy.IPolicyDataSource;


public class EmbeddedNeo4JDataSource <D extends IEntityDataSource<?, ?, ?>, E extends IPolicyDataSource<?, ?, ?>> {

	public static final String DB_PATH = "neo4j-test";
	public static final Integer DB_PORT = 7686;
	private GraphDatabaseService service;
	
	private D entityInstance;
	private E policyInstance;
	
	public void startDatabase() throws IOException{
		clearDatabase();
		GraphDatabaseSettings.BoltConnector bolt = GraphDatabaseSettings.boltConnector("0");		
		String path = EmbeddedNeo4JDataSource.class.getClassLoader().getResource(".").getPath();
		
		service = new GraphDatabaseFactory().newEmbeddedDatabaseBuilder(new File(path, DB_PATH))
				.setConfig(bolt.type, "BOLT")
				.setConfig(bolt.enabled, "true")
				.setConfig(bolt.address, "localhost:"+DB_PORT.toString())
				.newGraphDatabase();
	}
	
	
	public void shutdownDatabase() throws IOException {
		service.shutdown();
		clearDatabase();
		
	}
	
	private void clearDatabase() throws IOException {
		String path = EmbeddedNeo4JDataSource.class.getClassLoader().getResource(".").getPath();		
		FileUtils.deleteRecursively(new File(path, DB_PATH));
	}
	
	public Integer getDatabasePort() {
		return DB_PORT;
	}


	public D getEntityInstance() {
		return entityInstance;
	}


	public void setEntityInstance(D entityInstance) {
		this.entityInstance = entityInstance;
	}


	public E getPolicyInstance() {
		return policyInstance;
	}


	public void setPolicyInstance(E policyInstance) {
		this.policyInstance = policyInstance;
	}
}
