package com.estafet.rest;

import javax.ws.rs.ApplicationPath;

import org.glassfish.jersey.server.ResourceConfig;

/**
 * Application reading all endpoints exposed as resources
 *
 */
@ApplicationPath("/")
public class RestApplication extends ResourceConfig
{

	public RestApplication() {
		packages("com.estafet.costaexpress.rest");
	}
	
}
