package com.estafet.costaexpress.datasource.statement.neo4j;

import java.util.HashMap;
import java.util.Map;

import com.estafet.costaexpress.datasource.entity.IEntityDataSource.EntitySearchAttributesEnum;
import com.estafet.datasource.neo4j.Neo4JDataSource;
import com.estafet.datasource.query.QueryCriteria;


public class MatchEntityQueryCypherStatement extends AbstractEntityQueryCypherStatement {

	public MatchEntityQueryCypherStatement(Neo4JDataSource<?> dataSource) {
		super(dataSource);
		
	}

	@Override
	protected String prepareStatement(QueryCriteria criteria) {
		String type = criteria.getCriteria().get(EntitySearchAttributesEnum.TYPE.value());
		String id = criteria.getCriteria().get(EntitySearchAttributesEnum.CODE.value());
		
		StringBuilder statementBuilder = new StringBuilder();
		statementBuilder.append("MATCH (n:").append(type).append(")")
						.append(" WHERE n.code='").append(id).append("' return n");
		return statementBuilder.toString();
	}

	@Override
	protected Map<String, Map<String, String>> getStatementFields() {
		Map<String, Map<String, String>> statementFields = new HashMap<String, Map<String, String>>();
		Map<String, String> fields = new HashMap<String, String>();
		fields.put(CODE, "code");
		fields.put(NAME, "name");
		statementFields.put("n", fields);
		
		return statementFields;
	}
}
