package com.estafet.costaexpress.datasource.statement.neo4j;

import java.util.List;

import org.neo4j.driver.v1.Record;
import org.neo4j.driver.v1.StatementResult;
import org.neo4j.driver.v1.Value;

import com.estafet.costaexpress.datasource.statement.neo4j.AbstractEntityQueryCypherStatement.SingleStringConsumer;
import com.estafet.datasource.CrossReferenceKey;
import com.estafet.datasource.NodeNotCreatedException;
import com.estafet.datasource.NodeNotFoundException;
import com.estafet.datasource.neo4j.Neo4JDataSource;
import com.estafet.datasource.query.QueryCriteria;
import com.estafet.datasource.statement.IQueryStatement;
import com.estafet.datasource.statement.neo4j.AbstractCypherStatement;

public abstract class AbstractCrossReferenceCypherQueryStatement extends AbstractCypherStatement implements IQueryStatement<CrossReferenceKey, QueryCriteria> {

	static final String CODE = "code";
	static final String NAME = "name";
	
	public AbstractCrossReferenceCypherQueryStatement(Neo4JDataSource<?> dataSource) {
		super(dataSource.newSession());
		
	}
	
	
	public CrossReferenceKey query(QueryCriteria criteria) throws NodeNotFoundException, NodeNotCreatedException {
		String matchCypherStatement = prepareStatement(criteria);
		StatementResult result = run(matchCypherStatement);	
		
		return toCrossReferenceKey(result);
	}
	


	protected CrossReferenceKey toCrossReferenceKey(StatementResult result) {
		CrossReferenceKey key = null;
		List<Record> list = result.list();
		if (!list.isEmpty()) {
					
			Value sourceValue = list.get(0).get(getSourceKey());

			String sourceCode = sourceValue.get(CODE).asString();
			String sourceName = sourceValue.get(NAME).asString();

			SingleStringConsumer sourceLabel = new SingleStringConsumer();
			sourceValue.asNode().labels().forEach(sourceLabel);
			String sourceType = sourceLabel.getValue();
			
			Value targetValue = list.get(0).get(getTargetKey());
			
			String targetCode = null;
			String targetName = null;
			String targetType = null;
			
			if (targetValue != null && !targetValue.isNull()) {
			
				targetCode = targetValue.get(CODE).asString();
				targetName = targetValue.get(NAME).asString();
				
				SingleStringConsumer targetLabel = new SingleStringConsumer();
				targetValue.asNode().labels().forEach(targetLabel);

				targetType = targetLabel.getValue();
				
			} else if (getSecondaryTargetKey()!= null) {
				Value secondaryTarget = list.get(0).get(getSecondaryTargetKey());
				if (secondaryTarget != null && !secondaryTarget.isNull()) {
					targetCode = secondaryTarget.get(CODE).asString();
					targetName = secondaryTarget.get(NAME).asString();
					
					SingleStringConsumer targetLabel = new SingleStringConsumer();
					secondaryTarget.asNode().labels().forEach(targetLabel);
					targetType = targetLabel.getValue();
				}
			}
			
			key = new CrossReferenceKey();
			
			key.setId(sourceCode);
			key.setEntityName(sourceName);
			key.setEntityType(sourceType);
			if (targetCode != null && targetName != null) {
				key.setTargetCode(targetCode);
				key.setTargetName(targetName);
				key.setTargetType(targetType);
			}
		}
		return key;
	}
	
	protected abstract String getSourceKey();
	
	protected abstract String getTargetKey();
	
	protected String getSecondaryTargetKey() {
		return null;
	}
	
	@Override
	protected StatementResult run(String statement) {
		return super.run(statement);
	}

}
