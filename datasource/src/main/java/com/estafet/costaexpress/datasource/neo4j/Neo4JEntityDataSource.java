package com.estafet.costaexpress.datasource.neo4j;

import java.util.Collections;
import java.util.List;
import java.util.Optional;

import org.neo4j.driver.v1.Session;

import com.estafet.costaexpress.datasource.entity.EntityAlreadyExistsException;
import com.estafet.costaexpress.datasource.entity.EntityNotCreatedException;
import com.estafet.costaexpress.datasource.entity.EntityNotFoundException;
import com.estafet.costaexpress.datasource.entity.IEntityDataSource;
import com.estafet.costaexpress.datasource.statement.neo4j.AbstractEntityListQueryCypherStatement;
import com.estafet.costaexpress.datasource.statement.neo4j.AbstractEntityQueryCypherStatement;
import com.estafet.costaexpress.datasource.statement.neo4j.CreateConnectionEntityCypherStatement;
import com.estafet.costaexpress.datasource.statement.neo4j.CreateEntityCypherStatement;
import com.estafet.costaexpress.datasource.statement.neo4j.CreateSiblingEntityCypherStatement;
import com.estafet.costaexpress.datasource.statement.neo4j.CreateUniqueConstraintStatement;
import com.estafet.costaexpress.datasource.statement.neo4j.DeleteEntityCypherStatement;
import com.estafet.costaexpress.datasource.statement.neo4j.MatchEntityQueryCypherStatement;
import com.estafet.costaexpress.datasource.statement.neo4j.MatchPriceListToEntityCypherStatement;
import com.estafet.costaexpress.datasource.statement.neo4j.MatchSiblingEntityQueryCypherStatement;
import com.estafet.costaexpress.model.entity.IPolicyAwareEntity;
import com.estafet.costaexpress.model.entity.connections.ConnectionTypesEnum;
import com.estafet.costaexpress.model.entity.types.EntityTypesEnum;
import com.estafet.costaexpress.model.policy.IPolicy;
import com.estafet.costaexpress.model.relation.OneToManyRelation;
import com.estafet.datasource.DataSourceFailureException;
import com.estafet.datasource.IDataSource;
import com.estafet.datasource.NodeNotCreatedException;
import com.estafet.datasource.NodeNotFoundException;
import com.estafet.datasource.neo4j.Neo4JDataSource;
import com.estafet.datasource.neo4j.Neo4JDataSourceInitializer;
import com.estafet.datasource.query.QueryCriteria;
import com.estafet.datasource.statement.StatementFailedException;
import com.estafet.datasource.statement.neo4j.AbstractExecutableCypherStatement;

public class Neo4JEntityDataSource extends Neo4JDataSource<IPolicyAwareEntity> implements IEntityDataSource<IPolicyAwareEntity, OneToManyRelation, IPolicy>{
	public Neo4JEntityDataSource(Neo4JDataSourceInitializer initializer) {
		super(initializer);
	}

	@Override
	public Optional<IPolicyAwareEntity> find(QueryCriteria criteria) throws DataSourceFailureException {
		if (ready()) {
			MatchEntityQueryCypherStatement statement = new MatchEntityQueryCypherStatement(this);
			try {
				IPolicyAwareEntity entity = query(statement, criteria);
				if (entity != null) {
					return Optional.of(entity);
				}
				return Optional.<IPolicyAwareEntity>empty();
			} catch (EntityNotCreatedException e) {
				return Optional.<IPolicyAwareEntity>empty();
			}
		}
		throw new DataSourceFailureException("Datasource is not ready");
	}

	@Override
	public List<IPolicyAwareEntity> findSiblingEntities(QueryCriteria criteria) throws DataSourceFailureException {
		if (ready()) {
			MatchSiblingEntityQueryCypherStatement statement = new MatchSiblingEntityQueryCypherStatement(this);
			try {
				return queryItems(statement, criteria);
			} catch (NodeNotFoundException e) {
				return Collections.<IPolicyAwareEntity>emptyList();
			} catch (NodeNotCreatedException e) {
				return Collections.<IPolicyAwareEntity>emptyList();
			}
		}
		throw new DataSourceFailureException("Datasource is not ready");
	}
	
	@Override
	public void create(IPolicyAwareEntity entity) throws DataSourceFailureException, EntityAlreadyExistsException {
		if (ready()) {
			Session session = newSession();
			CreateEntityCypherStatement statement = new CreateEntityCypherStatement(session);
			try {
				execute(statement, entity);
			} catch (StatementFailedException e) {
				if (isConflict(e)){
					throw new EntityAlreadyExistsException(e.getMessage());
				} else {
					throw new DataSourceFailureException("Unable to create entity: " + e.getMessage(), e);
				}
			}			
		} else {
			throw new DataSourceFailureException("Datasource is not ready");
		}
	}
	
	@Override
	public void createSibling(IPolicyAwareEntity entity, String fromNodeId, String fromNodeType) throws DataSourceFailureException, EntityAlreadyExistsException {
		if (ready()) {
			Session session = newSession();
			ConnectionTypesEnum connectionType = (EntityTypesEnum.MACHINE.equals(entity.getType()) ? ConnectionTypesEnum.HAS : ConnectionTypesEnum.MANAGES);
			CreateSiblingEntityCypherStatement statement = new CreateSiblingEntityCypherStatement(session, fromNodeId, fromNodeType, connectionType.type());;
			try {
				execute(statement, entity);
			} catch (StatementFailedException e) {
				if (isConflict(e)){
					throw new EntityAlreadyExistsException(e.getMessage());
				} else {
					throw new DataSourceFailureException("Unable to create entity: " + e.getMessage(), e);
				}
			}
		} else {
			throw new DataSourceFailureException("Datasource is not ready");
		}		
	}
	
	
	@Override
	public void createConnection(String fromNodeId, String fromNodeType, String toNodeId, String connectionType) throws DataSourceFailureException {
		if (ready()) {
			Session session = newSession();
			CreateConnectionEntityCypherStatement statement = new CreateConnectionEntityCypherStatement(session, fromNodeId, fromNodeType, connectionType);
			try {
				execute(statement, toNodeId);
			} catch (StatementFailedException e) {
				throw new DataSourceFailureException("Statement failed", e);
			}
		} else {
			throw new DataSourceFailureException("Datasource is not ready");
		}
	}
	
	@Override
	public void delete(String nodeId, String type) throws DataSourceFailureException, EntityNotFoundException {
		delete(nodeId, type, false);
		
	}
	
	public void delete(String nodeId, String type, boolean cascade) throws DataSourceFailureException, EntityNotFoundException {
		if (ready()) {
			Session session = newSession();
			DeleteEntityCypherStatement statement = new DeleteEntityCypherStatement(session, cascade);
			try {
				execute(statement, nodeId);
			} catch (StatementFailedException e) {
				if (isEntityNotFound(e)) {
					throw new EntityNotFoundException(e.getMessage(), e);
				} else {
					throw new DataSourceFailureException("Unable to delete entity: " + e.getMessage(), e);
				}
			}
		} else {
			throw new DataSourceFailureException("Datasource is not ready");
		}
	}
	
	
	@Override
	public Optional<OneToManyRelation> findEntitiesUnderPolicy(QueryCriteria criteria,  Optional<IDataSource<IPolicy>> policyDataSource)
			throws DataSourceFailureException {
		if (policyDataSource.isPresent()) {
			Optional<IPolicy> policy = policyDataSource.get().find(criteria);
			if (policy.isPresent()) {
				
				QueryCriteria crossReferenceCriteria = new QueryCriteria();
				crossReferenceCriteria.addCriteria(EntitySearchAttributesEnum.CODE.value(), policy.get().getId());
				crossReferenceCriteria.addCriteria(EntitySearchAttributesEnum.TYPE.value(), policy.get().getPolicyType().value());
				MatchPriceListToEntityCypherStatement statement = new MatchPriceListToEntityCypherStatement(this);
				
				try {
					List<IPolicyAwareEntity> result = queryItems(statement, crossReferenceCriteria);
					if (result.size() > 0) {
						OneToManyRelation relation = new OneToManyRelation();
						relation.setSource(policy.get());
						relation.setTarget(result);
						return Optional.of(relation);
					}
					return Optional.<OneToManyRelation>empty();
				} catch (NodeNotCreatedException e) {
					return Optional.<OneToManyRelation>empty();
				} catch (NodeNotFoundException e) {
					return Optional.<OneToManyRelation>empty();
				}
			}
			
			return Optional.<OneToManyRelation>empty();
		}
		return Optional.<OneToManyRelation>empty();
	}
	
	protected IPolicyAwareEntity query(AbstractEntityQueryCypherStatement statement, QueryCriteria criteria) throws EntityNotCreatedException {
		return statement.query(criteria);
	}
	
	protected List<IPolicyAwareEntity> queryItems(AbstractEntityListQueryCypherStatement statement, QueryCriteria criteria) throws EntityNotFoundException, EntityNotCreatedException {
		return statement.queryItems(criteria);
	}
	
	@Override
	protected void createUniqueConstraints() {
		for (EntityTypesEnum type : EntityTypesEnum.values()) {
			CreateUniqueConstraintStatement statement = new CreateUniqueConstraintStatement(newSession());
			try {
				statement.execute(type.value());
			} catch (StatementFailedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
	}

	@Override
	protected boolean isConflict(StatementFailedException e) {
		return super.isConflict(e);
	}
	
	@Override
	protected void execute(AbstractExecutableCypherStatement<IPolicyAwareEntity> statement, IPolicyAwareEntity entity)
			throws StatementFailedException {
	
		super.execute(statement, entity);
	}
	
	@Override
	protected void execute(AbstractExecutableCypherStatement<String> statement, String id)
			throws StatementFailedException {
	
		super.execute(statement, id);
	}
	
}
