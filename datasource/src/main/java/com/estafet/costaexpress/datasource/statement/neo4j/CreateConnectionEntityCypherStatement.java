package com.estafet.costaexpress.datasource.statement.neo4j;

import org.neo4j.driver.v1.Session;
import org.neo4j.driver.v1.StatementResult;
import org.neo4j.driver.v1.exceptions.ClientException;

import com.estafet.datasource.neo4j.Neo4JErrorCodes;
import com.estafet.datasource.statement.StatementFailedException;
import com.estafet.datasource.statement.neo4j.AbstractExecutableCypherStatement;

public class CreateConnectionEntityCypherStatement extends AbstractExecutableCypherStatement<String> {

	private final String fromNodeId;
	private final String fromNodeType;
	private final String connectionType;

	public CreateConnectionEntityCypherStatement(Session session, String fromNodeId, String fromNodeType, String connectionType) {
		super(session);
		
		this.fromNodeId = fromNodeId;
		this.fromNodeType = fromNodeType;
		this.connectionType = connectionType;
	}

	@Override
	protected String prepareStatement(String siblingId) {
		StringBuilder statementBuilder = new StringBuilder();
		statementBuilder.append("MATCH (policy:").append(fromNodeType).append("{code:'").append(fromNodeId).append("'})")
						.append(" MATCH (entity {code:'").append(siblingId).append("'})")
						.append(" CREATE UNIQUE (policy)-[:").append(connectionType).append("]->(entity)");
		
		return statementBuilder.toString();
	}

	@Override
	protected void checkStatementResult(StatementResult result) throws StatementFailedException {
		try {
			if(result.summary().counters().relationshipsCreated()== 0) {
				throw new StatementFailedException("Entity not found", Neo4JErrorCodes.ENITY_NOT_FOUND);
			}
		} catch (ClientException e) {
			throw new StatementFailedException(e.getMessage(), e, e.code());
		}
	}


	
	

}
