package com.estafet.costaexpress.datasource.statement.neo4j;

import org.neo4j.driver.v1.Session;
import org.neo4j.driver.v1.StatementResult;
import org.neo4j.driver.v1.exceptions.ClientException;

import com.estafet.costaexpress.model.entity.IPolicyAwareEntity;
import com.estafet.datasource.statement.StatementFailedException;
import com.estafet.datasource.statement.neo4j.AbstractExecutableCypherStatement;

public class CreateSiblingEntityCypherStatement extends AbstractExecutableCypherStatement<IPolicyAwareEntity> {

	private final String connectionType;
	private final String fromNodeType;
	private final String fromNodeId;

	public CreateSiblingEntityCypherStatement(Session session, String fromNodeId, String fromNodeType,
			String connectionType) {
		super(session);

		this.fromNodeId = fromNodeId;
		this.fromNodeType = fromNodeType;
		this.connectionType = connectionType;

	}

	@Override
	protected String prepareStatement(IPolicyAwareEntity entity) {
		StringBuilder statementBuilder = new StringBuilder();
		statementBuilder.append("CREATE (x:").append(entity.getType().value()).append(" {code:\"")
				.append(entity.getId()).append("\", name:\"").append(entity.getName()).append("\"})").append(" WITH x")
				.append(" MATCH (y:").append(fromNodeType).append("{code:\"").append(fromNodeId).append("\"})")
				.append(" CREATE UNIQUE (y)-[:").append(connectionType).append("]-> (x)");

		return statementBuilder.toString();
	}

	@Override
	protected void checkStatementResult(StatementResult result) throws StatementFailedException {
		try {
			result.list();		
		} catch (ClientException e) {
			throw new StatementFailedException(e.getMessage(), e, e.code());
		}
	}

}
