package com.estafet.costaexpress.datasource.statement.neo4j;

import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Optional;
import java.util.function.Consumer;

import org.neo4j.driver.v1.Record;
import org.neo4j.driver.v1.StatementResult;
import org.neo4j.driver.v1.Value;

import com.estafet.costaexpress.datasource.entity.EntityNotCreatedException;
import com.estafet.costaexpress.datasource.entity.EntityNotFoundException;
import com.estafet.costaexpress.datasource.entity.IEntityDataSource;
import com.estafet.costaexpress.model.entity.IPolicyAwareEntity;
import com.estafet.costaexpress.model.entity.factory.EntityFactoryProvider;
import com.estafet.costaexpress.model.entity.types.EntityTypesEnum;
import com.estafet.costaexpress.model.policy.IPolicy;
import com.estafet.datasource.neo4j.Neo4JDataSource;
import com.estafet.datasource.node.IDataSourceNode;
import com.estafet.datasource.query.QueryCriteria;
import com.estafet.datasource.statement.IQueryStatement;
import com.estafet.datasource.statement.neo4j.AbstractCypherStatement;

public abstract class AbstractEntityQueryCypherStatement extends AbstractCypherStatement implements IQueryStatement<IDataSourceNode, QueryCriteria> {

	static class SingleStringConsumer implements Consumer<String> {
		
		private String value;

		@Override
		public void accept(String value) {
			if (this.value == null) {
				this.value = value;
			}
		}
		
		public String getValue() {
			return value;
		}
	}
	
	private final Neo4JDataSource<?> dataSource;	
	
	
	static final String CODE = "code";
	static final String NAME = "name";
	
	public AbstractEntityQueryCypherStatement(Neo4JDataSource<?> dataSource) {
		super(dataSource.newSession());		
		this.dataSource = dataSource;
		
	}	
	
	protected abstract Map<String, Map<String, String>> getStatementFields(); 
	

	protected IPolicyAwareEntity toSingleEntity(StatementResult result) throws EntityNotCreatedException {
		
		@SuppressWarnings("unchecked")
		EntityFactoryProvider provider = new EntityFactoryProvider((IEntityDataSource<IPolicyAwareEntity, ?, IPolicy>)dataSource);
		List<Record> list = result.list();
		if (!list.isEmpty()) {
			Map<String, Map<String, String>> statementFields = getStatementFields();			
			for (Entry<String, Map<String, String>> fieldEntry : statementFields.entrySet()) {
				
				String index = fieldEntry.getKey();
				Value nodeValue = list.get(0).get(index);
				
				String code = nodeValue.get(CODE).asString();
				String name = nodeValue.get(NAME).asString();
				
				SingleStringConsumer label = new SingleStringConsumer();
				nodeValue.asNode().labels().forEach(label);				
				Optional<EntityTypesEnum> type = EntityTypesEnum.fromValue(label.getValue());	
				
				if (type.isPresent()) {
					try {
						return provider.getFactory(type.get()).createEntity(name, code);
					} catch (IllegalArgumentException e) {
						throw new EntityNotCreatedException("Can not find factory for type: " + label.getValue());
					}
				}
				throw new EntityNotCreatedException("Can not find factory for type: " + label.getValue());
			}
		}
		
		return null;
	}
	
	@Override
	protected StatementResult run(String statement) {
		return super.run(statement);
	}
	
	@Override
	public IPolicyAwareEntity query(QueryCriteria criteria) throws EntityNotCreatedException{
		String matchCypherStatement = prepareStatement(criteria);
		StatementResult result = run(matchCypherStatement);		
		
		return toSingleEntity(result);
		
	}
	
	protected List<IPolicyAwareEntity> queryItems(AbstractEntityListQueryCypherStatement statement, QueryCriteria criteria) throws EntityNotFoundException, EntityNotCreatedException {
		return statement.queryItems(criteria);
	}

}
