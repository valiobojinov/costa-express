package com.estafet.costaexpress.datasource.statement.neo4j;

import com.estafet.costaexpress.datasource.entity.IEntityDataSource.EntitySearchAttributesEnum;
import com.estafet.costaexpress.datasource.neo4j.Neo4JEntityDataSource;
//import com.estafet.datasource.initializer.query.QueryCriteria.SearchAttributesEnum;
import com.estafet.datasource.query.QueryCriteria;

public class MatchSiblingEntityQueryCypherStatement extends AbstractEntityListQueryCypherStatement {

	public MatchSiblingEntityQueryCypherStatement(Neo4JEntityDataSource dataSource) {
		super(dataSource);
	}

	@Override
	protected String getTargetKey() {
		return "sibling";
	}

	@Override
	protected String prepareStatement(QueryCriteria criteria) {
		String type = criteria.getCriteria().get(EntitySearchAttributesEnum.TYPE.value());
		String id = criteria.getCriteria().get(EntitySearchAttributesEnum.CODE.value());
		
		StringBuilder statementBuilder = new StringBuilder();
		statementBuilder.append("MATCH (entity:").append(type).append("{code:'").append(id).append("'})")
			.append("-[:manages]->(sibling) RETURN sibling");
		
		return statementBuilder.toString();
	}

	

}
