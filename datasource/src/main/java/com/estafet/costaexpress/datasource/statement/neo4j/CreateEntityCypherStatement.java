package com.estafet.costaexpress.datasource.statement.neo4j;

import org.neo4j.driver.v1.Session;
import org.neo4j.driver.v1.StatementResult;
import org.neo4j.driver.v1.exceptions.ClientException;

import com.estafet.costaexpress.model.entity.IPolicyAwareEntity;
import com.estafet.datasource.statement.StatementFailedException;
import com.estafet.datasource.statement.neo4j.AbstractExecutableCypherStatement;

public class CreateEntityCypherStatement extends AbstractExecutableCypherStatement<IPolicyAwareEntity> {

	public CreateEntityCypherStatement(Session session) {
		super(session);		
	}

	@Override
	protected String prepareStatement(IPolicyAwareEntity entity) {
		StringBuilder statementBuilder = new StringBuilder();
		//session.run("CREATE (n:" + node.getType() + " {code:\"" + node.getId() + "\", name:\"" + node.getName() + "\"})");
		statementBuilder.append("CREATE (n:")
								 .append(entity.getType().value()).append(" {code:\"").append(entity.getId())
								 .append("\", name:\"").append(entity.getName()).append("\"})");
		return statementBuilder.toString();
	}

	@Override
	protected void checkStatementResult(StatementResult result) throws StatementFailedException {
		try {
		result.list();
		} catch (ClientException e) {
			throw new StatementFailedException(e.getMessage(), e, e.code());
		}
	}

	

}
