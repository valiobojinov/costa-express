package com.estafet.costaexpress.datasource.statement.neo4j;

import java.text.SimpleDateFormat;
import java.util.GregorianCalendar;
import java.util.TimeZone;

import org.neo4j.driver.v1.Session;
import org.neo4j.driver.v1.StatementResult;

import com.estafet.costaexpress.model.generated.pricelist.PriceTable;
import com.estafet.costaexpress.model.generated.pricelist.PriceTable.Price;
import com.estafet.costaexpress.model.policy.impl.PriceListPolicy;
import com.estafet.datasource.statement.StatementFailedException;

public class CreatePriceListPolicyCypherStatement extends AbstractExecutablePolicyCypherStatement<PriceListPolicy> {

	public CreatePriceListPolicyCypherStatement(Session session) {
		super(session);
	}

	@Override
	protected String prepareStatement(PriceListPolicy policy) {

		String id = policy.getId();
		
		PriceTable priceTable = policy.getPolicyItem().getItem();
		
		
		StringBuilder statementBuilder = new StringBuilder();
		
		SimpleDateFormat dateFormat = new SimpleDateFormat(DATE_TIME_PATTERN);
		GregorianCalendar calendar = priceTable.getEffectiveDate().toGregorianCalendar();
		TimeZone zone = priceTable.getEffectiveDate().getTimeZone(0);
		dateFormat.setTimeZone(zone);
		dateFormat.setCalendar(calendar);
		String effectiveDate = dateFormat.format(calendar.getTime());

		statementBuilder.append("CREATE (list:pricelist {code: \"").append(id).append("\", name: \"").append(id).append("\"})")
			.append(" CREATE (table:pricetable {code:\"").append(id).append("\", ")
		    .append("isOneShot: \"").append(priceTable.isIsOneShot()).append("\", ")
		    .append("effectiveDate: \"").append(effectiveDate).append("\"})");
		
		StringBuilder connectionBuilder = new StringBuilder();
		    		
		for (int i=0; i < priceTable.getPrice().size(); i++) {
			String priceStatement = praparePriceStatement(i, priceTable.getPrice().get(i));
			statementBuilder.append(priceStatement);
			
			String connectionStatement = prepareConnectionStatement(i);
			connectionBuilder.append(connectionStatement);
		}
		
		statementBuilder.append(connectionBuilder.toString());
		
		return statementBuilder.toString();
	}
	
	String praparePriceStatement(int index, Price price) {
		
		//CREATE (price1:price{ProdAuditId: "1", RegularPrice: "1.0", LargePrice: "1.8", SyrupPrice: "0.7", DoubleShotPrice: "1.0", WithFixedSyrupPrice: "1.5"}) 

		StringBuilder statementBuilder = new StringBuilder();
		
		statementBuilder.append(" CREATE (price").append(index).append(":price {")
			.append("ProdAuditId:\"").append(price.getProdAuditId()).append("\", ")
			.append("RegularPrice:\"").append(price.getRegularPrice()).append("\", ")
			.append("LargePrice:\"").append(price.getLargePrice()).append("\", ")
			.append("SyrupPrice:\"").append(price.getSyrupPrice()).append("\", ")
			.append("DoubleShotPrice:\"").append(price.getDoubleShotPrice()).append("\", ")
			.append("WithFixedSyrupPrice:\"").append(price.getWithFixedSyrupPrice()).append("\"})");
			
		return statementBuilder.toString();
	}
	
	String prepareConnectionStatement(int index) {
		//CREATE UNIQUE (list)-[:uses]-(table)-[:has]-(price2)
		
		StringBuilder statementBuilder = new StringBuilder();		
		statementBuilder.append("CREATE UNIQUE (list)-[:uses]-(table)-[:has]-(price").append(index).append(")");
		return statementBuilder.toString();
	}

	@Override
	protected void checkStatementResult(StatementResult result) throws StatementFailedException{
		if (result.summary().counters().nodesCreated() == 0) {
			throw new StatementFailedException("Entity not created", "Not created");
		}
	}
	
}
