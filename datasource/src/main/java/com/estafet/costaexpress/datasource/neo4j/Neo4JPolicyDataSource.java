package com.estafet.costaexpress.datasource.neo4j;

import java.util.Optional;

import org.neo4j.driver.v1.Session;

import com.estafet.costaexpress.datasource.entity.IEntityDataSource;
import com.estafet.costaexpress.datasource.policy.IPolicyDataSource;
import com.estafet.costaexpress.datasource.policy.PolicyAlreadyExistsException;
import com.estafet.costaexpress.datasource.policy.PolicyNotCreatedException;
import com.estafet.costaexpress.datasource.policy.PolicyNotFoundException;
import com.estafet.costaexpress.datasource.statement.neo4j.AbstractPolicyQueryCypherStatement;
import com.estafet.costaexpress.datasource.statement.neo4j.CreateConnectionEntityCypherStatement;
import com.estafet.costaexpress.datasource.statement.neo4j.CreatePriceListPolicyCypherStatement;
import com.estafet.costaexpress.datasource.statement.neo4j.CreateUniqueConstraintStatement;
import com.estafet.costaexpress.datasource.statement.neo4j.DeletePriceListPolicyCypherStatement;
import com.estafet.costaexpress.datasource.statement.neo4j.MatchEntityToPriceListCypherStatement;
import com.estafet.costaexpress.datasource.statement.neo4j.MatchPriceListPolicyQueryCypherStatement;
import com.estafet.costaexpress.model.entity.IPolicyAwareEntity;
import com.estafet.costaexpress.model.entity.connections.ConnectionTypesEnum;
import com.estafet.costaexpress.model.policy.IPolicy;
import com.estafet.costaexpress.model.policy.impl.PriceListPolicy;
import com.estafet.costaexpress.model.policy.types.PolicyTypesEnum;
import com.estafet.costaexpress.model.relation.OneToOneRelation;
import com.estafet.datasource.CrossReferenceKey;
import com.estafet.datasource.DataSourceFailureException;
import com.estafet.datasource.NodeNotCreatedException;
import com.estafet.datasource.NodeNotFoundException;
import com.estafet.datasource.neo4j.Neo4JDataSourceInitializer;
import com.estafet.datasource.query.QueryCriteria;
import com.estafet.datasource.statement.StatementFailedException;
import com.estafet.datasource.statement.neo4j.AbstractExecutableCypherStatement;

public class Neo4JPolicyDataSource extends RelatedNeo4jDataSource<IPolicy, OneToOneRelation, IPolicyAwareEntity> implements IPolicyDataSource<IPolicy, OneToOneRelation, IPolicyAwareEntity>{
	public Neo4JPolicyDataSource(Neo4JDataSourceInitializer initializer) {
		super(initializer);
	}
	
	@Override
	public Optional<IPolicy> find(QueryCriteria criteria) throws DataSourceFailureException {
		if (ready()) {
			criteria.addCriteria(PolicySearchAttributesEnum.ITEM.value(), "price");
			criteria.addCriteria(PolicySearchAttributesEnum.ITEM_TYPE.value(), "pricetable");
			
			AbstractPolicyQueryCypherStatement<? extends IPolicy, ?> statement = new MatchPriceListPolicyQueryCypherStatement(this);
			try {
				IPolicy policy = query(statement, criteria);
				if (policy != null) {
					return Optional.of(policy);
				}
				return Optional.<IPolicy>empty();
			} catch (PolicyNotCreatedException e) {
				return Optional.<IPolicy>empty();
			}
		
		}
		throw new DataSourceFailureException("Datasource is not ready");
	}	
	
	@Override
	public void create(IPolicy policyEntity) throws DataSourceFailureException, PolicyAlreadyExistsException {
		if (ready()) {		
			try {
				createPolicyEntity(policyEntity);
			} catch (StatementFailedException e) {
				if (isConflict(e)){
					throw new PolicyAlreadyExistsException(e.getMessage());
				} else {
					throw new DataSourceFailureException("Unable to create entity: " + e.getMessage(), e);
				}
			}
		} else {
			throw new DataSourceFailureException("Datasource is not ready");
		}
		
	}

	@Override
	public void delete(String policyId, String type) throws DataSourceFailureException, PolicyNotFoundException {
		if (ready()) {
			Session session = newSession();
			DeletePriceListPolicyCypherStatement statement = new DeletePriceListPolicyCypherStatement(session);
			try {
				execute(statement, policyId);
			} catch (StatementFailedException e) {
				if (isEntityNotFound(e)) {
					throw new PolicyNotFoundException(e.getMessage(), e);
				} else {
					throw new DataSourceFailureException("Unable to delete entity: " + e.getMessage(), e);
				}
			}
		} else {
			throw new DataSourceFailureException("Datasource is not ready");
		}		
	}	
	
	@Override
	public void applyPolicy(String policyId, String policyType, String targetEntityId) throws DataSourceFailureException, PolicyNotFoundException {
		if (ready()) {
			Session session = newSession();
			CreateConnectionEntityCypherStatement statement = new CreateConnectionEntityCypherStatement(session, policyId, policyType, ConnectionTypesEnum.APPLIES_TO.type());
			try {
				execute(statement, targetEntityId);
			} catch (StatementFailedException e) {
				if (isEntityNotFound(e)) {
					throw new PolicyNotFoundException(e.getMessage(), e);
				} else {
					throw new DataSourceFailureException("Unable to create connection" + e.getMessage(), e);
				}
			}
			
		} else {
			throw new DataSourceFailureException("Datasource is not ready");
		}
	}
	
	
	
	void createPolicyEntity(IPolicy policyEntity) throws StatementFailedException {
		Session session = newSession();
		if (PolicyTypesEnum.PRICELIST.equals(policyEntity.getPolicyType())) {			
			CreatePriceListPolicyCypherStatement statement = new CreatePriceListPolicyCypherStatement(session);
			execute(statement, (PriceListPolicy)policyEntity);
		}		
	}

	

	@Override
	protected void createUniqueConstraints() {
		for (PolicyTypesEnum type : PolicyTypesEnum.values()) {
			CreateUniqueConstraintStatement statement = new CreateUniqueConstraintStatement(newSession());
			try {
				statement.execute(type.value());
			} catch (StatementFailedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
	}

	@Override
	public Optional<OneToOneRelation> findPolicyForEntity(QueryCriteria criteria,
			IEntityDataSource<IPolicyAwareEntity, ?, IPolicy> relatedDataSource) throws DataSourceFailureException {
		Optional<IPolicyAwareEntity> entity = relatedDataSource.find(criteria);
		if (entity.isPresent()) {
			MatchEntityToPriceListCypherStatement statement = new MatchEntityToPriceListCypherStatement(this);
			QueryCriteria crossReferenceCriteria = new QueryCriteria();
			try {
				CrossReferenceKey key = query(statement, criteria);
				if (key != null) {
					crossReferenceCriteria.addCriteria(PolicySearchAttributesEnum.TYPE.value(), key.getTargetType());
					crossReferenceCriteria.addCriteria(PolicySearchAttributesEnum.CODE.value(), key.getTargetCode());
					Optional<IPolicy> policy = find(crossReferenceCriteria);
					if (policy.isPresent()) {

						return Optional.of(new OneToOneRelation(entity.get(), policy.get()));
					}
				}
				return Optional.<OneToOneRelation>empty();
			} catch (NodeNotFoundException e) {
				return Optional.<OneToOneRelation>empty();
			} catch (NodeNotCreatedException e) {
				return Optional.<OneToOneRelation>empty();
			}
		}

		return Optional.<OneToOneRelation>empty();
	}

	
	protected CrossReferenceKey query (MatchEntityToPriceListCypherStatement statement, QueryCriteria criteria) throws NodeNotFoundException, NodeNotCreatedException {
		return statement.query(criteria);
	}	
	
	protected void execute (CreatePriceListPolicyCypherStatement statement, PriceListPolicy policy) throws StatementFailedException {
		statement.execute(policy);
	}
	
	protected IPolicy query(AbstractPolicyQueryCypherStatement<? extends IPolicy, ?> statement, QueryCriteria criteria) throws PolicyNotCreatedException {
		return statement.query(criteria);
	}
	
	@Override
	protected void execute(AbstractExecutableCypherStatement<IPolicy> statement, IPolicy entity)
			throws StatementFailedException {
		super.execute(statement, entity);
	}
	
	@Override
	protected void execute(AbstractExecutableCypherStatement<String> statement, String id)
			throws StatementFailedException {	
		super.execute(statement, id);
	}
	
	@Override
	protected boolean isEntityNotFound(StatementFailedException e) {
		return super.isEntityNotFound(e);
	}
	
	@Override
	protected boolean isConflict(StatementFailedException e) {
		return super.isConflict(e);
	}

}
