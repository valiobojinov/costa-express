package com.estafet.costaexpress.datasource.statement.neo4j;

import org.neo4j.driver.v1.Session;
import org.neo4j.driver.v1.StatementResult;
import org.neo4j.driver.v1.exceptions.ClientException;

import com.estafet.datasource.neo4j.Neo4JErrorCodes;
import com.estafet.datasource.statement.StatementFailedException;
import com.estafet.datasource.statement.neo4j.AbstractExecutableCypherStatement;

public class DeleteEntityCypherStatement extends AbstractExecutableCypherStatement<String>{

	@SuppressWarnings("unused")
	private boolean cascade;
	
	public DeleteEntityCypherStatement(Session session) {
		this(session, false);
	}
	
	public DeleteEntityCypherStatement(Session session, boolean cascade) {
		super(session);
	
		this.cascade = cascade;
	}

	@Override
	protected String prepareStatement(String nodeId) {
		StringBuilder statementBuidler = new StringBuilder();
		statementBuidler.append("MATCH (n {code: \"").append(nodeId).append("\"}) DETACH DELETE n");
		return statementBuidler.toString();
	}

	@Override
	protected void checkStatementResult(StatementResult result) throws StatementFailedException {
		try {
			if(result.summary().counters().nodesDeleted() == 0) {
				throw new StatementFailedException("Entity not found", Neo4JErrorCodes.ENITY_NOT_FOUND);
			}
		} catch (ClientException e) {
			throw new StatementFailedException(e.getMessage(), e, e.code());
		}
	}



}
