package com.estafet.costaexpress.datasource.statement.neo4j;

import com.estafet.costaexpress.datasource.policy.IPolicyDataSource.PolicySearchAttributesEnum;
import com.estafet.datasource.neo4j.Neo4JDataSource;
//import com.estafet.datasource.initializer.query.QueryCriteria.SearchAttributesEnum;
import com.estafet.datasource.query.QueryCriteria;

public class MatchPriceListPolicyQueryCypherStatement extends AbstractPriceListPolicyQueryCypherStatement{
	
	public MatchPriceListPolicyQueryCypherStatement(Neo4JDataSource<?> dataSource) {
		super(dataSource);		
	}	

	@Override
	protected String prepareStatement(QueryCriteria criteria) {
		String type = criteria.getCriteria().get(PolicySearchAttributesEnum.TYPE.value());
		String id = criteria.getCriteria().get(PolicySearchAttributesEnum.CODE.value());
		String item = criteria.getCriteria().get(PolicySearchAttributesEnum.ITEM.value());
		String itemType = criteria.getCriteria().get(PolicySearchAttributesEnum.ITEM_TYPE.value());
		
		StringBuilder statementBuilder = new StringBuilder();
		statementBuilder.append("MATCH (p:").append(type).append("{code:'").append(id).append("'})")
			.append("-[:uses]->(t:").append(itemType).append(")<-[:has]-(i:").append(item).append(")")
			.append(" RETURN p, t, i");
		
		return statementBuilder.toString();
	}
	
}
