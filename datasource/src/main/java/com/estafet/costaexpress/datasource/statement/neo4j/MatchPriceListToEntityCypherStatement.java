package com.estafet.costaexpress.datasource.statement.neo4j;

import java.util.List;

import org.neo4j.driver.v1.StatementResult;

import com.estafet.costaexpress.datasource.entity.EntityNotCreatedException;
import com.estafet.costaexpress.datasource.entity.EntityNotFoundException;
import com.estafet.costaexpress.datasource.neo4j.Neo4JEntityDataSource;
import com.estafet.costaexpress.datasource.policy.IPolicyDataSource.PolicySearchAttributesEnum;
import com.estafet.costaexpress.model.entity.IPolicyAwareEntity;
import com.estafet.datasource.query.QueryCriteria;

public class MatchPriceListToEntityCypherStatement extends AbstractEntityListQueryCypherStatement {

	public MatchPriceListToEntityCypherStatement(Neo4JEntityDataSource dataSource) {
		super(dataSource);
	}

	@Override
	protected String prepareStatement(QueryCriteria criteria) {
		String type = criteria.getCriteria().get(PolicySearchAttributesEnum.TYPE.value());
		String id = criteria.getCriteria().get(PolicySearchAttributesEnum.CODE.value());
		
		StringBuilder statementBuilder = new StringBuilder();
		
		statementBuilder.append("MATCH (price:").append(type).append("{code:'").append(id).append("'})-[:appliesTo]->(group)-[:manages*]->(excluded)<-[:appliesTo]-(p:pricelist)")
			.append(" WHERE p.code <> price.code")
			.append(" WITH collect(excluded) as already_assigned")
			.append(" MATCH path = (price:").append(type).append("{code:'").append(id).append("'})-[:appliesTo|manages*]->(entity)")
			.append(" WHERE NOT any(node in already_assigned WHERE node in NODES(path))")
			.append(" RETURN price, entity");
		
		return statementBuilder.toString();
	}

	@Override
	public List<IPolicyAwareEntity> queryItems(QueryCriteria criteria) throws EntityNotFoundException, EntityNotCreatedException {
		String matchCypherStatement = prepareStatement(criteria);
		StatementResult result = run(matchCypherStatement);		
		
		return toEntityList(result);
	}


	

	/*@Override
	protected String getSourceKey() {
		return "price";
	}*/

	@Override
	protected String getTargetKey() {
		// TODO Auto-generated method stub
		return "entity";
	}


}
