package com.estafet.costaexpress.datasource.statement.neo4j;

import java.util.List;
import java.util.Optional;
import java.util.function.Consumer;

import org.neo4j.driver.v1.Record;
import org.neo4j.driver.v1.StatementResult;
import org.neo4j.driver.v1.Value;

import com.estafet.costaexpress.datasource.policy.IPolicyDataSource;
import com.estafet.costaexpress.datasource.policy.IPolicyDataSource.PolicySearchAttributesEnum;
import com.estafet.costaexpress.datasource.policy.PolicyNotCreatedException;
import com.estafet.costaexpress.model.policy.IPolicy;
import com.estafet.costaexpress.model.policy.PolicyItem;
import com.estafet.costaexpress.model.policy.factory.IPolicyFactory;
import com.estafet.costaexpress.model.policy.factory.PolicyFactoryProvider;
import com.estafet.costaexpress.model.policy.types.PolicyTypesEnum;
import com.estafet.costaexpress.model.relation.OneToOneRelation;
import com.estafet.datasource.neo4j.Neo4JDataSource;
import com.estafet.datasource.query.QueryCriteria;
import com.estafet.datasource.statement.IQueryStatement;
import com.estafet.datasource.statement.neo4j.AbstractCypherStatement;

public abstract class AbstractPolicyQueryCypherStatement<P extends IPolicy, I> extends AbstractCypherStatement implements IQueryStatement<P, QueryCriteria> {
	
	static class SingleStringConsumer implements Consumer<String> {
		
		private String value;

		@Override
		public void accept(String value) {
			if (this.value == null) {
				this.value = value;
			}
		}
		
		public String getValue() {
			return value;
		}
	}
	
	private final Neo4JDataSource<?> dataSource;	
	
	static final String POLICY_INDEX = "p";
	static final String ITEM_INDEX = "i";
	static final String TABLE_INDEX = "t";
	
	
	
	public AbstractPolicyQueryCypherStatement(Neo4JDataSource<?> dataSource) {
		super(dataSource.newSession());		
		this.dataSource = dataSource;
		
	}	
	
	protected abstract I toPolicyItem(List<Record> nodes) throws PolicyNotCreatedException;

	protected P toSingleEntity(StatementResult result) throws PolicyNotCreatedException {

		@SuppressWarnings("unchecked")
		PolicyFactoryProvider provider = new PolicyFactoryProvider((IPolicyDataSource<IPolicy, OneToOneRelation, ?>)dataSource);
		List<Record> list = result.list();
		if (!list.isEmpty()) {
			
			Value resultValue = list.get(0).get(POLICY_INDEX);

			String code = resultValue.get(PolicySearchAttributesEnum.CODE.value()).asString();
			String name = resultValue.get(PolicySearchAttributesEnum.NAME.value()).asString();

			SingleStringConsumer label = new SingleStringConsumer();
			resultValue.asNode().labels().forEach(label);
			Optional<PolicyTypesEnum> type = PolicyTypesEnum.fromValue(label.getValue());

			if (type.isPresent()) {
				try {
					@SuppressWarnings("unchecked")
					IPolicyFactory<P, I> factory = (IPolicyFactory<P, I>) provider.getFactory(type.get());
					PolicyItem<I> item = new PolicyItem<I>(toPolicyItem(list));
					return factory.createPolicy(name, code, item);
				} catch (IllegalArgumentException e) {
					throw new PolicyNotCreatedException("Can not find factory for type: " + label.getValue());
				}
			}
			throw new PolicyNotCreatedException("Can not find factory for type: " + label.getValue());
		}

		throw new PolicyNotCreatedException("Can not create policy from an empty result set");
	}
	
	
	@Override
	public P query(QueryCriteria criteria) throws PolicyNotCreatedException {
		String matchCypherStatement = prepareStatement(criteria);
		StatementResult result = run(matchCypherStatement);		
		
		return toSingleEntity(result);
		
	}
	
	@Override
	protected StatementResult run(String statement) {
		return super.run(statement);
	}

}
