package com.estafet.costaexpress.datasource.statement.neo4j;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import org.neo4j.driver.v1.Record;
import org.neo4j.driver.v1.StatementResult;
import org.neo4j.driver.v1.Value;

import com.estafet.costaexpress.datasource.entity.EntityNotCreatedException;
import com.estafet.costaexpress.datasource.entity.EntityNotFoundException;
import com.estafet.costaexpress.datasource.neo4j.Neo4JEntityDataSource;
import com.estafet.costaexpress.datasource.statement.neo4j.AbstractEntityQueryCypherStatement.SingleStringConsumer;
import com.estafet.costaexpress.model.entity.IPolicyAwareEntity;
import com.estafet.costaexpress.model.entity.factory.EntityFactoryProvider;
import com.estafet.costaexpress.model.entity.types.EntityTypesEnum;
import com.estafet.datasource.query.QueryCriteria;
import com.estafet.datasource.statement.neo4j.AbstractCypherStatement;

public abstract class AbstractEntityListQueryCypherStatement extends AbstractCypherStatement {

	static final String CODE = "code";
	static final String NAME = "name";
	
	private final Neo4JEntityDataSource dataSource;
	
	public AbstractEntityListQueryCypherStatement(Neo4JEntityDataSource dataSource) {
		super(dataSource.newSession());
		
		this.dataSource = dataSource;
	}

	
	public List<IPolicyAwareEntity> queryItems(QueryCriteria criteria) throws EntityNotFoundException, EntityNotCreatedException {
		String matchCypherStatement = prepareStatement(criteria);
		StatementResult result = run(matchCypherStatement);		
		
		return toEntityList(result);
	}

	protected List<IPolicyAwareEntity> toEntityList(StatementResult result) throws EntityNotCreatedException {
		EntityFactoryProvider provider = new EntityFactoryProvider(getDataSource());
		
		List<Record> records = result.list();
		if (!records.isEmpty()) {
			List<IPolicyAwareEntity> list = new ArrayList<IPolicyAwareEntity>(records.size());
			for (Record record : records) {
				
				Value nodeValue = record.get(getTargetKey());
				
				String code = nodeValue.get(CODE).asString();
				String name = nodeValue.get(NAME).asString();
				
				SingleStringConsumer label = new SingleStringConsumer();
				nodeValue.asNode().labels().forEach(label);				
				Optional<EntityTypesEnum> type = EntityTypesEnum.fromValue(label.getValue());	
				
				if (type.isPresent()) {
					try {
						list.add(provider.getFactory(type.get()).createEntity(name, code));
					} catch (IllegalArgumentException e) {
						throw new EntityNotCreatedException("Can not find factory for type: " + label.getValue());
					}
				}				
			}
			
			return list;
		}
		return Collections.<IPolicyAwareEntity>emptyList();
	}

	protected abstract String getTargetKey();

	@Override
	protected StatementResult run(String statement) {
		return super.run(statement);
	}

	private Neo4JEntityDataSource getDataSource() {
		return dataSource;
	}

}
