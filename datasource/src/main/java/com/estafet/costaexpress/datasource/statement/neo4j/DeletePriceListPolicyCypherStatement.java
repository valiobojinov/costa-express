package com.estafet.costaexpress.datasource.statement.neo4j;

import org.neo4j.driver.v1.Session;
import org.neo4j.driver.v1.StatementResult;
import org.neo4j.driver.v1.exceptions.ClientException;

import com.estafet.datasource.neo4j.Neo4JErrorCodes;
import com.estafet.datasource.statement.StatementFailedException;
import com.estafet.datasource.statement.neo4j.AbstractExecutableCypherStatement;

public class DeletePriceListPolicyCypherStatement extends AbstractExecutableCypherStatement<String> {

	public DeletePriceListPolicyCypherStatement(Session session) {
		super(session);
	}

	@Override
	protected String prepareStatement(String identifier) {
		
		StringBuilder statementBuidler = new StringBuilder();
		statementBuidler.append("MATCH (pricelist{code: '").append(identifier).append("'})")
			.append(" OPTIONAL MATCH (pricelist)-[uses_connection:uses]-(pricetable)-[has_connection:has]-(price)")
			.append(" DETACH DELETE uses_connection, has_connection,  pricelist, pricetable, price");
		return statementBuidler.toString();
	}

	@Override
	protected void checkStatementResult(StatementResult result) throws StatementFailedException {
		try {
			if(result.summary().counters().nodesDeleted() == 0) {
				throw new StatementFailedException("Pricelist not found", Neo4JErrorCodes.ENITY_NOT_FOUND);
			}
		} catch (ClientException e) {
			throw new StatementFailedException(e.getMessage(), e, e.code());
		}
	}
}
