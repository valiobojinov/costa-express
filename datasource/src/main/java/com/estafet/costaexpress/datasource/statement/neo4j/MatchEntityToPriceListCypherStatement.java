package com.estafet.costaexpress.datasource.statement.neo4j;

import com.estafet.costaexpress.datasource.entity.IEntityDataSource.EntitySearchAttributesEnum;
import com.estafet.costaexpress.datasource.neo4j.Neo4JPolicyDataSource;
import com.estafet.datasource.query.QueryCriteria;

public class MatchEntityToPriceListCypherStatement extends AbstractCrossReferenceCypherQueryStatement {

	public MatchEntityToPriceListCypherStatement(Neo4JPolicyDataSource dataSource) {
		super(dataSource);
	}

	@Override
	protected String prepareStatement(QueryCriteria criteria) {
		
		String type = criteria.getCriteria().get(EntitySearchAttributesEnum.TYPE.value());
		String id = criteria.getCriteria().get(EntitySearchAttributesEnum.CODE.value());
		
		StringBuilder statementBuilder = new StringBuilder();
		
		statementBuilder.append("MATCH (entity:").append(type).append(" {code:'").append(id).append("'})")
				.append(" OPTIONAL MATCH (entity)<-[:manages*]-(group)<-[:appliesTo*]-(parent_price)")
				.append(" OPTIONAL MATCH (price)-[:appliesTo]->(entity)")
				.append(" WITH price, parent_price, entity, size(shortestPath((entity)-[*]-(parent_price))) as distance")
				.append(" ORDER BY distance")
				.append(" RETURN price, entity, parent_price, distance LIMIT 1");
		
		return statementBuilder.toString();
	}

	@Override
	protected String getSecondaryTargetKey() {
		return "parent_price";
	}

	@Override
	protected String getSourceKey() {
		return "entity";
	}

	@Override
	protected String getTargetKey() {		
		return "price";
	}
}
