package com.estafet.costaexpress.datasource.statement.neo4j;

import java.math.BigDecimal;
import java.text.ParseException;
import java.util.List;

import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.XMLGregorianCalendar;

import org.neo4j.driver.v1.Record;
import org.neo4j.driver.v1.Value;

import com.estafet.costaexpress.datasource.policy.PolicyNotCreatedException;
import com.estafet.costaexpress.model.generated.pricelist.PriceTable;
import com.estafet.costaexpress.model.generated.pricelist.PriceTable.Price;
import com.estafet.costaexpress.model.policy.impl.PriceListPolicy;
import com.estafet.datasource.neo4j.Neo4JDataSource;


public abstract class AbstractPriceListPolicyQueryCypherStatement extends AbstractPolicyQueryCypherStatement<PriceListPolicy, PriceTable> {

	static final String PRICE_TABLE_FIELD_IS_ONE_SHOT = "isOneShot";
	static final String PRICE_TABLE_EFFECTIVE_DATE = "effectiveDate";
	
	static final String SYRUP_PRICE = "SyrupPrice";
	static final String PROD_AUTID_ID = "ProdAuditId";
	static final String LARGE_PRICE = "LargePrice";
	static final String REGULAR_PRICE = "RegularPrice";
	static final String WITH_FIXED_SYRUP_PRICE = "WithFixedSyrupPrice";
	static final String DOUBLE_SHOT_PRICE = "DoubleShotPrice";
	
	public AbstractPriceListPolicyQueryCypherStatement(Neo4JDataSource<?> dataSource) {
		super(dataSource);
	}

	@Override
	protected PriceTable toPolicyItem(List<Record> list) throws PolicyNotCreatedException {
		
		PriceTable priceTable = new PriceTable();
		
		if (list.size() > 0) {
			Value tableValue = list.get(0).get(TABLE_INDEX);
			boolean isOneShot = new Boolean(tableValue.get(PRICE_TABLE_FIELD_IS_ONE_SHOT).asString());
			priceTable.setIsOneShot(isOneShot);
			String effectiveDate = tableValue.get(PRICE_TABLE_EFFECTIVE_DATE).asString();

			XMLGregorianCalendar xmlDate;
			try {
				xmlDate = toXmlCalendar(effectiveDate);
			} catch (ParseException e)  {
				throw new PolicyNotCreatedException("Effective date is corrupted", e);
			}
			catch (DatatypeConfigurationException e) {
				throw new PolicyNotCreatedException("Effective date is corrupted", e);
			}
			priceTable.setEffectiveDate(xmlDate);

			
			for (int index = 0; index < list.size(); index++) {
				Value itemValue = list.get(index).get(ITEM_INDEX);
				Price price = new PriceTable.Price();
				price.setProdAuditId(itemValue.get(PROD_AUTID_ID).asString());
				price.setDoubleShotPrice(new BigDecimal(itemValue.get(DOUBLE_SHOT_PRICE).asString()));
				price.setSyrupPrice(new BigDecimal(itemValue.get(SYRUP_PRICE).asString()));
				price.setWithFixedSyrupPrice(new BigDecimal(itemValue.get(WITH_FIXED_SYRUP_PRICE).asString()));
				price.setLargePrice(new BigDecimal(itemValue.get(LARGE_PRICE).asString()));
				price.setRegularPrice(new BigDecimal(itemValue.get(REGULAR_PRICE).asString()));
				priceTable.getPrice().add(price);
			}
		}
		return priceTable;
	}

	@Override
	protected XMLGregorianCalendar toXmlCalendar(String effectiveDate)
			throws ParseException, DatatypeConfigurationException {
		return super.toXmlCalendar(effectiveDate);
	}

}
