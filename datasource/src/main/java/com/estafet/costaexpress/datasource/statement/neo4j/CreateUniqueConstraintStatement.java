package com.estafet.costaexpress.datasource.statement.neo4j;

import org.neo4j.driver.v1.Session;
import org.neo4j.driver.v1.StatementResult;

import com.estafet.datasource.statement.neo4j.AbstractExecutableCypherStatement;

public class CreateUniqueConstraintStatement extends AbstractExecutableCypherStatement<String> {

	public CreateUniqueConstraintStatement(Session session) {
		super(session);
	}

	@Override
	protected String prepareStatement(String entityType) {
		//CREATE CONSTRAINT ON (e:group) ASSERT e.code IS UNIQUE
		StringBuilder statementBuilder = new StringBuilder();
		statementBuilder.append("CREATE CONSTRAINT ON (e:")
			.append(entityType).append(") ASSERT e.code IS UNIQUE");
		return statementBuilder.toString();
	}

	@Override
	protected void checkStatementResult(StatementResult result) {
	}

}
