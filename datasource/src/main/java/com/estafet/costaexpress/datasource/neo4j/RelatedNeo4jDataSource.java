package com.estafet.costaexpress.datasource.neo4j;

import com.estafet.datasource.neo4j.Neo4JDataSource;
import com.estafet.datasource.neo4j.Neo4JDataSourceInitializer;

public abstract class RelatedNeo4jDataSource<N, R, T> extends Neo4JDataSource<N> {

	public RelatedNeo4jDataSource(Neo4JDataSourceInitializer initializer) {
		super(initializer);
	}
}