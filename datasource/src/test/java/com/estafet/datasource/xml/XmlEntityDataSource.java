package com.estafet.datasource.xml;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map.Entry;
import java.util.Optional;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.TransformerFactoryConfigurationError;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;

import org.apache.log4j.Logger;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import com.estafet.costaexpress.datasource.entity.EntityNotCreatedException;
import com.estafet.costaexpress.datasource.entity.IEntityDataSource;
import com.estafet.costaexpress.model.entity.IEntity;
import com.estafet.costaexpress.model.entity.IPolicyAwareEntity;
import com.estafet.costaexpress.model.entity.UnknownEntityTypeException;
import com.estafet.costaexpress.model.entity.factory.EntityFactoryProvider;
import com.estafet.costaexpress.model.entity.factory.IEntityFactory;
import com.estafet.costaexpress.model.entity.impl.MachineEntity;
import com.estafet.costaexpress.model.entity.types.EntityTypesEnum;
import com.estafet.costaexpress.model.generated.pricelist.PriceTable;
import com.estafet.costaexpress.model.relation.OneToManyRelation;
import com.estafet.datasource.AbstractDataSource;
import com.estafet.datasource.DataSourceFailureException;
import com.estafet.datasource.IDataSource;
import com.estafet.datasource.NodeAlreadyExistsException;
import com.estafet.datasource.NodeNotFoundException;
import com.estafet.datasource.initializer.DataSourceNotInitializedException;
import com.estafet.datasource.query.QueryCriteria;

public class XmlEntityDataSource extends AbstractDataSource<XmlDataSourceInitializer, IPolicyAwareEntity> implements IEntityDataSource<IPolicyAwareEntity, OneToManyRelation, IEntity> {

	private static Logger LOGGER = Logger.getLogger(XmlEntityDataSource.class);

	private Document document;

	private File xmlSourceFile;

	public XmlEntityDataSource(XmlDataSourceInitializer initializer) {
		super(initializer);
	}
	
	@Override
	public Optional<IPolicyAwareEntity> find(QueryCriteria criteria)
			throws DataSourceFailureException {
		if (ready()) {
			try {
				Optional<Node> result = findNode(criteria);
				if (result.isPresent()) {
					IPolicyAwareEntity entity = newEntity(result.get());
					return Optional.of(entity);
				}
				return Optional.<IPolicyAwareEntity>empty();		
			} catch (EntityNotCreatedException e) {
				return Optional.<IPolicyAwareEntity>empty();
			} catch (UnknownEntityTypeException e) {
				throw new DataSourceFailureException(e.getMessage(), e);
			}
		}
		return Optional.<IPolicyAwareEntity>empty();
	}	

	@Override
	public List<IPolicyAwareEntity> findSiblingEntities(QueryCriteria siblingQueryCriteria) throws DataSourceFailureException {
		if (ready()) {
			XPath xPath = XPathFactory.newInstance().newXPath();
			try {
				String xPathExpression = prepareXPathExpression(siblingQueryCriteria);
				NodeList result = (NodeList) xPath.evaluate(xPathExpression, getSourceNode(), XPathConstants.NODESET);
				if (result.getLength() > 0)
				{
					List<IPolicyAwareEntity> queryResult = new ArrayList<IPolicyAwareEntity>(result.getLength());
					for (int index = 0; index < result.getLength(); index++) {
						try {
							queryResult.add(newEntity(result.item(index)));
						} catch (UnknownEntityTypeException e) {
							//XXX Ignore xml will not be used in production
						} catch (EntityNotCreatedException e) {
							//XXX Ignore xml will not be used in production
						}
					}
					return queryResult;
				}
				return Collections.<IPolicyAwareEntity>emptyList();
				
			}
			catch (XPathExpressionException e) {
				throw new DataSourceFailureException(e.getMessage(), e);
			}

		}
		return Collections.<IPolicyAwareEntity>emptyList();
	}


	@Override
	public void reset() throws DataSourceNotInitializedException {
		close();
		init();
	}

	@Override
	public void close() {
		document = null;
	}

	@Override
	public boolean ready() {
		return document != null;
	}
	
	@Override
	public void init() throws DataSourceNotInitializedException {
		try {
			xmlSourceFile = getDataSourceInitializer().getXmlDataSourceFile();
			
			initializeDataSource();
		} catch (ParserConfigurationException e) {
			LOGGER.error(String.format("Unable to parse %s", getSourcePath()), e);
			throw new DataSourceNotInitializedException(e.getMessage(), e);
		} catch (FileNotFoundException e) {
			LOGGER.error(String.format("Source file %s does not exist", getSourcePath()), e);
			throw new DataSourceNotInitializedException(e.getMessage(), e);
		} catch (SAXException e) {
			LOGGER.error(String.format("Unable to parse %s", getSourcePath()), e);
			throw new DataSourceNotInitializedException(e.getMessage(), e);
		} catch (IOException e) {
			throw new DataSourceNotInitializedException(e.getMessage(), e);
		}
	}
	
	public Node getSourceNode() {
		return document;
	}
	
	void initializeDataSource() throws ParserConfigurationException, FileNotFoundException, SAXException, IOException  {
		DocumentBuilder builder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
		document = builder.parse(getSource());
	}
	
	
	String getSourcePath() {
		return xmlSourceFile.getAbsolutePath();
	}
	
	
	private InputStream getSource() throws FileNotFoundException {
		return new FileInputStream(xmlSourceFile);
	}	
	
	private void applyPolicyChanges(Node policyParentNode, IPolicyAwareEntity entity) throws DataSourceFailureException{
		if (entity.getType().equals(EntityTypesEnum.MACHINE)) {
			PriceTable priceTable = ((MachineEntity)entity).getPriceTable();
			if (priceTable != null) {
				Element priceListElement = document.createElement("PriceList");				
				
				try {
					JAXBContext ctx = JAXBContext.newInstance(PriceTable.class);
					ctx.createMarshaller().marshal(priceTable, priceListElement);
				} catch (JAXBException e) {
					throw new DataSourceFailureException(e.getMessage(), e);
				}
				
				policyParentNode.appendChild(priceListElement);
			}
		}
	}
	
	private void save() throws DataSourceFailureException {
		File parent = xmlSourceFile.getParentFile();
		if (parent.isDirectory()) {
			File result = new File(parent, xmlSourceFile.getName() + ".xml");
				try {
					Transformer transformer = TransformerFactory.newInstance().newTransformer();
					transformer.transform(new DOMSource(document), new StreamResult(result));
				} catch (TransformerConfigurationException e) {
					throw new DataSourceFailureException(e.getMessage(), e);
				} catch (TransformerFactoryConfigurationError e) {
					throw new DataSourceFailureException(e.getMessage(), e);
				} catch (TransformerException e) {
					throw new DataSourceFailureException(e.getMessage(), e);
				}
		}
	}
	
	private String prepareXPathExpression(QueryCriteria criteria) {
		StringBuilder expressionBuilder = new StringBuilder();
		if (criteria.hasCriteria()) {			
			expressionBuilder.append("//Entity");
			for (Entry<String, String> entry : criteria.getCriteria().entrySet()) {
				expressionBuilder.append("[@").append(entry.getKey()).append("='").append(entry.getValue()).append("']");				
			}
			if (criteria.isQuerySiblingEntities()) {
				expressionBuilder.append("/SiblingEntities/*");
			}
		}
		return expressionBuilder.toString();
	}
	
	private IPolicyAwareEntity newEntity(Node result) throws UnknownEntityTypeException, EntityNotCreatedException {
		Node typeAttribute = result.getAttributes().getNamedItem(EntitySearchAttributesEnum.TYPE.value());
		String type = typeAttribute.getNodeValue();
		
		Node nameAttribute = result.getAttributes().getNamedItem(EntitySearchAttributesEnum.NAME.value());
		String name = nameAttribute.getNodeValue();
		
		Node idAttribute = result.getAttributes().getNamedItem(EntitySearchAttributesEnum.CODE.value());
		String id = idAttribute.getNodeValue();
		
		return (IPolicyAwareEntity)getFactoryForType(EntityTypesEnum.fromValue(type).get()).createFromDataSource(name, id);
	}
	
	private IEntityFactory<IPolicyAwareEntity> getFactoryForType(EntityTypesEnum type) {
		EntityFactoryProvider provider = new EntityFactoryProvider(this);
		return provider.getFactory(type);
	}

	public Optional<Node> findNode(QueryCriteria criteria) throws DataSourceFailureException {
		XPath xPath = XPathFactory.newInstance().newXPath();
		try {
			String xPathExpression = prepareXPathExpression(criteria);
			Node result = (Node) xPath.evaluate(xPathExpression, getSourceNode(), XPathConstants.NODE);
			if (result != null) {
				return Optional.of(result);
			}
			return Optional.<Node>empty();
		} catch (XPathExpressionException e) {
			throw new DataSourceFailureException(e.getMessage(), e);
		}
	}


	@Override
	protected XmlDataSourceInitializer getDataSourceInitializer() {
		return (XmlDataSourceInitializer)super.getDataSourceInitializer();
	}

	@Override
	public void create(IPolicyAwareEntity entity) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void createConnection(String fromNodeId, String fromNodeType, String toNodeId, String connectionType)
			throws DataSourceFailureException, NodeAlreadyExistsException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void createSibling(IPolicyAwareEntity entity, String fromNodeId, String fromNodeType)
			throws DataSourceFailureException, NodeAlreadyExistsException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void delete(String entityId, String type, boolean cascade)
			throws DataSourceFailureException, NodeNotFoundException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void delete(String nodeId, String type) throws DataSourceFailureException, NodeNotFoundException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public Optional<OneToManyRelation> findEntitiesUnderPolicy(QueryCriteria criteria,
			Optional<IDataSource<IEntity>> policyDataSource) throws DataSourceFailureException {
		// TODO Auto-generated method stub
		return null;
	}

}
