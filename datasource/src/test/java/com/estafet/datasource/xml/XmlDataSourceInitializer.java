package com.estafet.datasource.xml;

import java.io.File;

import com.estafet.datasource.initializer.IDataSourceInitializer;

public class XmlDataSourceInitializer implements IDataSourceInitializer{
	private final File xmlDataSourceFile;

	public XmlDataSourceInitializer(File xmlDataSourceFile) {
		this.xmlDataSourceFile = xmlDataSourceFile;
	}

	public File getXmlDataSourceFile() {
		return xmlDataSourceFile;
	}
}
