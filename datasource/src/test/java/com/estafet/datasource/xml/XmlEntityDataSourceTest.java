package com.estafet.datasource.xml;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.List;
import java.util.Optional;

import javax.xml.parsers.ParserConfigurationException;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;
import org.xml.sax.SAXException;

import com.estafet.costaexpress.datasource.entity.IEntityDataSource.EntitySearchAttributesEnum;
import com.estafet.costaexpress.model.entity.IPolicyAwareEntity;
import com.estafet.costaexpress.model.entity.UnknownEntityTypeException;
import com.estafet.costaexpress.model.entity.impl.AbstractCompositeEntity;
import com.estafet.costaexpress.model.entity.impl.GroupEntity;
import com.estafet.costaexpress.model.entity.impl.MachineEntity;
import com.estafet.costaexpress.model.entity.impl.SiteEntity;
import com.estafet.costaexpress.model.generated.pricelist.PriceTable;
import com.estafet.costaexpress.model.policy.impl.PriceListPolicy;
import com.estafet.datasource.DataSourceFailureException;
import com.estafet.datasource.initializer.DataSourceNotInitializedException;
import com.estafet.datasource.node.IDataSourceNode;
import com.estafet.datasource.query.QueryCriteria;


public class XmlEntityDataSourceTest {

	private XmlEntityDataSource dataSource;

	@Before
	public void setup() throws DataSourceNotInitializedException {
		String path = getClass().getClassLoader().getResource("costa-sample-hierarchy.xml").getPath();		
		 
		dataSource = new XmlEntityDataSource(new XmlDataSourceInitializer(new File(path)));
		dataSource.init();	
	}
	
	@Test
	public void findRootEntityTest() throws DataSourceFailureException, UnknownEntityTypeException {
		QueryCriteria criteria = new QueryCriteria();
		criteria.addCriteria(EntitySearchAttributesEnum.TYPE.value(), "group");
		criteria.addCriteria(EntitySearchAttributesEnum.NAME.value(), "Costa");		
		Optional<IPolicyAwareEntity> result = dataSource.find(criteria);
		
		assertTrue(result.isPresent());		
		assertTrue(result.get().isComposite());
		
		AbstractCompositeEntity entity = (AbstractCompositeEntity) result.get();
		
		assertEquals("group", entity.getType().value());
		assertEquals("Costa", entity.getName());
		
		assertEquals(3, entity.getSiblingEntities().size());
	}
	
	@Test
	public void findRandomGroupEntityTest() throws DataSourceFailureException, UnknownEntityTypeException {
		QueryCriteria criteria = new QueryCriteria();
		
		String GROUP = "group";
		String SHELL_MOTO = "Shell Moto";
		String NON_EXISTING = "Non existing";
				
		criteria.addCriteria(EntitySearchAttributesEnum.TYPE.value(), GROUP);		
		criteria.addCriteria(EntitySearchAttributesEnum.NAME.value(), SHELL_MOTO);
		
		
		Optional<IPolicyAwareEntity> result = dataSource.find(criteria);
		
		assertTrue(result.isPresent());		
		assertTrue(result.get().isComposite());
		AbstractCompositeEntity entity = (AbstractCompositeEntity) result.get();
		
		assertEquals(GROUP, entity.getType().value());
		assertEquals(SHELL_MOTO, entity.getName());
		
		assertEquals(4, entity.getSiblingEntities().size());
		
		criteria.addCriteria(EntitySearchAttributesEnum.NAME.value(), NON_EXISTING);
		
		Optional<IPolicyAwareEntity> nonexistingResult = dataSource.find(criteria);
		assertFalse(nonexistingResult.isPresent());
	}
	
	
	@Test
	public void findRandomSiteEntityTest() throws DataSourceFailureException, UnknownEntityTypeException {
		QueryCriteria criteria = new QueryCriteria();
		
		String SITE = "site";
		String SHELL_SITE_C = "Shell Site C";
				
		criteria.addCriteria(EntitySearchAttributesEnum.TYPE.value(), SITE);		
		criteria.addCriteria(EntitySearchAttributesEnum.NAME.value(), SHELL_SITE_C);
		
		
		Optional<IPolicyAwareEntity> result = dataSource.find(criteria);
		
		assertTrue(result.isPresent());
		assertTrue(result.get().isComposite());
		AbstractCompositeEntity entity = (AbstractCompositeEntity) result.get();
		
		assertEquals(SITE, entity.getType().value());
		assertEquals(SHELL_SITE_C, entity.getName());
		
		assertEquals(1, entity.getSiblingEntities().size());
	}
	
	@Test
	public void findRandomMachineEntityTest() throws DataSourceFailureException, UnknownEntityTypeException {
		QueryCriteria criteria = new QueryCriteria();
		
		String MACHINE = "machine";
		String MACINE_B3 = "Machine B3";
				
		criteria.addCriteria(EntitySearchAttributesEnum.TYPE.value(), MACHINE);		
		criteria.addCriteria(EntitySearchAttributesEnum.NAME.value(), MACINE_B3);
		
		
		Optional<IPolicyAwareEntity> result = dataSource.find(criteria);
		
		assertTrue(result.isPresent());
		assertFalse(result.get().isComposite());
		MachineEntity entity = (MachineEntity) result.get();		
		
		assertEquals(MACHINE, entity.getType().value());
		assertEquals(MACINE_B3, entity.getName());	
		
	}
	
	@Test 
	public void applyPolicyOnMacineTest() throws DataSourceFailureException, UnknownEntityTypeException {
		QueryCriteria criteria = new QueryCriteria();
		String MACHINE = "machine";
		String MACINE_B3 = "Machine B3";
				
		criteria.addCriteria(EntitySearchAttributesEnum.TYPE.value(), MACHINE);		
		criteria.addCriteria(EntitySearchAttributesEnum.NAME.value(), MACINE_B3);
		
		
		Optional<IPolicyAwareEntity> result = dataSource.find(criteria);
		
		assertTrue(result.isPresent());
		assertFalse(result.get().isComposite());
		MachineEntity entity = (MachineEntity) result.get();		
		
		assertEquals(MACHINE, entity.getType().value());
		assertEquals(MACINE_B3, entity.getName());	
		entity.applyPolicy(newPriceListPolicy("2.50"));
	}
	
	@Test 
	public void applyPolicyOnSiteTest() throws DataSourceFailureException, UnknownEntityTypeException {
		QueryCriteria criteria = new QueryCriteria();
		String SITE = "site";
		String SHELL_SITE_A = "Shell Site A";
				
		criteria.addCriteria(EntitySearchAttributesEnum.TYPE.value(), SITE);		
		criteria.addCriteria(EntitySearchAttributesEnum.NAME.value(), SHELL_SITE_A);
		
		
		Optional<IPolicyAwareEntity> result = dataSource.find(criteria);
		
		assertTrue(result.isPresent());
		assertTrue(result.get().isComposite());
		SiteEntity entity = (SiteEntity) result.get();		
		
		assertEquals(SITE, entity.getType().value());
		assertEquals(SHELL_SITE_A, entity.getName());	
		entity.applyPolicy(newPriceListPolicy("2.50"));
	}
	
	private PriceListPolicy newPriceListPolicy(String dobleShotPrice) {
		
		PriceTable priceTable = new PriceTable();
		priceTable.setIsOneShot(true);
		PriceTable.Price itemPrice = new PriceTable.Price();
		itemPrice.setDoubleShotPrice(new BigDecimal(dobleShotPrice));
		priceTable.getPrice().add(itemPrice);
		
		PriceListPolicy policy = new PriceListPolicy("name", "id", priceTable);
		return policy;
	}

	
	@Test
	public void findSiblingEntitiesTest() throws DataSourceFailureException, UnknownEntityTypeException {
		
		QueryCriteria criteria = new QueryCriteria();
		criteria.addCriteria(EntitySearchAttributesEnum.TYPE.value(), "group");
		criteria.addCriteria(EntitySearchAttributesEnum.NAME.value(), "Costa");
		criteria.addCriteria(EntitySearchAttributesEnum.CODE.value(), "Costa_id");
		IPolicyAwareEntity entity = new GroupEntity("Costa", "Costa_id");		
		
		assertNotNull(entity);		
		QueryCriteria siblingCriteria = newSiblingEntitiesQueryCriteria(entity);
		List<IPolicyAwareEntity> result = dataSource.findSiblingEntities(siblingCriteria);
		assertFalse(result.isEmpty());
		assertEquals(3, result.size());
		
	}

	@Test
	public void dataSourceInitializatoinTest() throws DataSourceNotInitializedException, DataSourceFailureException, UnknownEntityTypeException	{
		assertTrue(dataSource.ready());
		dataSource.close();
		assertFalse(dataSource.ready());
		
		Optional<IPolicyAwareEntity> result = dataSource.find(new QueryCriteria());		
		assertFalse(result.isPresent());
		List<IPolicyAwareEntity> siblingEntities = dataSource.findSiblingEntities(newSiblingEntitiesQueryCriteria(new GroupEntity("test", "testId")));
		assertEquals(0, siblingEntities.size());
		dataSource.reset();
		assertTrue(dataSource.ready());
	}
	
	@Test(expected=DataSourceNotInitializedException.class)
	public void dataSourceInitializationFailure1Test() throws DataSourceNotInitializedException, FileNotFoundException, ParserConfigurationException, SAXException, IOException {
		XmlEntityDataSource mockDataSource = Mockito.mock(XmlEntityDataSource.class);
		Mockito.when(mockDataSource.getDataSourceInitializer()).thenReturn(new XmlDataSourceInitializer(new File("datasource.xml")));

		Mockito.doCallRealMethod().when(mockDataSource).init();
		Mockito.when(mockDataSource.getSourcePath()).thenReturn("datasource.xml");
		Mockito.doThrow(new ParserConfigurationException("forced")).when(mockDataSource).initializeDataSource();
		mockDataSource.init();
	}
	
	@Test(expected=DataSourceNotInitializedException.class)
	public void dataSourceInitializationFailure2Test() throws DataSourceNotInitializedException, FileNotFoundException, ParserConfigurationException, SAXException, IOException {
		XmlEntityDataSource mockDataSource = Mockito.mock(XmlEntityDataSource.class);
		Mockito.when(mockDataSource.getDataSourceInitializer()).thenReturn(new XmlDataSourceInitializer(new File("datasource.xml")));
		Mockito.doCallRealMethod().when(mockDataSource).init();
		Mockito.when(mockDataSource.getSourcePath()).thenReturn("datasource.xml");
		Mockito.doThrow(new FileNotFoundException("forced")).when(mockDataSource).initializeDataSource();		
		mockDataSource.init();
	}
	
	@Test(expected=DataSourceNotInitializedException.class)
	public void dataSourceInitializationFailure3Test() throws DataSourceNotInitializedException, FileNotFoundException, ParserConfigurationException, SAXException, IOException {
		XmlEntityDataSource mockDataSource = Mockito.mock(XmlEntityDataSource.class);
		Mockito.when(mockDataSource.getDataSourceInitializer()).thenReturn(new XmlDataSourceInitializer(new File("datasource.xml")));
		Mockito.doCallRealMethod().when(mockDataSource).init();
		Mockito.when(mockDataSource.getSourcePath()).thenReturn("datasource.xml");
		Mockito.doThrow(new SAXException("forced")).when(mockDataSource).initializeDataSource();
		mockDataSource.init();
	}
	
	@Test(expected=DataSourceNotInitializedException.class)
	public void dataSourceInitializationFailure4Test() throws DataSourceNotInitializedException, FileNotFoundException, ParserConfigurationException, SAXException, IOException {
		XmlEntityDataSource mockDataSource = Mockito.mock(XmlEntityDataSource.class);
		Mockito.when(mockDataSource.getDataSourceInitializer()).thenReturn(new XmlDataSourceInitializer(new File("datasource.xml")));
		Mockito.doCallRealMethod().when(mockDataSource).init();
		Mockito.when(mockDataSource.getSourcePath()).thenReturn("datasource.xml");
		Mockito.doThrow(new IOException("forced")).when(mockDataSource).initializeDataSource();
		mockDataSource.init();
	}
	
	private QueryCriteria newSiblingEntitiesQueryCriteria(IDataSourceNode node){
		QueryCriteria criteria = new QueryCriteria();
		if(node != null){
			if(node.getId() != null && !node.getId().isEmpty()){
				criteria.addCriteria(EntitySearchAttributesEnum.CODE.value(), node.getId());
			}
			if(node.getNodeType() != null && !node.getNodeType().isEmpty()){
				criteria.addCriteria(EntitySearchAttributesEnum.TYPE.value(), node.getNodeType());
			}
			if(node.getNodeName() != null && !node.getNodeName().isEmpty()){
				criteria.addCriteria(EntitySearchAttributesEnum.NAME.value(), node.getNodeName());
			}
		}
		criteria.setQueryForSiblingEntities();
		return criteria;
	}
}
