package com.estafet.costaexpress.datasource.statement.neo4j;

import static org.junit.Assert.assertEquals;

import java.math.BigDecimal;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.TimeZone;

import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import com.estafet.costaexpress.model.generated.pricelist.PriceTable;
import com.estafet.costaexpress.model.generated.pricelist.PriceTable.Price;
import com.estafet.costaexpress.model.policy.impl.PriceListPolicy;

public class CreatePriceListPolicyCypherStatementTest {

	private static final String CREATE_PRICELIST_STATEMENT = "CREATE (list:pricelist {code: \"test\", name: \"test\"}) CREATE (table:pricetable {code:\"test\", isOneShot: \"true\", effectiveDate: \"3917-03-17T00:00:00.000+02:00\"}) CREATE (price0:price {ProdAuditId:\"id1\", RegularPrice:\"1.5\", LargePrice:\"1.5\", SyrupPrice:\"1.4\", DoubleShotPrice:\"1.0\", WithFixedSyrupPrice:\"1.7\"}) CREATE (price1:price {ProdAuditId:\"id2\", RegularPrice:\"2.5\", LargePrice:\"2.5\", SyrupPrice:\"2.4\", DoubleShotPrice:\"2.0\", WithFixedSyrupPrice:\"2.7\"}) CREATE (price2:price {ProdAuditId:\"id3\", RegularPrice:\"3.5\", LargePrice:\"3.0\", SyrupPrice:\"3.4\", DoubleShotPrice:\"3.0\", WithFixedSyrupPrice:\"3.7\"})CREATE UNIQUE (list)-[:uses]-(table)-[:has]-(price0)CREATE UNIQUE (list)-[:uses]-(table)-[:has]-(price1)CREATE UNIQUE (list)-[:uses]-(table)-[:has]-(price2)";


	private CreatePriceListPolicyCypherStatement instance;
	
	@Before
	public void setup() {
		instance = new CreatePriceListPolicyCypherStatement(null);
	}
	@Ignore
	@Test
	public void prepareStatementTest() throws DatatypeConfigurationException {
		PriceTable priceTable = newPriceTable();
		PriceListPolicy policy = new PriceListPolicy("test", "test", priceTable);
		String statement = instance.prepareStatement(policy);
		System.out.println(statement);
		assertEquals(CREATE_PRICELIST_STATEMENT, statement);
	}
	
	@SuppressWarnings("deprecation")
	private PriceTable newPriceTable() throws DatatypeConfigurationException {
		PriceTable table = new PriceTable();
		table.setIsOneShot(true);
		GregorianCalendar calendar = new GregorianCalendar();
		calendar.setTimeZone(TimeZone.getTimeZone("GMT+02:00"));
		calendar.setTimeInMillis(new Date(2017, 2, 17 ).getTime());
		XMLGregorianCalendar effectiveDate = DatatypeFactory.newInstance().newXMLGregorianCalendar(calendar);
		
		table.setEffectiveDate(effectiveDate);
		
		Price price1 = new Price();
		Price price2 = new Price();
		Price price3 = new Price();
				
		price1.setDoubleShotPrice(new BigDecimal("1.0"));
		price1.setRegularPrice(new BigDecimal("1.5"));
		price1.setLargePrice(new BigDecimal("1.5"));
		price1.setSyrupPrice(new BigDecimal("1.4"));
		price1.setWithFixedSyrupPrice(new BigDecimal("1.7"));
		price1.setProdAuditId("id1");
		
		price2.setDoubleShotPrice(new BigDecimal("2.0"));
		price2.setRegularPrice(new BigDecimal("2.5"));
		price2.setLargePrice(new BigDecimal("2.5"));
		price2.setSyrupPrice(new BigDecimal("2.4"));
		price2.setWithFixedSyrupPrice(new BigDecimal("2.7"));
		price2.setProdAuditId("id2");
		
		price3.setDoubleShotPrice(new BigDecimal("3.0"));
		price3.setRegularPrice(new BigDecimal("3.5"));
		price3.setLargePrice(new BigDecimal("3.0"));
		price3.setSyrupPrice(new BigDecimal("3.4"));
		price3.setWithFixedSyrupPrice(new BigDecimal("3.7"));
		price3.setProdAuditId("id3");
		
		
		
		table.getPrice().add(price1);		
		table.getPrice().add(price2);		
		table.getPrice().add(price3);
		return table;
	}
}
