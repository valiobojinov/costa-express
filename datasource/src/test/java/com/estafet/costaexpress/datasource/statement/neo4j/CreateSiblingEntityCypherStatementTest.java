package com.estafet.costaexpress.datasource.statement.neo4j;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;
import org.neo4j.driver.v1.StatementResult;
import org.neo4j.driver.v1.exceptions.ClientException;

import com.estafet.costaexpress.model.entity.impl.GroupEntity;
import com.estafet.datasource.statement.StatementFailedException;

public class CreateSiblingEntityCypherStatementTest {
	private CreateSiblingEntityCypherStatement instance;
	private GroupEntity entity;
	private StatementResult mockResult;

	
	private static final String CREATE_SIBLING_STATEMENT = "CREATE (x:group {code:\"ID1\", name:\"New group\"}) WITH x MATCH (y:group{code:\"ID1\"}) CREATE UNIQUE (y)-[:manages]-> (x)";
	
	@Before
	public void setup() {
		instance = new CreateSiblingEntityCypherStatement(null, "ID1", "group", "manages");
		mockResult = Mockito.mock(StatementResult.class);
		entity = new GroupEntity("New group", "ID1");
	}
	
	@Test
	public void prepareStatementTest() {
		String statement = instance.prepareStatement(entity);
		System.out.println(statement);
		assertEquals(CREATE_SIBLING_STATEMENT, statement);
	}
	
	@Test(expected = StatementFailedException.class)
	public void checkFailedStatementTest() throws StatementFailedException {
		Mockito.when(mockResult.list()).thenThrow(new ClientException("forced"));
		instance.checkStatementResult(mockResult);
	}
	
	@Test
	public void checkStatementTest() throws StatementFailedException {
		instance.checkStatementResult(mockResult);
	}
}
