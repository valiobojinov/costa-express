package com.estafet.costaexpress.datasource.statement.neo4j;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;
import org.neo4j.driver.v1.StatementResult;
import org.neo4j.driver.v1.exceptions.ClientException;
import org.neo4j.driver.v1.summary.ResultSummary;
import org.neo4j.driver.v1.summary.SummaryCounters;

import com.estafet.costaexpress.model.entity.types.EntityTypesEnum;
import com.estafet.datasource.statement.StatementFailedException;

public class CreateConnectionEntityCypherStatementTest {
	
	private CreateConnectionEntityCypherStatement instance;
	
	private StatementResult mockResult;
	private ResultSummary mockSummary;
    private SummaryCounters mockCoutners;
	
	private static final String CREATE_CONNECTION_STATEMENT = "MATCH (policy:ID1{code:'group'}) MATCH (entity {code:'ID2'}) CREATE UNIQUE (policy)-[:manages]->(entity)";
	
	@Before
	public void setup() {
		instance = new CreateConnectionEntityCypherStatement(null, EntityTypesEnum.GROUP.value(), "ID1", "manages");
		
		mockResult = Mockito.mock(StatementResult.class);
		mockSummary = Mockito.mock(ResultSummary.class);
	    mockCoutners = Mockito.mock(SummaryCounters.class);
	    
		Mockito.when(mockResult.summary()).thenReturn(mockSummary);
		Mockito.when(mockSummary.counters()).thenReturn(mockCoutners);
	}
	
	@Test
	public void prepareStatementTest() {
		String statement = instance.prepareStatement("ID2");
		System.out.println(statement);
		assertEquals(CREATE_CONNECTION_STATEMENT, statement);
	}
	
	@Test
	public void checkSuccessfulStatementResultTest() throws StatementFailedException {
		
		Mockito.when(mockCoutners.relationshipsCreated()).thenReturn(1);
		instance.checkStatementResult(mockResult);
	}
	
	@Test(expected=StatementFailedException.class)	
	public void checkNotFoundStatementResultTest() throws StatementFailedException {
		
		Mockito.when(mockCoutners.relationshipsCreated()).thenReturn(0);
		instance.checkStatementResult(mockResult);
	}
	
	@Test(expected=StatementFailedException.class)	
	public void checkFailedStatementResultTest() throws StatementFailedException   {
		
		Mockito.when(mockResult.summary()).thenThrow(new ClientException("forced"));
		instance.checkStatementResult(mockResult);
	}
}
