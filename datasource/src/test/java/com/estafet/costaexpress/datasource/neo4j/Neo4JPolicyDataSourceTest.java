package com.estafet.costaexpress.datasource.neo4j;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Optional;

import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;

import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import com.estafet.costaexpress.datasource.entity.EntityAlreadyExistsException;
import com.estafet.costaexpress.datasource.policy.PolicyAlreadyExistsException;
import com.estafet.costaexpress.datasource.policy.PolicyNotFoundException;
import com.estafet.costaexpress.model.entity.IPolicyAwareEntity;
import com.estafet.costaexpress.model.entity.impl.GroupEntity;
import com.estafet.costaexpress.model.entity.types.EntityTypesEnum;
import com.estafet.costaexpress.model.generated.pricelist.PriceTable;
import com.estafet.costaexpress.model.generated.pricelist.PriceTable.Price;
import com.estafet.costaexpress.model.policy.IPolicy;
import com.estafet.costaexpress.model.policy.impl.PriceListPolicy;
import com.estafet.costaexpress.model.policy.types.PolicyTypesEnum;
import com.estafet.costaexpress.model.relation.OneToOneRelation;
import com.estafet.costaexpress.test.tools.EmbeddedNeo4JDataSource;
import com.estafet.datasource.DataSourceFailureException;
import com.estafet.datasource.initializer.DataSourceNotInitializedException;
import com.estafet.datasource.neo4j.Neo4JDataSourceInitializer;
import com.estafet.datasource.query.QueryCriteria;



public class Neo4JPolicyDataSourceTest {
	
	private static EmbeddedNeo4JDataSource<Neo4JEntityDataSource, Neo4JPolicyDataSource> dataSource = new EmbeddedNeo4JDataSource<>();
	
	@BeforeClass
	public static void startDatabase() throws IOException {		 
		dataSource.startDatabase();
	}

	@Before
	public void init(){
		Neo4JDataSourceInitializer initializer = new Neo4JDataSourceInitializer(EmbeddedNeo4JDataSource.DB_PORT, "localhost", "admin", "neo4j");
		dataSource.setEntityInstance(new Neo4JEntityDataSource(initializer));
		dataSource.setPolicyInstance(new Neo4JPolicyDataSource(initializer));
	}
		
	@Test
	public void findPolicyForEntityTest() throws DataSourceFailureException, DataSourceNotInitializedException, EntityAlreadyExistsException, PolicyAlreadyExistsException, PolicyNotFoundException, ParseException, DatatypeConfigurationException{
		dataSource.getEntityInstance().init();
		dataSource.getPolicyInstance().init();
		assertTrue(dataSource.getEntityInstance().ready());
		assertTrue(dataSource.getPolicyInstance().ready());
		
		GroupEntity test = new GroupEntity("findPolicyForEntityTest", "findPolicyForEntityTestId");
		dataSource.getEntityInstance().create(test);
		Optional<IPolicyAwareEntity> entity = dataSource.getEntityInstance().find(Neo4JEntityDataSourceTest.newQueryCriteriaFromNode(test));
		assertTrue(entity.isPresent());
		assertTrue(entity.get() instanceof GroupEntity);
		GroupEntity group = (GroupEntity)entity.get();
		assertEquals("findPolicyForEntityTest", group.getNodeName());
		assertEquals("findPolicyForEntityTest", group.getName());
		assertEquals("findPolicyForEntityTestId", group.getId());		
		GroupEntity child = new GroupEntity("findPolicyForEntityTestchildGroup", "findPolicyForEntityTestchildGroupId");
		
		dataSource.getEntityInstance().createSibling(child, "findPolicyForEntityTestId", EntityTypesEnum.GROUP.value());
		
		PriceTable priceTable = new PriceTable();
		priceTable.setIsOneShot(true);
		GregorianCalendar effectiveDateCalendar = new GregorianCalendar();	
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSXXX");
		Date date = dateFormat.parse("2017-02-17T14:04:40.772+02:00");
		effectiveDateCalendar.setTime(date);
		XMLGregorianCalendar calendar =  DatatypeFactory.newInstance().newXMLGregorianCalendar(effectiveDateCalendar);		
		priceTable.setEffectiveDate(calendar);
		Price price = new PriceTable.Price();
		price.setDoubleShotPrice(new BigDecimal("1.80"));
		price.setLargePrice(new BigDecimal("1.10"));
		price.setProdAuditId("Drink1");
		price.setRegularPrice(new BigDecimal("1.00"));
		price.setSyrupPrice(new BigDecimal("0.90"));
		price.setWithFixedSyrupPrice(new BigDecimal("0.99"));
		priceTable.getPrice().add(price);
		
		PriceListPolicy priceList = new PriceListPolicy("findPolicyForEntityTest", "findPolicyForEntityTest", priceTable);
		dataSource.getPolicyInstance().create(priceList);
		
		Optional<IPolicy> policy = dataSource.getPolicyInstance().find(Neo4JEntityDataSourceTest.newQueryCriteriaFromNode(priceList));
		assertTrue(policy.isPresent());
		dataSource.getPolicyInstance().applyPolicy(policy.get().getId(), policy.get().getPolicyType().value(), group.getId());

		QueryCriteria nodeCrteria = Neo4JEntityDataSourceTest.newQueryCriteriaFromNode(group);
		Optional<OneToOneRelation> result = dataSource.getPolicyInstance().findPolicyForEntity(nodeCrteria, dataSource.getEntityInstance());


		assertTrue(result.isPresent());

	}
	
	@Test
	public void createFindPolicyTest() throws DataSourceFailureException, DataSourceNotInitializedException, PolicyAlreadyExistsException, ParseException, DatatypeConfigurationException  {
		dataSource.getEntityInstance().init();
		dataSource.getPolicyInstance().init();
		assertTrue(dataSource.getEntityInstance().ready());
		assertTrue(dataSource.getPolicyInstance().ready());
		
		PriceTable priceTable = new PriceTable();
		priceTable.setIsOneShot(true);
		GregorianCalendar effectiveDateCalendar = new GregorianCalendar();	
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSXXX");
		Date date = dateFormat.parse("2017-02-17T14:04:40.772+02:00");
		effectiveDateCalendar.setTime(date);
		XMLGregorianCalendar calendar =  DatatypeFactory.newInstance().newXMLGregorianCalendar(effectiveDateCalendar);		
		priceTable.setEffectiveDate(calendar);
		Price price = new PriceTable.Price();
		price.setDoubleShotPrice(new BigDecimal("1.80"));
		price.setLargePrice(new BigDecimal("1.10"));
		price.setProdAuditId("Drink1");
		price.setRegularPrice(new BigDecimal("1.00"));
		price.setSyrupPrice(new BigDecimal("0.90"));
		price.setWithFixedSyrupPrice(new BigDecimal("0.99"));
		priceTable.getPrice().add(price);
		
		PriceListPolicy test = new PriceListPolicy("createFindPolicyTest", "createFindPolicyTest", priceTable);
		dataSource.getPolicyInstance().create(test);
		Optional<IPolicy> entity = dataSource.getPolicyInstance().find(Neo4JEntityDataSourceTest.newQueryCriteriaFromNode(test));
		assertTrue(entity.isPresent());
		assertTrue(entity.get() instanceof PriceListPolicy);
		PriceListPolicy priceList = (PriceListPolicy)entity.get();
		assertEquals("createFindPolicyTest", priceList.getNodeName());
		
		assertEquals("createFindPolicyTest", priceList.getId());
		assertEquals(PolicyTypesEnum.PRICELIST, priceList.getPolicyType());
		assertEquals(PolicyTypesEnum.PRICELIST.value(), priceList.getNodeType());
	}
	
	@AfterClass
	public static void afterTestMethod() throws IOException{
		dataSource.shutdownDatabase();
	}

}
