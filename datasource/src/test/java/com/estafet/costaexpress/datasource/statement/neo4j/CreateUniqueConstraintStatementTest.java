package com.estafet.costaexpress.datasource.statement.neo4j;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

public class CreateUniqueConstraintStatementTest {
	
	private static final String CREATE_CONSTRAINT = "CREATE CONSTRAINT ON (e:group) ASSERT e.code IS UNIQUE";
	private CreateUniqueConstraintStatement instance;

	@Before
	public void setup() {
		instance = new CreateUniqueConstraintStatement(null);

	}
	
	@Test
	public void prepareStatementTest() {
		String statement = instance.prepareStatement("group");
		System.out.println(statement);
		assertEquals(CREATE_CONSTRAINT, statement);
	}
}
