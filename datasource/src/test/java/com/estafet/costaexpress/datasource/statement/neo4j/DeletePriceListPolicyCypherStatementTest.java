package com.estafet.costaexpress.datasource.statement.neo4j;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;
import org.neo4j.driver.v1.StatementResult;
import org.neo4j.driver.v1.exceptions.ClientException;
import org.neo4j.driver.v1.summary.ResultSummary;
import org.neo4j.driver.v1.summary.SummaryCounters;

import com.estafet.datasource.statement.StatementFailedException;

public class DeletePriceListPolicyCypherStatementTest {
	private static final String DELETE_POLICY_STATEMENT = "MATCH (pricelist{code: 'test-123'}) OPTIONAL MATCH (pricelist)-[uses_connection:uses]-(pricetable)-[has_connection:has]-(price) DETACH DELETE uses_connection, has_connection,  pricelist, pricetable, price";
	private DeletePriceListPolicyCypherStatement instance;
	
	private StatementResult mockResult;
	private ResultSummary mockSummary;
    private SummaryCounters mockCoutners;
	
	@Before
	public void setup() {
		instance = new DeletePriceListPolicyCypherStatement(null);
		
		mockResult = Mockito.mock(StatementResult.class);
		mockSummary = Mockito.mock(ResultSummary.class);
	    mockCoutners = Mockito.mock(SummaryCounters.class);
	    
		Mockito.when(mockResult.summary()).thenReturn(mockSummary);
		Mockito.when(mockSummary.counters()).thenReturn(mockCoutners);
	}
	
	@Test
	public void prepareStatementTest() {
		String statement = instance.prepareStatement("test-123");
		System.out.println(statement);
		assertEquals(DELETE_POLICY_STATEMENT, statement);
	}
	
	@Test(expected=StatementFailedException.class)	
	public void checkNotFoundStatementResultTest() throws StatementFailedException {
		
		Mockito.when(mockCoutners.nodesDeleted()).thenReturn(0);
		instance.checkStatementResult(mockResult);
	}
	
	@Test(expected=StatementFailedException.class)	
	public void checkFailedStatementResultTest() throws StatementFailedException   {
		
		Mockito.when(mockResult.summary()).thenThrow(new ClientException("forced"));
		instance.checkStatementResult(mockResult);
	}
}
