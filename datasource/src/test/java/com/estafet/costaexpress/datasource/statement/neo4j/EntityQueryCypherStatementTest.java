package com.estafet.costaexpress.datasource.statement.neo4j;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;
import org.neo4j.driver.v1.Record;
import org.neo4j.driver.v1.StatementResult;
import org.neo4j.driver.v1.Value;
import org.neo4j.driver.v1.types.Node;

import com.estafet.costaexpress.datasource.entity.EntityNotCreatedException;
import com.estafet.costaexpress.model.entity.IPolicyAwareEntity;
import com.estafet.datasource.query.QueryCriteria;

public class EntityQueryCypherStatementTest {
	private AbstractEntityQueryCypherStatement instance;
	
	@Before
	public void setup() {
		instance = Mockito.mock(AbstractEntityQueryCypherStatement.class);
	}
	
	@Test
	public void queryTest() throws EntityNotCreatedException {
		StatementResult result = Mockito.mock(StatementResult.class);
		
		Record record = Mockito.mock(Record.class);
		Value n = Mockito.mock(Value.class);
		Value name = Mockito.mock(Value.class);
		Value code = Mockito.mock(Value.class);
		Mockito.when(name.asString()).thenReturn("Name");
		Mockito.when(code.asString()).thenReturn("Code");
		
		Mockito.when(n.get("name")).thenReturn(name);
		Mockito.when(n.get("code")).thenReturn(code);
		
		
		List<Record> recordList = new ArrayList<Record>();
		Mockito.when(result.list()).thenReturn(recordList);
		recordList.add(record);
		
		Mockito.when(record.get(Mockito.anyString())).thenReturn(n);
		Node node = Mockito.mock(Node.class);
		Mockito.when(n.asNode()).thenReturn(node);
		Mockito.when(node.labels()).thenReturn(new Iterable<String>() {
			
			@Override
			public Iterator<String> iterator() {
				return new ArrayList<String>(Arrays.<String>asList(new String[]{"group"})).iterator();
			}
		});
		
		Mockito.when(instance.query(Mockito.any(QueryCriteria.class))).thenCallRealMethod();
		Mockito.when(instance.run(Mockito.anyString())).thenReturn(result);
		Mockito.when(instance.toSingleEntity(Mockito.any(StatementResult.class))).thenCallRealMethod();
		
		Map<String, Map<String, String>> statementFields = new HashMap<String, Map<String, String>>();
		Map<String, String> fields = new HashMap<String, String>();
		fields.put(AbstractEntityQueryCypherStatement.CODE, "code");
		fields.put(AbstractEntityQueryCypherStatement.NAME, "name");
		statementFields.put("n", fields);
		
		Mockito.when(instance.getStatementFields()).thenReturn(statementFields);
		
		IPolicyAwareEntity entity = instance.query(new QueryCriteria());
		
		assertEquals("Name", entity.getName());
		
		
	}
	
	@Test(expected=EntityNotCreatedException.class)
	public void queryUnknownTest() throws EntityNotCreatedException {
		StatementResult result = Mockito.mock(StatementResult.class);
		
		Record record = Mockito.mock(Record.class);
		Value n = Mockito.mock(Value.class);
		Value name = Mockito.mock(Value.class);
		Value code = Mockito.mock(Value.class);
		Mockito.when(name.asString()).thenReturn("Name");
		Mockito.when(code.asString()).thenReturn("Code");
		
		Mockito.when(n.get("name")).thenReturn(name);
		Mockito.when(n.get("code")).thenReturn(code);
		
		
		List<Record> recordList = new ArrayList<Record>();
		Mockito.when(result.list()).thenReturn(recordList);
		recordList.add(record);
		
		Mockito.when(record.get(Mockito.anyString())).thenReturn(n);
		Node node = Mockito.mock(Node.class);
		Mockito.when(n.asNode()).thenReturn(node);
		Mockito.when(node.labels()).thenReturn(new Iterable<String>() {
			
			@Override
			public Iterator<String> iterator() {
				return new ArrayList<String>(Arrays.<String>asList(new String[]{"unknown"})).iterator();
			}
		});
		
		Mockito.when(instance.query(Mockito.any(QueryCriteria.class))).thenCallRealMethod();
		Mockito.when(instance.run(Mockito.anyString())).thenReturn(result);
		Mockito.when(instance.toSingleEntity(Mockito.any(StatementResult.class))).thenCallRealMethod();
		
		Map<String, Map<String, String>> statementFields = new HashMap<String, Map<String, String>>();
		Map<String, String> fields = new HashMap<String, String>();
		fields.put(AbstractEntityQueryCypherStatement.CODE, "code");
		fields.put(AbstractEntityQueryCypherStatement.NAME, "name");
		statementFields.put("n", fields);
		
		Mockito.when(instance.getStatementFields()).thenReturn(statementFields);
		
		instance.query(new QueryCriteria());
	}
	
	@Test
	public void toSingleEntityTest() throws EntityNotCreatedException {
		Mockito.when(instance.toSingleEntity(Mockito.any(StatementResult.class))).thenCallRealMethod();
		
		Map<String, Map<String, String>> statementFields = new HashMap<String, Map<String, String>>();
		Map<String, String> fields = new HashMap<String, String>();
		fields.put(AbstractEntityQueryCypherStatement.CODE, "code");
		fields.put(AbstractEntityQueryCypherStatement.NAME, "name");
		statementFields.put("n", fields);
		
		Mockito.when(instance.getStatementFields()).thenReturn(statementFields);
		
		StatementResult mockStatementResult = Mockito.mock(StatementResult.class);
		List<Record> list = new ArrayList<Record>();
		Record mockRecord = Mockito.mock(Record.class);
		
		Value mockValue = Mockito.mock(Value.class);

		
		Value mockCodeValue = Mockito.mock(Value.class);
		Mockito.when(mockCodeValue.asString()).thenReturn("code");
		Mockito.when(mockValue.get(AbstractEntityQueryCypherStatement.CODE)).thenReturn(mockCodeValue);
		
		
		Value mockNameValue = Mockito.mock(Value.class);
		Mockito.when(mockNameValue.asString()).thenReturn("name");
		Mockito.when(mockValue.get(AbstractEntityQueryCypherStatement.NAME)).thenReturn(mockNameValue);

		Node mockNode = Mockito.mock(Node.class);
		Mockito.when(mockValue.asNode()).thenReturn(mockNode);
		
		Mockito.when(mockNode.labels()).thenReturn(new Iterable<String>() {
			
			@Override
			public Iterator<String> iterator() {
				return new ArrayList<String>(Arrays.<String>asList(new String[]{"group"})).iterator();
			}
		});
		
		
		
		Mockito.when(mockRecord.get("n")).thenReturn(mockValue);
		list.add(mockRecord);
		Mockito.when(mockStatementResult.list()).thenReturn(list);
		
		
		
		IPolicyAwareEntity entity = instance.toSingleEntity(mockStatementResult);
		System.out.println(entity.getClass());
	}
}
