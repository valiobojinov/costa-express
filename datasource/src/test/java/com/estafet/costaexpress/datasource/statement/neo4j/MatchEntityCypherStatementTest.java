package com.estafet.costaexpress.datasource.statement.neo4j;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import com.estafet.costaexpress.datasource.entity.IEntityDataSource.EntitySearchAttributesEnum;
import com.estafet.costaexpress.model.entity.types.EntityTypesEnum;
import com.estafet.datasource.neo4j.Neo4JDataSource;
import com.estafet.datasource.query.QueryCriteria;


public class MatchEntityCypherStatementTest {
	private MatchEntityQueryCypherStatement instance;
	private QueryCriteria criteria;
	private static final String MATCH_STATEMENT = "MATCH (n:group) WHERE n.code='N1234' return n";
	
	@Before
	public void setup() {
		instance = new MatchEntityQueryCypherStatement(Mockito.mock(Neo4JDataSource.class));
		criteria = new QueryCriteria();
		criteria.addCriteria(EntitySearchAttributesEnum.CODE.value(), "N1234");
		criteria.addCriteria(EntitySearchAttributesEnum.TYPE.value(), EntityTypesEnum.GROUP.value());
		
	}
	
	@Test
	public void prepareStatementTest() {
		String statement = instance.prepareStatement(criteria);
		System.out.println(statement);
		assertEquals(MATCH_STATEMENT, statement);
	}

}
