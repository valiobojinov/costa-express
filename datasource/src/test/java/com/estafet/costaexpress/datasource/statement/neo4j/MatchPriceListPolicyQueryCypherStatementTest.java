package com.estafet.costaexpress.datasource.statement.neo4j;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.math.BigDecimal;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

import javax.xml.datatype.DatatypeConfigurationException;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;
import org.neo4j.driver.v1.Record;
import org.neo4j.driver.v1.Value;

import com.estafet.costaexpress.datasource.policy.IPolicyDataSource.PolicySearchAttributesEnum;
import com.estafet.costaexpress.datasource.policy.PolicyNotCreatedException;
import com.estafet.costaexpress.model.generated.pricelist.PriceTable;
import com.estafet.costaexpress.model.policy.types.PolicyTypesEnum;
import com.estafet.datasource.neo4j.Neo4JDataSource;
import com.estafet.datasource.query.QueryCriteria;


public class MatchPriceListPolicyQueryCypherStatementTest {
	private MatchPriceListPolicyQueryCypherStatement instance;
	private QueryCriteria criteria;
	private static final String MATCH_STATEMENT = "MATCH (p:pricelist{code:'N1234'})-[:uses]->(t:pricetable)<-[:has]-(i:price) RETURN p, t, i";
	
	private static final String CREATED_DATE = "2017-02-17T14:04:40.772+02:00";
	private static final String IS_ONE_SHOT = "true";
	
	private static final String PROD_AUTID_ID_1 = "PROD_AUTID_ID_1";
	private static final String PROD_AUTID_ID_2 = "PROD_AUTID_ID_2";
	private static final String PROD_AUTID_ID_3 = "PROD_AUTID_ID_3";
	
	private static final String DOUBLE_SHOT_PRICE_1 = new Double("1.8").toString();
	private static final String DOUBLE_SHOT_PRICE_2 = new Double("1.6").toString();
	private static final String DOUBLE_SHOT_PRICE_3 = new Double("1.4").toString();
	
	private static final String SYRUP_PRICE_1 = new Double("0.8").toString();
	private static final String SYRUP_PRICE_2 = new Double("0.6").toString();
	private static final String SYRUP_PRICE_3 = new Double("0.3").toString();
	
	private static final String WITH_FIXED_SYRUP_PRICE_1 = new Double("1.1").toString();
	private static final String WITH_FIXED_SYRUP_PRICE_2 = new Double("1.3").toString();
	private static final String WITH_FIXED_SYRUP_PRICE_3 = new Double("1.5").toString();
	
	private static final String LARGE_PRICE_1 = new Double("2.3").toString();
	private static final String LARGE_PRICE_2 = new Double("2.5").toString();
	private static final String LARGE_PRICE_3 = new Double("2.7").toString();
	
	private static final String REGULAR_PRICE_1 = new Double("1.25").toString();
	private static final String REGULAR_PRICE_2 = new Double("1.35").toString();
	private static final String REGULAR_PRICE_3 = new Double("1.55").toString();



	
	
	
	@Before
	public void setup() {
		instance = new MatchPriceListPolicyQueryCypherStatement(Mockito.mock(Neo4JDataSource.class));
		criteria = new QueryCriteria();
		criteria.addCriteria(PolicySearchAttributesEnum.CODE.value(), "N1234");
		criteria.addCriteria(PolicySearchAttributesEnum.TYPE.value(), PolicyTypesEnum.PRICELIST.value());
		criteria.addCriteria(PolicySearchAttributesEnum.ITEM.value(), "price");
		criteria.addCriteria(PolicySearchAttributesEnum.ITEM_TYPE.value(), "pricetable");
		
	}
	
	@Test
	public void prepareStatementTest() {
		String statement = instance.prepareStatement(criteria);
		System.out.println(statement);
		assertEquals(MATCH_STATEMENT, statement);
	}
	
	@Test	
	public void toPolicyItemTest() throws PolicyNotCreatedException {
		List<Record> list = new ArrayList<Record>();
		
		Record record1 = Mockito.mock(Record.class);
		Record record2 = Mockito.mock(Record.class);
		Record record3 = Mockito.mock(Record.class);
		
		Value value1 = Mockito.mock(Value.class);
		Value value2 = Mockito.mock(Value.class);
		Value value3 = Mockito.mock(Value.class);
		
		list.add(record1);
		list.add(record2);
		list.add(record3);
		
		Value tableValue = Mockito.mock(Value.class);		
		
		Mockito.when(record1.get(MatchPriceListPolicyQueryCypherStatement.TABLE_INDEX)).thenReturn(tableValue);
		
		
		Value isOneShotValue = Mockito.mock(Value.class);		
		Mockito.when(isOneShotValue.asString()).thenReturn(IS_ONE_SHOT);
		
		Value effectiveDateValue = Mockito.mock(Value.class);
		Mockito.when(effectiveDateValue.asString()).thenReturn(CREATED_DATE);
		
		Mockito.when(tableValue.get(MatchPriceListPolicyQueryCypherStatement.PRICE_TABLE_FIELD_IS_ONE_SHOT)).thenReturn(isOneShotValue);		
		Mockito.when(tableValue.get(MatchPriceListPolicyQueryCypherStatement.PRICE_TABLE_EFFECTIVE_DATE)).thenReturn(effectiveDateValue);
		
		Mockito.when(record1.get(MatchPriceListPolicyQueryCypherStatement.ITEM_INDEX)).thenReturn(value1);
		Mockito.when(record2.get(MatchPriceListPolicyQueryCypherStatement.ITEM_INDEX)).thenReturn(value2);
		Mockito.when(record3.get(MatchPriceListPolicyQueryCypherStatement.ITEM_INDEX)).thenReturn(value3);
		
		Value prodAutId1 = Mockito.mock(Value.class);
		Mockito.when(prodAutId1.asString()).thenReturn(PROD_AUTID_ID_1);
		Mockito.when(value1.get(MatchPriceListPolicyQueryCypherStatement.PROD_AUTID_ID)).thenReturn(prodAutId1);
		
		Value doubleShotPrice1 = Mockito.mock(Value.class);
		Mockito.when(doubleShotPrice1.asString()).thenReturn(DOUBLE_SHOT_PRICE_1);
		Mockito.when(value1.get(MatchPriceListPolicyQueryCypherStatement.DOUBLE_SHOT_PRICE)).thenReturn(doubleShotPrice1);

		Value withFixedSyrupPrice1 = Mockito.mock(Value.class);
		Mockito.when(withFixedSyrupPrice1.asString()).thenReturn(WITH_FIXED_SYRUP_PRICE_1);
		Mockito.when(value1.get(MatchPriceListPolicyQueryCypherStatement.WITH_FIXED_SYRUP_PRICE)).thenReturn(withFixedSyrupPrice1);

		
		Value syrupPrice1 = Mockito.mock(Value.class);
		Mockito.when(syrupPrice1.asString()).thenReturn(SYRUP_PRICE_1);
		Mockito.when(value1.get(MatchPriceListPolicyQueryCypherStatement.SYRUP_PRICE)).thenReturn(syrupPrice1);
		
		Value largePrice1 = Mockito.mock(Value.class);
		Mockito.when(largePrice1.asString()).thenReturn(LARGE_PRICE_1);
		Mockito.when(value1.get(MatchPriceListPolicyQueryCypherStatement.LARGE_PRICE)).thenReturn(largePrice1);
		
		Value regularPrice1 = Mockito.mock(Value.class);
		Mockito.when(regularPrice1.asString()).thenReturn(REGULAR_PRICE_1);
		Mockito.when(value1.get(MatchPriceListPolicyQueryCypherStatement.REGULAR_PRICE)).thenReturn(regularPrice1);
		
		
		Value prodAutId2 = Mockito.mock(Value.class);
		Mockito.when(prodAutId2.asString()).thenReturn(PROD_AUTID_ID_2);
		Mockito.when(value2.get(MatchPriceListPolicyQueryCypherStatement.PROD_AUTID_ID)).thenReturn(prodAutId2);
		
		Value doubleShotPrice2 = Mockito.mock(Value.class);
		Mockito.when(doubleShotPrice2.asString()).thenReturn(DOUBLE_SHOT_PRICE_2);
		Mockito.when(value2.get(MatchPriceListPolicyQueryCypherStatement.DOUBLE_SHOT_PRICE)).thenReturn(doubleShotPrice2);
		
		Value withFixedSyrupPrice2 = Mockito.mock(Value.class);
		Mockito.when(withFixedSyrupPrice2.asString()).thenReturn(WITH_FIXED_SYRUP_PRICE_2);
		Mockito.when(value2.get(MatchPriceListPolicyQueryCypherStatement.WITH_FIXED_SYRUP_PRICE)).thenReturn(withFixedSyrupPrice2);
		
		Value syrupPrice2 = Mockito.mock(Value.class);
		Mockito.when(syrupPrice2.asString()).thenReturn(SYRUP_PRICE_2);
		Mockito.when(value2.get(MatchPriceListPolicyQueryCypherStatement.SYRUP_PRICE)).thenReturn(syrupPrice2);
		
		Value largePrice2 = Mockito.mock(Value.class);
		Mockito.when(largePrice2.asString()).thenReturn(LARGE_PRICE_2);
		Mockito.when(value2.get(MatchPriceListPolicyQueryCypherStatement.LARGE_PRICE)).thenReturn(largePrice2);
		
		Value regularPrice2 = Mockito.mock(Value.class);
		Mockito.when(regularPrice2.asString()).thenReturn(REGULAR_PRICE_2);
		Mockito.when(value2.get(MatchPriceListPolicyQueryCypherStatement.REGULAR_PRICE)).thenReturn(regularPrice2);
		
		Value prodAutId3 = Mockito.mock(Value.class);
		Mockito.when(prodAutId3.asString()).thenReturn(PROD_AUTID_ID_3);
		Mockito.when(value3.get(MatchPriceListPolicyQueryCypherStatement.PROD_AUTID_ID)).thenReturn(prodAutId3);
		
		Value doubleShotPrice3 = Mockito.mock(Value.class);
		Mockito.when(doubleShotPrice3.asString()).thenReturn(DOUBLE_SHOT_PRICE_3);
		Mockito.when(value3.get(MatchPriceListPolicyQueryCypherStatement.DOUBLE_SHOT_PRICE)).thenReturn(doubleShotPrice3);
		
		Value syrupPrice3 = Mockito.mock(Value.class);
		Mockito.when(syrupPrice3.asString()).thenReturn(SYRUP_PRICE_3);
		Mockito.when(value3.get(MatchPriceListPolicyQueryCypherStatement.SYRUP_PRICE)).thenReturn(syrupPrice3);
		
		Value withFixedSyrupPrice3 = Mockito.mock(Value.class);
		Mockito.when(withFixedSyrupPrice3.asString()).thenReturn(WITH_FIXED_SYRUP_PRICE_3);
		Mockito.when(value3.get(MatchPriceListPolicyQueryCypherStatement.WITH_FIXED_SYRUP_PRICE)).thenReturn(withFixedSyrupPrice3);
		
		Value largePrice3 = Mockito.mock(Value.class);
		Mockito.when(largePrice3.asString()).thenReturn(LARGE_PRICE_3);
		Mockito.when(value3.get(MatchPriceListPolicyQueryCypherStatement.LARGE_PRICE)).thenReturn(largePrice3);
		
		Value regularPrice3 = Mockito.mock(Value.class);
		Mockito.when(regularPrice3.asString()).thenReturn(REGULAR_PRICE_3);
		Mockito.when(value3.get(MatchPriceListPolicyQueryCypherStatement.REGULAR_PRICE)).thenReturn(regularPrice3);
		
		PriceTable item = instance.toPolicyItem(list);
		
		assertTrue(item.isIsOneShot());
		assertEquals(3, item.getPrice().size());
		
		assertEquals(PROD_AUTID_ID_1, item.getPrice().get(0).getProdAuditId());
		assertEquals(new BigDecimal(DOUBLE_SHOT_PRICE_1), item.getPrice().get(0).getDoubleShotPrice());
		assertEquals(new BigDecimal(LARGE_PRICE_1), item.getPrice().get(0).getLargePrice());
		assertEquals(new BigDecimal(SYRUP_PRICE_1), item.getPrice().get(0).getSyrupPrice());
		assertEquals(new BigDecimal(WITH_FIXED_SYRUP_PRICE_1), item.getPrice().get(0).getWithFixedSyrupPrice());
		assertEquals(new BigDecimal(REGULAR_PRICE_1), item.getPrice().get(0).getRegularPrice());
		
		assertEquals(PROD_AUTID_ID_2, item.getPrice().get(1).getProdAuditId());
		assertEquals(new BigDecimal(DOUBLE_SHOT_PRICE_2), item.getPrice().get(1).getDoubleShotPrice());
		assertEquals(new BigDecimal(LARGE_PRICE_2), item.getPrice().get(1).getLargePrice());
		assertEquals(new BigDecimal(SYRUP_PRICE_2), item.getPrice().get(1).getSyrupPrice());
		assertEquals(new BigDecimal(WITH_FIXED_SYRUP_PRICE_2), item.getPrice().get(1).getWithFixedSyrupPrice());
		assertEquals(new BigDecimal(REGULAR_PRICE_2), item.getPrice().get(1).getRegularPrice());
		
		assertEquals(PROD_AUTID_ID_3, item.getPrice().get(2).getProdAuditId());
		assertEquals(new BigDecimal(DOUBLE_SHOT_PRICE_3), item.getPrice().get(2).getDoubleShotPrice());
		assertEquals(new BigDecimal(LARGE_PRICE_3), item.getPrice().get(2).getLargePrice());
		assertEquals(new BigDecimal(SYRUP_PRICE_3), item.getPrice().get(2).getSyrupPrice());
		assertEquals(new BigDecimal(WITH_FIXED_SYRUP_PRICE_3), item.getPrice().get(2).getWithFixedSyrupPrice());
		assertEquals(new BigDecimal(REGULAR_PRICE_3), item.getPrice().get(2).getRegularPrice());



	}
	
	
	@SuppressWarnings("unchecked")
	@Test(expected=PolicyNotCreatedException.class)
	public void toPolicyItemTestFailed1() throws PolicyNotCreatedException, ParseException, DatatypeConfigurationException {
		
		MatchPriceListPolicyQueryCypherStatement mockInstance = Mockito.mock(MatchPriceListPolicyQueryCypherStatement.class);
		
		List<Record> list = new ArrayList<Record>();
		
		Record record1 = Mockito.mock(Record.class);
		Record record2 = Mockito.mock(Record.class);
		Record record3 = Mockito.mock(Record.class);
		
		Value value1 = Mockito.mock(Value.class);
		Value value2 = Mockito.mock(Value.class);
		Value value3 = Mockito.mock(Value.class);
		
		list.add(record1);
		list.add(record2);
		list.add(record3);
		
		Value tableValue = Mockito.mock(Value.class);	
		
		Mockito.when(mockInstance.toXmlCalendar(Mockito.anyString())).thenThrow(new ParseException("forced", 2));
		
		Mockito.when(record1.get(MatchPriceListPolicyQueryCypherStatement.TABLE_INDEX)).thenReturn(tableValue);
		
		
		Value isOneShotValue = Mockito.mock(Value.class);		
		Mockito.when(isOneShotValue.asString()).thenReturn(IS_ONE_SHOT);
		
		Value effectiveDateValue = Mockito.mock(Value.class);
		Mockito.when(effectiveDateValue.asString()).thenReturn(CREATED_DATE);
		
		Mockito.when(tableValue.get(MatchPriceListPolicyQueryCypherStatement.PRICE_TABLE_FIELD_IS_ONE_SHOT)).thenReturn(isOneShotValue);		
		Mockito.when(tableValue.get(MatchPriceListPolicyQueryCypherStatement.PRICE_TABLE_EFFECTIVE_DATE)).thenReturn(effectiveDateValue);
		
		Mockito.when(record1.get(MatchPriceListPolicyQueryCypherStatement.ITEM_INDEX)).thenReturn(value1);
		Mockito.when(record2.get(MatchPriceListPolicyQueryCypherStatement.ITEM_INDEX)).thenReturn(value2);
		Mockito.when(record3.get(MatchPriceListPolicyQueryCypherStatement.ITEM_INDEX)).thenReturn(value3);
		
		Mockito.when(mockInstance.toPolicyItem(Mockito.anyList())).thenCallRealMethod();
		
		mockInstance.toPolicyItem(list);
	}
	
	@SuppressWarnings("unchecked")
	@Test(expected=PolicyNotCreatedException.class)
	public void toPolicyItemTestFailed() throws PolicyNotCreatedException, ParseException, DatatypeConfigurationException {
		
		MatchPriceListPolicyQueryCypherStatement mockInstance = Mockito.mock(MatchPriceListPolicyQueryCypherStatement.class);
		
		List<Record> list = new ArrayList<Record>();
		
		Record record1 = Mockito.mock(Record.class);
		Record record2 = Mockito.mock(Record.class);
		Record record3 = Mockito.mock(Record.class);
		
		Value value1 = Mockito.mock(Value.class);
		Value value2 = Mockito.mock(Value.class);
		Value value3 = Mockito.mock(Value.class);
		
		list.add(record1);
		list.add(record2);
		list.add(record3);
		
		Value tableValue = Mockito.mock(Value.class);	
		
		Mockito.when(mockInstance.toXmlCalendar(Mockito.anyString())).thenThrow(new DatatypeConfigurationException("forced"));
		
		Mockito.when(record1.get(MatchPriceListPolicyQueryCypherStatement.TABLE_INDEX)).thenReturn(tableValue);
		
		
		Value isOneShotValue = Mockito.mock(Value.class);		
		Mockito.when(isOneShotValue.asString()).thenReturn(IS_ONE_SHOT);
		
		Value effectiveDateValue = Mockito.mock(Value.class);
		Mockito.when(effectiveDateValue.asString()).thenReturn(CREATED_DATE);
		
		Mockito.when(tableValue.get(MatchPriceListPolicyQueryCypherStatement.PRICE_TABLE_FIELD_IS_ONE_SHOT)).thenReturn(isOneShotValue);		
		Mockito.when(tableValue.get(MatchPriceListPolicyQueryCypherStatement.PRICE_TABLE_EFFECTIVE_DATE)).thenReturn(effectiveDateValue);
		
		Mockito.when(record1.get(MatchPriceListPolicyQueryCypherStatement.ITEM_INDEX)).thenReturn(value1);
		Mockito.when(record2.get(MatchPriceListPolicyQueryCypherStatement.ITEM_INDEX)).thenReturn(value2);
		Mockito.when(record3.get(MatchPriceListPolicyQueryCypherStatement.ITEM_INDEX)).thenReturn(value3);
		
		Mockito.when(mockInstance.toPolicyItem(Mockito.anyList())).thenCallRealMethod();
		
		mockInstance.toPolicyItem(list);
	}

}
