package com.estafet.costaexpress.datasource.neo4j;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.doCallRealMethod;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.io.IOException;
import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Optional;

import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;

import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import com.estafet.costaexpress.datasource.entity.EntityAlreadyExistsException;
import com.estafet.costaexpress.datasource.entity.EntityNotCreatedException;
import com.estafet.costaexpress.datasource.entity.EntityNotFoundException;
import com.estafet.costaexpress.datasource.policy.PolicyAlreadyExistsException;
import com.estafet.costaexpress.datasource.policy.PolicyNotFoundException;
import com.estafet.costaexpress.datasource.statement.neo4j.CreateConnectionEntityCypherStatement;
import com.estafet.costaexpress.datasource.statement.neo4j.CreateSiblingEntityCypherStatement;
import com.estafet.costaexpress.datasource.statement.neo4j.DeleteEntityCypherStatement;
import com.estafet.costaexpress.datasource.statement.neo4j.MatchEntityQueryCypherStatement;
import com.estafet.costaexpress.datasource.statement.neo4j.MatchPriceListToEntityCypherStatement;
import com.estafet.costaexpress.datasource.statement.neo4j.MatchSiblingEntityQueryCypherStatement;
import com.estafet.costaexpress.model.entity.IPolicyAwareEntity;
import com.estafet.costaexpress.model.entity.impl.GroupEntity;
import com.estafet.costaexpress.model.entity.types.EntityTypesEnum;
import com.estafet.costaexpress.model.generated.pricelist.PriceTable;
import com.estafet.costaexpress.model.generated.pricelist.PriceTable.Price;
import com.estafet.costaexpress.model.policy.impl.PriceListPolicy;
import com.estafet.costaexpress.model.relation.OneToManyRelation;
import com.estafet.costaexpress.test.tools.EmbeddedNeo4JDataSource;
import com.estafet.datasource.DataSourceFailureException;
import com.estafet.datasource.initializer.DataSourceNotInitializedException;
import com.estafet.datasource.neo4j.Neo4JDataSourceInitializer;
import com.estafet.datasource.query.QueryCriteria;
import com.estafet.datasource.statement.StatementFailedException;


public class Neo4JEntityDataSourceNegativeTest {
	
	private static Neo4JEntityDataSource mockInstance;	
	private static EmbeddedNeo4JDataSource<Neo4JEntityDataSource, Neo4JPolicyDataSource> dataSource = new EmbeddedNeo4JDataSource<>();
	
	@BeforeClass
	public static void startDatabase() throws IOException {
		dataSource.startDatabase();
	}
	
	@Before
	public void init(){ 
		mockInstance = mock(Neo4JEntityDataSource.class);
		Neo4JDataSourceInitializer initializer = new Neo4JDataSourceInitializer(EmbeddedNeo4JDataSource.DB_PORT, "localhost", "admin", "neo4j");
		dataSource.setEntityInstance(new Neo4JEntityDataSource(initializer));
		dataSource.setPolicyInstance(new Neo4JPolicyDataSource(initializer));
	}
	
	@Test(expected=DataSourceFailureException.class)
	public void findEntityNotInitializedTest() throws DataSourceFailureException, DataSourceNotInitializedException, EntityAlreadyExistsException {
		assertFalse(dataSource.getEntityInstance().ready());
		GroupEntity test = new GroupEntity("test", "test");
		dataSource.getEntityInstance().find(Neo4JEntityDataSourceTest.newQueryCriteriaFromNode(test));
	}
	
	@Test(expected=EntityNotFoundException.class)
	public void deleteNonExistingGroupTest() throws DataSourceFailureException, DataSourceNotInitializedException, EntityNotFoundException {
		dataSource.getEntityInstance().init();
		assertTrue(dataSource.getEntityInstance().ready());
		dataSource.getEntityInstance().delete("UnknownId", EntityTypesEnum.GROUP.value(), false);
	}
	
	@Test(expected=DataSourceFailureException.class)
	public void deleteUnableToDeleteEntityTest() throws EntityAlreadyExistsException, DataSourceFailureException, PolicyNotFoundException, StatementFailedException, EntityNotFoundException{
		when(mockInstance.ready()).thenReturn(true);
		doThrow(new StatementFailedException("forced")).when(mockInstance).execute(any(DeleteEntityCypherStatement.class), any(String.class));
		doCallRealMethod().when(mockInstance).delete(any(String.class), any(String.class), any(Boolean.class));
		mockInstance.delete("test", "test", true);
	}
	
	@Test(expected=DataSourceFailureException.class)
	public void findSiblingEntitiesNotInitializedTest() throws DataSourceFailureException, DataSourceNotInitializedException, EntityAlreadyExistsException {
		assertFalse(dataSource.getEntityInstance().ready());
		QueryCriteria criteria = Neo4JEntityDataSourceTest.newQueryCriteriaFromNode(new GroupEntity("", ""));
		assertEquals(Neo4JEntityDataSourceTest.newQueryCriteriaFromNode(new GroupEntity(null, null)).getCriteria().size(), 1);
		assertEquals(Neo4JEntityDataSourceTest.newQueryCriteriaFromNode(null).getCriteria().size(), 0);
		dataSource.getEntityInstance().findSiblingEntities(criteria);
	}
	
	
	@Test(expected=DataSourceFailureException.class)
	public void createNotInitializedTest() throws EntityAlreadyExistsException, DataSourceFailureException {
		assertFalse(dataSource.getEntityInstance().ready());

		GroupEntity test = new GroupEntity("test", "test");
		dataSource.getEntityInstance().create(test);
	}
	
	@Test(expected=DataSourceFailureException.class)
	public void createSiblingNotInitializedTest() throws EntityAlreadyExistsException, DataSourceFailureException {
		assertFalse(dataSource.getEntityInstance().ready());

		GroupEntity test = new GroupEntity("test", "test");
		dataSource.getEntityInstance().createSibling(test, "id", "type");
	}
	
	@Test(expected=DataSourceFailureException.class)
	public void deleteNotInitializedTest() throws EntityAlreadyExistsException, DataSourceFailureException, EntityNotFoundException {
		assertFalse(dataSource.getEntityInstance().ready());
		dataSource.getEntityInstance().delete("id", "type", false);
	}
	
	@Test(expected=DataSourceFailureException.class)
	public void createConnectionNotInitializedTest() throws DataSourceFailureException {
		assertFalse(dataSource.getEntityInstance().ready());
		dataSource.getEntityInstance().createConnection("test", "fake", "totallyFake", "fakeType");
	}
	
	@Test
	public void findEntityEntityNotCreatedTest() throws EntityNotCreatedException, DataSourceFailureException{
		when(mockInstance.ready()).thenReturn(true);
		when(mockInstance.query(any(MatchEntityQueryCypherStatement.class), any(QueryCriteria.class))).thenThrow(new EntityNotCreatedException("forced"));
		when(mockInstance.find(any(QueryCriteria.class))).thenCallRealMethod();
		Optional<IPolicyAwareEntity> result = mockInstance.find(new QueryCriteria());
		assertFalse(result.isPresent());
	}
	
	@Test(expected=EntityAlreadyExistsException.class)
	public void createSiblingEntityExceptionsTest() throws EntityAlreadyExistsException, DataSourceFailureException, StatementFailedException{
		when(mockInstance.ready()).thenReturn(true);
		when(mockInstance.isConflict(any(StatementFailedException.class))).thenReturn(true);
		doThrow(new StatementFailedException("forced")).when(mockInstance).execute(any(CreateSiblingEntityCypherStatement.class), any(IPolicyAwareEntity.class));
		doCallRealMethod().when(mockInstance).createSibling(any(IPolicyAwareEntity.class), any(String.class), any(String.class));
		GroupEntity test = new GroupEntity("test", "test");
		mockInstance.createSibling(test, "id", "type");
	}
	
	@Test
	public void findSiblingEntityExceptionsTest() throws DataSourceFailureException, EntityNotFoundException, EntityNotCreatedException{
		when(mockInstance.ready()).thenReturn(true);
		doThrow(new EntityNotFoundException("forced")).when(mockInstance).queryItems(any(MatchSiblingEntityQueryCypherStatement.class), any(QueryCriteria.class));
		doCallRealMethod().when(mockInstance).findSiblingEntities(any(QueryCriteria.class));
		GroupEntity test = new GroupEntity("test", "test");
		QueryCriteria criteria = Neo4JEntityDataSourceTest.newQueryCriteriaFromNode(test);
		List<IPolicyAwareEntity> result = mockInstance.findSiblingEntities(criteria);
		assertTrue(result.isEmpty());
		
		doThrow(new EntityNotCreatedException("forced")).when(mockInstance).queryItems(any(MatchSiblingEntityQueryCypherStatement.class), any(QueryCriteria.class));
		result = mockInstance.findSiblingEntities(criteria);
		assertTrue(result.isEmpty());
	}
	
	@Test(expected=DataSourceFailureException.class)
	public void createSiblingUnableToCreateEntityTest() throws EntityAlreadyExistsException, DataSourceFailureException, StatementFailedException{
		when(mockInstance.ready()).thenReturn(true);
		doThrow(new StatementFailedException("forced")).when(mockInstance).execute(any(CreateSiblingEntityCypherStatement.class), any(IPolicyAwareEntity.class));
		doCallRealMethod().when(mockInstance).createSibling(any(IPolicyAwareEntity.class), any(String.class), any(String.class));
		GroupEntity test = new GroupEntity("test", "test");
		mockInstance.createSibling(test, "id", "type");
	}
	
	@Test(expected=DataSourceFailureException.class)
	public void createConnectionDataSourceFailureTest() throws StatementFailedException, DataSourceFailureException{
		when(mockInstance.ready()).thenReturn(true);
		doThrow(new StatementFailedException("forced")).when(mockInstance).execute(any(CreateConnectionEntityCypherStatement.class), any(String.class));
		doCallRealMethod().when(mockInstance).createConnection(any(String.class), any(String.class), any(String.class), any(String.class));
		mockInstance.createConnection("test", "fake", "totallyFake", "fakeType");
	}
	
	@SuppressWarnings("unchecked")
	@Test
	public void findEntitiesUnderPolicyTest() throws DataSourceNotInitializedException, EntityAlreadyExistsException, DataSourceFailureException, ParseException, DatatypeConfigurationException, PolicyAlreadyExistsException, EntityNotFoundException, EntityNotCreatedException {
		dataSource.getPolicyInstance().init();
		dataSource.getEntityInstance().init();
		assertTrue(dataSource.getPolicyInstance().ready());
		assertTrue(dataSource.getEntityInstance().ready());
		PriceListPolicy policy = newPriceList("findEntitiesUnderPolicyTest", "findEntitiesUnderPolicyTestId");
		QueryCriteria criteria = Neo4JEntityDataSourceTest.newQueryCriteriaFromNode(policy);
		
		Optional<OneToManyRelation> result = dataSource.getEntityInstance().findEntitiesUnderPolicy(criteria, Optional.empty());
		assertFalse(result.isPresent());
		
		result = dataSource.getEntityInstance().findEntitiesUnderPolicy(criteria, Optional.of(dataSource.getPolicyInstance()));
		assertFalse(result.isPresent());
		
		dataSource.getPolicyInstance().create(policy);
		result = dataSource.getEntityInstance().findEntitiesUnderPolicy(criteria, Optional.of(dataSource.getPolicyInstance()));
		assertFalse(result.isPresent());
		
		doThrow(new EntityNotFoundException("forced")).when(mockInstance).queryItems(any(MatchPriceListToEntityCypherStatement.class), any(QueryCriteria.class));
		doCallRealMethod().when(mockInstance).findEntitiesUnderPolicy(any(QueryCriteria.class), any(Optional.class));
		result = mockInstance.findEntitiesUnderPolicy(criteria, Optional.of(dataSource.getPolicyInstance()));
		assertFalse(result.isPresent());
		
		doThrow(new EntityNotCreatedException("forced")).when(mockInstance).queryItems(any(MatchPriceListToEntityCypherStatement.class), any(QueryCriteria.class));
		result = mockInstance.findEntitiesUnderPolicy(criteria, Optional.of(dataSource.getPolicyInstance()));
		assertFalse(result.isPresent());
	}
	
	private PriceListPolicy newPriceList(String name, String id) throws ParseException, DatatypeConfigurationException {
		PriceTable priceTable = new PriceTable();
		priceTable.setIsOneShot(true);
		GregorianCalendar effectiveDateCalendar = new GregorianCalendar();	
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSXXX");
		Date date = dateFormat.parse("2017-02-17T14:04:40.772+02:00");
		effectiveDateCalendar.setTime(date);
		XMLGregorianCalendar calendar =  DatatypeFactory.newInstance().newXMLGregorianCalendar(effectiveDateCalendar);		
		priceTable.setEffectiveDate(calendar);
		Price price = new PriceTable.Price();
		price.setDoubleShotPrice(new BigDecimal("1.80"));
		price.setLargePrice(new BigDecimal("1.10"));
		price.setProdAuditId("Drink1");
		price.setRegularPrice(new BigDecimal("1.00"));
		price.setSyrupPrice(new BigDecimal("0.90"));
		price.setWithFixedSyrupPrice(new BigDecimal("0.99"));
		priceTable.getPrice().add(price);
		
		return new PriceListPolicy(name, id, priceTable);
		
	}
	
	@Test(expected=DataSourceFailureException.class)
	public void createUnableToCreateEntityTest() throws EntityAlreadyExistsException, DataSourceFailureException, StatementFailedException{
		when(mockInstance.ready()).thenReturn(true);
		doThrow(new StatementFailedException("forced")).when(mockInstance).execute(any(CreateSiblingEntityCypherStatement.class), any(IPolicyAwareEntity.class));
		doCallRealMethod().when(mockInstance).create(any(IPolicyAwareEntity.class));
		GroupEntity test = new GroupEntity("test", "test");
		mockInstance.create(test);
	}
	
	@Test(expected=EntityAlreadyExistsException.class)
	public void createEntityAlreadyExistsTest() throws EntityAlreadyExistsException, DataSourceFailureException, StatementFailedException{
		when(mockInstance.ready()).thenReturn(true);
		when(mockInstance.isConflict(any(StatementFailedException.class))).thenReturn(true);
		doThrow(new StatementFailedException("forced")).when(mockInstance).execute(any(CreateSiblingEntityCypherStatement.class), any(IPolicyAwareEntity.class));
		doCallRealMethod().when(mockInstance).create(any(IPolicyAwareEntity.class));
		GroupEntity test = new GroupEntity("test", "test");
		mockInstance.create(test);
	}
	
	@AfterClass
	public static void afterTestMethod() throws IOException{
		dataSource.shutdownDatabase();
	}
}
