package com.estafet.costaexpress.datasource.neo4j;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.doCallRealMethod;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.io.IOException;
import java.util.Optional;

import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.mockito.Mockito;

import com.estafet.costaexpress.datasource.entity.EntityAlreadyExistsException;
import com.estafet.costaexpress.datasource.entity.EntityNotCreatedException;
import com.estafet.costaexpress.datasource.entity.EntityNotFoundException;
import com.estafet.costaexpress.datasource.policy.PolicyAlreadyExistsException;
import com.estafet.costaexpress.datasource.policy.PolicyNotCreatedException;
import com.estafet.costaexpress.datasource.policy.PolicyNotFoundException;
import com.estafet.costaexpress.datasource.statement.neo4j.AbstractPolicyQueryCypherStatement;
import com.estafet.costaexpress.datasource.statement.neo4j.CreateConnectionEntityCypherStatement;
import com.estafet.costaexpress.datasource.statement.neo4j.DeletePriceListPolicyCypherStatement;
import com.estafet.costaexpress.datasource.statement.neo4j.MatchEntityToPriceListCypherStatement;
import com.estafet.costaexpress.model.entity.impl.GroupEntity;
import com.estafet.costaexpress.model.generated.pricelist.PriceTable;
import com.estafet.costaexpress.model.policy.IPolicy;
import com.estafet.costaexpress.model.policy.impl.PriceListPolicy;
import com.estafet.costaexpress.model.relation.OneToOneRelation;
import com.estafet.costaexpress.test.tools.EmbeddedNeo4JDataSource;
import com.estafet.datasource.DataSourceFailureException;
import com.estafet.datasource.NodeNotCreatedException;
import com.estafet.datasource.NodeNotFoundException;
import com.estafet.datasource.initializer.DataSourceNotInitializedException;
import com.estafet.datasource.neo4j.Neo4JDataSourceInitializer;
import com.estafet.datasource.query.QueryCriteria;
import com.estafet.datasource.statement.StatementFailedException;


public class Neo4JPolicyDataSourceNegativeTest {
	
	private static Neo4JPolicyDataSource mockInstance;
	private static EmbeddedNeo4JDataSource<Neo4JEntityDataSource, Neo4JPolicyDataSource> dataSource = new EmbeddedNeo4JDataSource<>();
	
	@BeforeClass
	public static void startDatabase() throws IOException {
		dataSource.startDatabase();
	}
	
	@Before
	public void init(){ 
		mockInstance = mock(Neo4JPolicyDataSource.class);
		Neo4JDataSourceInitializer initializer = new Neo4JDataSourceInitializer(EmbeddedNeo4JDataSource.DB_PORT, "localhost", "admin", "neo4j");
		dataSource.setEntityInstance(new Neo4JEntityDataSource(initializer));
		dataSource.setPolicyInstance(new Neo4JPolicyDataSource(initializer));
	}
	
	
	@Test(expected=DataSourceFailureException.class)
	public void createPolicyNotInitializedTest() throws DataSourceFailureException, PolicyNotCreatedException, PolicyAlreadyExistsException {
		assertFalse(dataSource.getPolicyInstance().ready());
		dataSource.getPolicyInstance().create(new PriceListPolicy("fake", "id", null));
	}
	
	@Test(expected=DataSourceFailureException.class)
	public void findPolicyNotInitializedTest() throws DataSourceFailureException, PolicyNotCreatedException {
		assertFalse(dataSource.getPolicyInstance().ready());
		dataSource.getPolicyInstance().find(null);
	}	

	@Test(expected=DataSourceFailureException.class)
	public void deletePolicyNotInitializedTest() throws DataSourceFailureException, PolicyNotCreatedException, PolicyNotFoundException {
		assertFalse(dataSource.getPolicyInstance().ready());
		dataSource.getPolicyInstance().delete("fake", "fakeType");
	}	
	@Test(expected=DataSourceFailureException.class)
	public void applyPolicyNotInitializedTest() throws PolicyNotFoundException, DataSourceFailureException {
		assertFalse(dataSource.getPolicyInstance().ready());
		dataSource.getPolicyInstance().applyPolicy("fake", "fakeType", "fake");
	}	
	
	@SuppressWarnings("unchecked")
	@Test
	public void findPolicyEntityNotCreatedTest() throws EntityNotCreatedException, DataSourceFailureException, PolicyNotCreatedException{
		when(mockInstance.ready()).thenReturn(true);
		when(mockInstance.query(any(AbstractPolicyQueryCypherStatement.class), any(QueryCriteria.class))).thenThrow(new PolicyNotCreatedException("forced"));
		when(mockInstance.find(any(QueryCriteria.class))).thenCallRealMethod();
		Optional<IPolicy> result = mockInstance.find(new QueryCriteria());
		assertFalse(result.isPresent());
	}
	
	@SuppressWarnings("unchecked")
	@Test
	public void findPolicyPolicyNotCreatedExceptionTest() throws EntityNotCreatedException, DataSourceFailureException, PolicyNotCreatedException{
		when(mockInstance.ready()).thenReturn(true);
		when(mockInstance.query(any(AbstractPolicyQueryCypherStatement.class), any(QueryCriteria.class))).thenThrow(new PolicyNotCreatedException("forced"));
		when(mockInstance.find(any(QueryCriteria.class))).thenCallRealMethod();
		Optional<IPolicy> result = mockInstance.find(new QueryCriteria());
		assertFalse(result.isPresent());
	}
	
	@SuppressWarnings("unchecked")
	@Test
	public void findPolicyReturnEmptyTest() throws EntityNotCreatedException, PolicyNotCreatedException, DataSourceFailureException{
		when(mockInstance.ready()).thenReturn(true);
		when(mockInstance.query(any(AbstractPolicyQueryCypherStatement.class), any(QueryCriteria.class))).thenReturn(null);
		when(mockInstance.find(any(QueryCriteria.class))).thenCallRealMethod();
		Optional<IPolicy> result = mockInstance.find(new QueryCriteria());
		assertFalse(result.isPresent());
	}
	
	@Test(expected=PolicyAlreadyExistsException.class)
	public void createPolicyAlreadyExistsTest() throws EntityAlreadyExistsException, DataSourceFailureException, StatementFailedException, PolicyAlreadyExistsException{
		when(mockInstance.ready()).thenReturn(true);
		when(mockInstance.isConflict(any(StatementFailedException.class))).thenReturn(true);
		doThrow(new StatementFailedException("forced")).when(mockInstance).createPolicyEntity(Mockito.any(IPolicy.class));
		doCallRealMethod().when(mockInstance).create(any(IPolicy.class));
		PriceListPolicy test = new PriceListPolicy("test", "test", new PriceTable());
		mockInstance.create(test);
	}

	@Test(expected=DataSourceFailureException.class)
	public void deletePolicyUnableToDeleteEntityTest() throws EntityAlreadyExistsException, DataSourceFailureException, StatementFailedException, PolicyNotFoundException{
		when(mockInstance.ready()).thenReturn(true);
		doThrow(new StatementFailedException("forced")).when(mockInstance).execute(any(DeletePriceListPolicyCypherStatement.class), any(String.class));
		doCallRealMethod().when(mockInstance).delete(any(String.class), any(String.class));
		mockInstance.delete("test", "test");
	}
	
	@Test(expected=PolicyNotFoundException.class)
	public void deletePolicyPolicyNotFoundTest() throws EntityAlreadyExistsException, DataSourceFailureException, StatementFailedException, PolicyNotFoundException{
		when(mockInstance.ready()).thenReturn(true);
		when(mockInstance.isEntityNotFound(any(StatementFailedException.class))).thenReturn(true);
		doThrow(new StatementFailedException("forced")).when(mockInstance).execute(any(DeletePriceListPolicyCypherStatement.class), any(String.class));
		doCallRealMethod().when(mockInstance).delete(any(String.class), any(String.class));
		mockInstance.delete("test", "test");
	}
	
	@Test(expected=DataSourceFailureException.class)
	public void applyPolicyUnabletoCreateConnectionTest() throws EntityAlreadyExistsException, DataSourceFailureException, StatementFailedException, PolicyNotFoundException{
		when(mockInstance.ready()).thenReturn(true);
		doThrow(new StatementFailedException("forced")).when(mockInstance).execute(any(CreateConnectionEntityCypherStatement.class), any(String.class));
		doCallRealMethod().when(mockInstance).applyPolicy(any(String.class), any(String.class), any(String.class));
		mockInstance.applyPolicy("test", "test", "test");
	}
	
	@Test(expected=PolicyNotFoundException.class)
	public void applyPolicyPolicyNotFoundTest() throws EntityAlreadyExistsException, DataSourceFailureException, StatementFailedException, PolicyNotFoundException{
		when(mockInstance.ready()).thenReturn(true);
		when(mockInstance.isEntityNotFound(any(StatementFailedException.class))).thenReturn(true);
		doThrow(new StatementFailedException("forced")).when(mockInstance).execute(any(CreateConnectionEntityCypherStatement.class), any(String.class));
		doCallRealMethod().when(mockInstance).applyPolicy(any(String.class), any(String.class), any(String.class));
		mockInstance.applyPolicy("test", "test", "test");
	}
	
	@Test
	public void findPolicyForEntityTest() throws DataSourceNotInitializedException, DataSourceFailureException, EntityAlreadyExistsException, NodeNotFoundException, NodeNotCreatedException{
		dataSource.getPolicyInstance().init();
		dataSource.getEntityInstance().init();
		assertTrue(dataSource.getPolicyInstance().ready());
		assertTrue(dataSource.getEntityInstance().ready());
		GroupEntity group = new GroupEntity("findPolicyForEntityTest", "findPolicyForEntityTestId");
		QueryCriteria criteria = Neo4JEntityDataSourceTest.newQueryCriteriaFromNode(group);
		
		Optional<OneToOneRelation> result = dataSource.getPolicyInstance().findPolicyForEntity(criteria, dataSource.getEntityInstance());
		assertFalse(result.isPresent());
		
		dataSource.getEntityInstance().create(group);
		result = dataSource.getPolicyInstance().findPolicyForEntity(criteria, dataSource.getEntityInstance());
		assertFalse(result.isPresent());
		
		doThrow(new EntityNotFoundException("forced")).when(mockInstance).query(any(MatchEntityToPriceListCypherStatement.class), any(QueryCriteria.class));
		doCallRealMethod().when(mockInstance).findPolicyForEntity(any(QueryCriteria.class), any(Neo4JEntityDataSource.class));
		result = mockInstance.findPolicyForEntity(criteria, dataSource.getEntityInstance());
		assertFalse(result.isPresent());
		
		doThrow(new EntityNotCreatedException("forced")).when(mockInstance).query(any(MatchEntityToPriceListCypherStatement.class), any(QueryCriteria.class));
		result = mockInstance.findPolicyForEntity(criteria, dataSource.getEntityInstance());
		assertFalse(result.isPresent());
	}
	
	@AfterClass
	public static void afterTestMethod() throws IOException{
		dataSource.shutdownDatabase();
	}
}
