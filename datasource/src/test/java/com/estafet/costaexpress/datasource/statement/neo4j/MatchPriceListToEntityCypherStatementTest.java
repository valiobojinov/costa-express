package com.estafet.costaexpress.datasource.statement.neo4j;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import com.estafet.costaexpress.datasource.neo4j.Neo4JEntityDataSource;
import com.estafet.costaexpress.datasource.policy.IPolicyDataSource.PolicySearchAttributesEnum;
import com.estafet.costaexpress.model.policy.types.PolicyTypesEnum;
import com.estafet.datasource.query.QueryCriteria;


public class MatchPriceListToEntityCypherStatementTest {
	private MatchPriceListToEntityCypherStatement instance;
	private static final String MATCH_PRICECARD_TO_ENTITY_STATEMENT = "MATCH (price:pricelist{code:'shell-xyz'})-[:appliesTo]->(group)-[:manages*]->(excluded)<-[:appliesTo]-(p:pricelist) WHERE p.code <> price.code WITH collect(excluded) as already_assigned MATCH path = (price:pricelist{code:'shell-xyz'})-[:appliesTo|manages*]->(entity) WHERE NOT any(node in already_assigned WHERE node in NODES(path)) RETURN price, entity";
															

	private QueryCriteria criteria;
	
	@Before
	public void setup() {		
		criteria = new QueryCriteria();
		instance = new MatchPriceListToEntityCypherStatement(Mockito.mock(Neo4JEntityDataSource.class));
	}
	
	
	@Test 
	public void prepareStatementTest() {
		criteria.addCriteria(PolicySearchAttributesEnum.TYPE.value(), PolicyTypesEnum.PRICELIST.value());
		criteria.addCriteria(PolicySearchAttributesEnum.CODE.value(), "shell-xyz");
		String statement = instance.prepareStatement(criteria);		
		System.out.println(statement);
		assertEquals(MATCH_PRICECARD_TO_ENTITY_STATEMENT, statement);
	
	}

}
