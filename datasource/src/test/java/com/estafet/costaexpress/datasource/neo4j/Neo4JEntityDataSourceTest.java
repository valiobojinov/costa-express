package com.estafet.costaexpress.datasource.neo4j;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Optional;

import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;

import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;

import com.estafet.costaexpress.datasource.entity.EntityAlreadyExistsException;
import com.estafet.costaexpress.datasource.entity.EntityNotFoundException;
import com.estafet.costaexpress.datasource.entity.IEntityDataSource.EntitySearchAttributesEnum;
import com.estafet.costaexpress.datasource.policy.PolicyAlreadyExistsException;
import com.estafet.costaexpress.datasource.policy.PolicyNotFoundException;
import com.estafet.costaexpress.model.entity.IPolicyAwareEntity;
import com.estafet.costaexpress.model.entity.connections.ConnectionTypesEnum;
import com.estafet.costaexpress.model.entity.impl.GroupEntity;
import com.estafet.costaexpress.model.entity.types.EntityTypesEnum;
import com.estafet.costaexpress.model.generated.pricelist.PriceTable;
import com.estafet.costaexpress.model.generated.pricelist.PriceTable.Price;
import com.estafet.costaexpress.model.policy.IPolicy;
import com.estafet.costaexpress.model.policy.impl.PriceListPolicy;
import com.estafet.costaexpress.model.policy.types.PolicyTypesEnum;
import com.estafet.costaexpress.model.relation.OneToManyRelation;
import com.estafet.costaexpress.test.tools.EmbeddedNeo4JDataSource;
import com.estafet.datasource.DataSourceFailureException;
import com.estafet.datasource.initializer.DataSourceNotInitializedException;
import com.estafet.datasource.neo4j.Neo4JDataSourceInitializer;
import com.estafet.datasource.node.IDataSourceNode;
import com.estafet.datasource.query.QueryCriteria;



public class Neo4JEntityDataSourceTest {
	
	private static EmbeddedNeo4JDataSource<Neo4JEntityDataSource, Neo4JPolicyDataSource> dataSource = new EmbeddedNeo4JDataSource<>();
	
	@BeforeClass
	public static void startDatabase() throws IOException {		 
		dataSource.startDatabase();
	}

	@Before
	public void init(){
		Neo4JDataSourceInitializer initializer = new Neo4JDataSourceInitializer(EmbeddedNeo4JDataSource.DB_PORT, "localhost", "admin", "neo4j");
		dataSource.setEntityInstance(new Neo4JEntityDataSource(initializer));
		dataSource.setPolicyInstance(new Neo4JPolicyDataSource(initializer));		
	}
	
	@Test
	public void createFindDeleteGroupTest() throws DataSourceFailureException, DataSourceNotInitializedException, EntityAlreadyExistsException, EntityNotFoundException {
		dataSource.getEntityInstance().init();
		assertTrue(dataSource.getEntityInstance().ready());
		
		GroupEntity test = new GroupEntity("createFindGroupTest", "createFindGroupTestId");
		dataSource.getEntityInstance().create(test);
		Optional<IPolicyAwareEntity> entity = dataSource.getEntityInstance().find(newQueryCriteriaFromNode(test));
		assertTrue(entity.isPresent());
		assertTrue(entity.get() instanceof GroupEntity);
		GroupEntity group = (GroupEntity)entity.get();
		assertEquals("createFindGroupTest", group.getNodeName());
		assertEquals("createFindGroupTest", group.getName());
		assertEquals("createFindGroupTestId", group.getId());
		assertEquals(EntityTypesEnum.GROUP, group.getType());
		assertEquals(EntityTypesEnum.GROUP.value(), group.getNodeType());
		
		GroupEntity siblingTest = new GroupEntity("createFindSiblingGroupTest", "createFindSiblingGroupTestId");
		dataSource.getEntityInstance().create(siblingTest);
		Optional<IPolicyAwareEntity> siblingEntity = dataSource.getEntityInstance().find(newQueryCriteriaFromNode(siblingTest));
		assertTrue(siblingEntity.isPresent());
		assertTrue(siblingEntity.get() instanceof GroupEntity);
		dataSource.getEntityInstance().createConnection(group.getId(), group.getNodeType(), siblingEntity.get().getId(), ConnectionTypesEnum.fromType("manages").get().type());
		List<IPolicyAwareEntity> resultList = dataSource.getEntityInstance().findSiblingEntities(newQueryCriteriaFromNode(group));
		assertFalse(resultList.isEmpty());
		assertEquals(siblingEntity.get().getId(), resultList.get(0).getId());
		
		List<IPolicyAwareEntity> result = dataSource.getEntityInstance().findSiblingEntities(newQueryCriteriaFromNode(null));
		assertEquals(result.size(), 0);
		assertEquals(newQueryCriteriaFromNode(new GroupEntity("", "")).getCriteria().size(), 1);
		assertEquals(newQueryCriteriaFromNode(new GroupEntity(null, null)).getCriteria().size(), 1);
		
		dataSource.getEntityInstance().delete("createFindGroupTestId", EntityTypesEnum.GROUP.value(), false);
		assertFalse(dataSource.getEntityInstance().find(newQueryCriteriaFromNode(test)).isPresent());
	}
	
	@Test
	public void createSiblingFindGroupTest() throws DataSourceFailureException, DataSourceNotInitializedException, EntityAlreadyExistsException {
		dataSource.getEntityInstance().init();
		assertTrue(dataSource.getEntityInstance().ready());
		
		GroupEntity test = new GroupEntity("createSiblingFindGroupTest", "createSiblingFindGroupTestId");
		GroupEntity child = new GroupEntity("createSiblingFindGroupTest1", "createSiblingFindGroupTestId1");
		dataSource.getEntityInstance().create(test);
		Optional<IPolicyAwareEntity> entity = dataSource.getEntityInstance().find(newQueryCriteriaFromNode(test));
		assertTrue(entity.isPresent());
		assertTrue(entity.get() instanceof GroupEntity);
		GroupEntity group = (GroupEntity)entity.get();
		assertEquals("createSiblingFindGroupTest", group.getNodeName());
		assertEquals("createSiblingFindGroupTest", group.getName());
		assertEquals("createSiblingFindGroupTestId", group.getId());
		assertEquals(EntityTypesEnum.GROUP, group.getType());
		assertEquals(EntityTypesEnum.GROUP.value(), group.getNodeType());
		dataSource.getEntityInstance().createSibling(child, "createSiblingFindGroupTestId", EntityTypesEnum.GROUP.value());
		Optional<IPolicyAwareEntity> sibling = dataSource.getEntityInstance().find(newQueryCriteriaFromNode(child));
		GroupEntity siblingEntity = (GroupEntity)sibling.get();
		assertEquals("createSiblingFindGroupTest1", siblingEntity.getNodeName());
		assertEquals("createSiblingFindGroupTest1", siblingEntity.getName());
		assertEquals("createSiblingFindGroupTestId1", siblingEntity.getId());
	}
	
	@Test
	public void findEntitiesUnderPolicyTest() throws DataSourceFailureException, DataSourceNotInitializedException, EntityAlreadyExistsException, PolicyAlreadyExistsException, PolicyNotFoundException, ParseException, DatatypeConfigurationException {
		dataSource.getEntityInstance().init();
		dataSource.getPolicyInstance().init();
		assertTrue(dataSource.getEntityInstance().ready());
		assertTrue(dataSource.getPolicyInstance().ready());
		GroupEntity test = new GroupEntity("group", "groupId");
		dataSource.getEntityInstance().create(test);
		Optional<IPolicyAwareEntity> entity = dataSource.getEntityInstance().find(newQueryCriteriaFromNode(test));
		assertTrue(entity.isPresent());
		assertTrue(entity.get() instanceof GroupEntity);
		GroupEntity group = (GroupEntity)entity.get();
		assertEquals("group", group.getNodeName());
		assertEquals("group", group.getName());
		assertEquals("groupId", group.getId());		
		GroupEntity child = new GroupEntity("childGroup", "childGroupId");
		
		dataSource.getEntityInstance().createSibling(child, "groupId", EntityTypesEnum.GROUP.value());
		
		PriceTable priceTable = new PriceTable();
		priceTable.setIsOneShot(true);
		GregorianCalendar effectiveDateCalendar = new GregorianCalendar();	
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSXXX");
		Date date = dateFormat.parse("2017-02-17T14:04:40.772+02:00");
		effectiveDateCalendar.setTime(date);
		XMLGregorianCalendar calendar =  DatatypeFactory.newInstance().newXMLGregorianCalendar(effectiveDateCalendar);		
		priceTable.setEffectiveDate(calendar);
		Price price = new PriceTable.Price();
		price.setDoubleShotPrice(new BigDecimal("1.80"));
		price.setLargePrice(new BigDecimal("1.10"));
		price.setProdAuditId("Drink1");
		price.setRegularPrice(new BigDecimal("1.00"));
		price.setSyrupPrice(new BigDecimal("0.90"));
		price.setWithFixedSyrupPrice(new BigDecimal("0.99"));
		priceTable.getPrice().add(price);
		
		PriceListPolicy priceList = new PriceListPolicy("findEntitiesUnderPolicyTest", "findEntitiesUnderPolicyTest", priceTable);
		dataSource.getPolicyInstance().create(priceList);
		Optional<IPolicy> policy = dataSource.getPolicyInstance().find(newQueryCriteriaFromNode(priceList));
		assertTrue(policy.isPresent());
		dataSource.getPolicyInstance().applyPolicy(policy.get().getId(), policy.get().getPolicyType().value(), group.getId());
		
		QueryCriteria criteria = new QueryCriteria();
		criteria.addCriteria(EntitySearchAttributesEnum.TYPE.value(), PolicyTypesEnum.PRICELIST.value());
		criteria.addCriteria(EntitySearchAttributesEnum.CODE.value(), "findEntitiesUnderPolicyTest");
		Optional<OneToManyRelation> relation = dataSource.getEntityInstance().findEntitiesUnderPolicy(criteria, Optional.of(dataSource.getPolicyInstance()));
		assertTrue(relation.isPresent());
		assertNotNull(relation.get().getSource());
		assertEquals("findEntitiesUnderPolicyTest", relation.get().getSource().getId());
		assertEquals(2, relation.get().getTarget().size());
	}
	
	public static QueryCriteria newQueryCriteriaFromNode(IDataSourceNode node){
		QueryCriteria criteria = new QueryCriteria();
		if(node != null){
			if(node.getId() != null && !node.getId().isEmpty()){
				criteria.addCriteria(EntitySearchAttributesEnum.CODE.value(), node.getId());
			}
			if(node.getNodeType() != null && !node.getNodeType().isEmpty()){
				criteria.addCriteria(EntitySearchAttributesEnum.TYPE.value(), node.getNodeType());
			}
			if(node.getNodeName() != null && !node.getNodeName().isEmpty()){
				criteria.addCriteria(EntitySearchAttributesEnum.NAME.value(), node.getNodeName());
			}
		}
		return criteria;
	}
	
	@Ignore
	@Test 
	public void readinessTest() throws DataSourceNotInitializedException {
		assertFalse(dataSource.getEntityInstance().ready());
		dataSource.getEntityInstance().init();
		assertTrue(dataSource.getEntityInstance().ready());
		dataSource.getEntityInstance().close();
		assertFalse(dataSource.getEntityInstance().ready());
		dataSource.getEntityInstance().reset();
		assertTrue(dataSource.getEntityInstance().ready());
		
	}
	
	@AfterClass
	public static void afterTestMethod() throws IOException{
		dataSource.shutdownDatabase();
	}

}
