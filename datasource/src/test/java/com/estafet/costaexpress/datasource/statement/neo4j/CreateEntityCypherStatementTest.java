package com.estafet.costaexpress.datasource.statement.neo4j;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;
import org.neo4j.driver.v1.StatementResult;
import org.neo4j.driver.v1.exceptions.ClientException;

import com.estafet.costaexpress.model.entity.impl.GroupEntity;
import com.estafet.datasource.statement.StatementFailedException;

public class CreateEntityCypherStatementTest {

	private StatementResult mockResult;
	private CreateEntityCypherStatement instance;
	private GroupEntity entity;
	private static final String GROUP_CREATE_STATEMENT = "CREATE (n:group {code:\"N1234\", name:\"Group\"})";

	@Before
	public void setup() {
		instance = new CreateEntityCypherStatement(null);
		entity = new GroupEntity("Group", "N1234");
		mockResult = Mockito.mock(StatementResult.class);

	}

	@Test
	public void prepareStatementTest() {
		String statement = instance.prepareStatement(entity);
		System.out.println(statement);
		assertEquals(GROUP_CREATE_STATEMENT, statement);
	}

	@Test(expected = StatementFailedException.class)
	public void checkFailedStatementTest() throws StatementFailedException {
		Mockito.when(mockResult.list()).thenThrow(new ClientException("forced"));
		instance.checkStatementResult(mockResult);
	}
	
	@Test
	public void checkStatementTest() throws StatementFailedException {
		instance.checkStatementResult(mockResult);
	}
}
