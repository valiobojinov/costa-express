package com.estafet.costaexpress.datasource.statement.neo4j;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;
import org.neo4j.driver.v1.Record;
import org.neo4j.driver.v1.StatementResult;
import org.neo4j.driver.v1.Value;
import org.neo4j.driver.v1.types.Node;

import com.estafet.costaexpress.datasource.entity.EntityNotCreatedException;
import com.estafet.costaexpress.datasource.entity.EntityNotFoundException;
import com.estafet.costaexpress.datasource.entity.IEntityDataSource.EntitySearchAttributesEnum;
import com.estafet.costaexpress.datasource.neo4j.Neo4JEntityDataSource;
import com.estafet.costaexpress.model.entity.types.EntityTypesEnum;
import com.estafet.datasource.query.QueryCriteria;


public class MatchSiblingEntityQueryCypherStatementTest {
	private MatchSiblingEntityQueryCypherStatement instance;
	private static final String STATEMENT = "MATCH (entity:group{code:'ID'})-[:manages]->(sibling) RETURN sibling";
	
	private static final String ID = "ID";
	
	@Before
	public void setup() {
		instance = new MatchSiblingEntityQueryCypherStatement(Mockito.mock(Neo4JEntityDataSource.class));
	}
	
	@Test 
	public void prepareStatementTest() {
		QueryCriteria criteria = new QueryCriteria();
		criteria.addCriteria(EntitySearchAttributesEnum.CODE.value(), ID);
		criteria.addCriteria(EntitySearchAttributesEnum.TYPE.value(), EntityTypesEnum.GROUP.value());
		String statement = instance.prepareStatement(criteria);
		System.out.println(statement);
		assertEquals(STATEMENT, statement);
		
	}
	
	@Test 
	public void queryItemsTest() throws EntityNotFoundException, EntityNotCreatedException {	
		MatchSiblingEntityQueryCypherStatement mockInstance = Mockito.mock(MatchSiblingEntityQueryCypherStatement.class);
		Mockito.when(mockInstance.queryItems(Mockito.any(QueryCriteria.class))).thenCallRealMethod();
		
		
		StatementResult mockStatementResult = Mockito.mock(StatementResult.class);
		
		Record record = Mockito.mock(Record.class);
		Value n = Mockito.mock(Value.class);
		Value name = Mockito.mock(Value.class);
		Value code = Mockito.mock(Value.class);
		Mockito.when(name.asString()).thenReturn("Name");
		Mockito.when(code.asString()).thenReturn("Code");
		
		Mockito.when(n.get("name")).thenReturn(name);
		Mockito.when(n.get("code")).thenReturn(code);
		
		
		List<Record> recordList = new ArrayList<Record>();
		Mockito.when(mockStatementResult.list()).thenReturn(recordList);
		recordList.add(record);
		
		Mockito.when(record.get(Mockito.anyString())).thenReturn(n);
		Node node = Mockito.mock(Node.class);
		Mockito.when(n.asNode()).thenReturn(node);
		Mockito.when(node.labels()).thenReturn(new Iterable<String>() {
			
			@Override
			public Iterator<String> iterator() {
				return new ArrayList<String>(Arrays.<String>asList(new String[]{"group"})).iterator();
			}
		});
		

		
		Mockito.when(mockInstance.run(Mockito.anyString())).thenReturn(mockStatementResult);


		Mockito.when(mockInstance.toEntityList(Mockito.any(StatementResult.class))).thenCallRealMethod();

		QueryCriteria criteria = new QueryCriteria();
		criteria.addCriteria(EntitySearchAttributesEnum.CODE.value(), ID);
		criteria.addCriteria(EntitySearchAttributesEnum.TYPE.value(), EntityTypesEnum.GROUP.value());
		mockInstance.queryItems(criteria);
	}
}
