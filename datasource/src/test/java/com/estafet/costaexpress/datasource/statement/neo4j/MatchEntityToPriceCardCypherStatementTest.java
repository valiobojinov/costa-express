package com.estafet.costaexpress.datasource.statement.neo4j;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import com.estafet.costaexpress.datasource.entity.IEntityDataSource.EntitySearchAttributesEnum;
import com.estafet.costaexpress.datasource.neo4j.Neo4JPolicyDataSource;
import com.estafet.costaexpress.model.entity.types.EntityTypesEnum;
import com.estafet.datasource.query.QueryCriteria;


public class MatchEntityToPriceCardCypherStatementTest {
	private MatchEntityToPriceListCypherStatement instance;
	private static final String MATCH_ENTITY_TO_PRICECARD_STATEMENT = "MATCH (entity:site {code:'site-code'}) OPTIONAL MATCH (entity)<-[:manages*]-(group)<-[:appliesTo*]-(parent_price) OPTIONAL MATCH (price)-[:appliesTo]->(entity) WITH price, parent_price, entity, size(shortestPath((entity)-[*]-(parent_price))) as distance ORDER BY distance RETURN price, entity, parent_price, distance LIMIT 1";
															

	private QueryCriteria criteria;
	
	@Before
	public void setup() {		
		criteria = new QueryCriteria();
		instance = new MatchEntityToPriceListCypherStatement(Mockito.mock(Neo4JPolicyDataSource.class));
	}
	
	@Test 
	public void prepareStatementTest() {
		criteria.addCriteria(EntitySearchAttributesEnum.TYPE.value(), EntityTypesEnum.SITE.value());
		criteria.addCriteria(EntitySearchAttributesEnum.CODE.value(), "site-code");
		String statement = instance.prepareStatement(criteria);		
		System.out.println(statement);
		assertEquals(MATCH_ENTITY_TO_PRICECARD_STATEMENT, statement);
	}
}
