$(function(){
	var selectedNodes = [];
	var cy = window.cy = cytoscape({
		container: $('#cy'),
        boxSelectionEnabled: true,
		autounselectify: true,

		layout: {
			name: 'dagre',
		    directed: true
		},

		style: cytoscape.stylesheet()
		    .selector('node')
		      .css({
					'content': 'data(name)',
					'height': 60,
					'width': 60,
					'background-fit': 'cover',
					'border-color': '#000',
					'border-width': 3,
					'border-opacity': 0.5
		      })
		    .selector('edge')
		      .css({
					'width': 4,
					'target-arrow-shape': 'triangle',
					'line-color': '#ffaaaa',
					'target-arrow-color': '#ffaaaa',
					'curve-style': 'bezier'
			})
		   .selector('#uk')
		     .css({
					'background-image': 'images/uk-flag.jpg'
		      })
		   .selector('#costa-express')
		     .css({
					'background-image': 'http://www.costa.co.uk/media/111301/Expresspin.png'
		     })
		   .selector('#shell')
		      .css({
					'background-image': 'images/shell.jpg'
		      })
		    .selector('#machine-1')
		      .css({
					'background-image': 'images/costa-express-machine.png'
		     })
		    .selector('#machine-1')
		      .css({
					'background-image': 'images/costa-express-machine.png'
		      })
		    .selector('#machine-2')
		      .css({
					'background-image': 'images/costa-express-machine.png'
		      })
		    .selector('#machine-3')
		      .css({
					'background-image': 'images/costa-express-machine.png'
		      })
		    .selector('#machine-4')
		      .css({
					'background-image': 'images/costa-express-machine.png'
		      })
		    .selector('#city-pricelist')
		      .css({
					'background-image': 'images/pound.jpg'
		      })
		    .selector('#moto-pricelist')
		      .css({
					'background-image': 'images/pound.jpg'
		      })    
		    .selector('#bp-pricelist')
		      .css({
					'background-image': 'images/pound.jpg'
		      })
		    .selector('#bp')
		      .css({
					'background-image': 'images/bp.png'
		     }),

	 	elements: {
		    nodes: [
		    
			    { data: { id: 'costa-express-id', type: 'group', name: 'Costa Express' } }
		    ],
		    
		    edges: [
		    ]
  		},
	});

	$('#policy-details').css('display', 'none');
	
	cy.on('click','node', function(event){
		var type = event.cyTarget.data('type');
		var id = event.cyTarget.id();
		var clickTime = new Date().getTime();		

	 	$.ajax({
	 		type: 'GET',
	 		async: false,
	 		contentType : 'application/json',
	 		url: 'http://backend.neo4j.svc:80/rest-web-0.0.1-SNAPSHOT/' + type + '/' + id,
	 		success : function (data, status, xhr){
	 			if (status === 'success') {
	 				var entityContentHtml = '<table border=1><tr><td bgcolor="#98012E"><font color="#F6E9C7">Name: </font></td><td bgcolor="#98012E"><font color="#F6E9C7">'+data.name+'</font></th></tr><tr><td>ID</td><td>'+data.id+'</td></tr><tr><td>Type</td><td>'+data.type+'</td></tr></table>'; 
	 			 	$('#entityContent').html(entityContentHtml);
	 			 	$('#btnShowPolicyTab').css('display', 'block');
	 			 	$('#btnHidePolicyTab').css('display', 'none');
	 			}
	 		}	 	
	 	});  
				
	 	$.ajax({
	 		type: 'GET',
	 		async: false,
	 		contentType : 'application/json',
	 		url: 'http://localhost:8181/' + type + '/' + id + '/sibling',
	 		success : function (data, status, xhr){
	 			if (status === 'success') {
	 				if (data != null && data.elements != null) {
	 					for (var i = 0; i < data.elements.length; i ++) {
	 						var item = data.elements[i];
	 						var clickedId = item.any['id'] + ':' + item.any['type'] + item.any['name'];
	 						if (selectedNodes.indexOf(clickedId) < 0) {
				 				cy.add([
						        {group: "nodes", data: { id: item.any['id'], type: item.any['type'], name: item.any['name']}},
						        {group: "edges", data: { source: id, target: item.any['id'] }}]);
				 				selectedNodes.push(clickedId);
	 						}
	 					}	 					
	 				}
	 			}
	 		}
		}); 	
	});
});
