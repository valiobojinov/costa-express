package com.estafet.costaexpress.datasource.entity;

import java.util.Optional;

import com.estafet.datasource.DataSourceFailureException;
import com.estafet.datasource.IDataSource;
import com.estafet.datasource.IHierarchicalDataSource;
import com.estafet.datasource.node.IDataSourceNode;
import com.estafet.datasource.query.QueryCriteria;

public interface IEntityDataSource<E extends IDataSourceNode, R, P extends IDataSourceNode> extends IHierarchicalDataSource<E> {
	public static enum EntitySearchAttributesEnum {

		TYPE("type"), NAME("name"), ID("id"), CODE("code");

		private final String value;

		EntitySearchAttributesEnum(String value) {
			this.value = value;
		}

		public String value() {
			return value;
		}
	};
	
	Optional<R> findEntitiesUnderPolicy(QueryCriteria criteria, Optional<IDataSource<P>> policyDataSource) throws DataSourceFailureException;
		
}
