package com.estafet.costaexpress.datasource.entity;

import com.estafet.datasource.NodeAlreadyExistsException;

public class EntityAlreadyExistsException extends NodeAlreadyExistsException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 157868513227328445L;
	
	public EntityAlreadyExistsException(String message) {
		super(message);
	}
	
	public EntityAlreadyExistsException(String message, Throwable cause) {
		super(message, cause);
	}

}
