package com.estafet.costaexpress.datasource.entity;

import com.estafet.datasource.NodeNotFoundException;

public class EntityNotFoundException extends NodeNotFoundException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8832881499068174126L;

	public EntityNotFoundException(String message) {
		super(message);
	}
	
	public EntityNotFoundException(String message, Throwable cause) {
		super(message, cause);
	}
}
