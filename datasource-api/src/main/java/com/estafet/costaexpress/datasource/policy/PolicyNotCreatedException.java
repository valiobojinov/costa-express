package com.estafet.costaexpress.datasource.policy;

import com.estafet.datasource.NodeNotCreatedException;

public class PolicyNotCreatedException extends NodeNotCreatedException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4702546071517543511L;
	
	public PolicyNotCreatedException(String message) {
		super(message);
	}
	
	public PolicyNotCreatedException(String message, Throwable cause) {
		super(message, cause);
	}
	
	

}
