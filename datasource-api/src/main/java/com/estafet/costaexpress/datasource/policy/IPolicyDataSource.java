package com.estafet.costaexpress.datasource.policy;

import java.util.Optional;

import com.estafet.costaexpress.datasource.entity.IEntityDataSource;
import com.estafet.datasource.DataSourceFailureException;
import com.estafet.datasource.IDataSource;
import com.estafet.datasource.node.IDataSourceNode;
import com.estafet.datasource.query.QueryCriteria;

public interface IPolicyDataSource<P extends IDataSourceNode, R, E extends IDataSourceNode> extends IDataSource<P> {
	
	public static enum PolicySearchAttributesEnum {

		TYPE("type"), NAME("name"), ID("id"), CODE("code"), ITEM("item"), ITEM_TYPE("item-type");

		private final String value;

		PolicySearchAttributesEnum(String value) {
			this.value = value;
		}

		public String value() {
			return value;
		}
	};

	Optional<R> findPolicyForEntity(QueryCriteria criteria, IEntityDataSource<E, ?, P> relatedDataSource) throws DataSourceFailureException;
	void applyPolicy(String policyId, String policyType, String targetEntityId) throws DataSourceFailureException, PolicyNotFoundException;
	
	
	
}
