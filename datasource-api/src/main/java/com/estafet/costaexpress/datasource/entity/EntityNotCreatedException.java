package com.estafet.costaexpress.datasource.entity;

import com.estafet.datasource.NodeNotCreatedException;

public class EntityNotCreatedException extends NodeNotCreatedException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4702546071517543511L;
	
	public EntityNotCreatedException(String message) {
		super(message);
	}
	
	public EntityNotCreatedException(String message, Throwable cause) {
		super(message, cause);
	}
	
	

}
