package com.estafet.costaexpress.datasource.policy;

import com.estafet.datasource.NodeNotFoundException;

public class PolicyNotFoundException extends NodeNotFoundException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8832881499068174126L;

	public PolicyNotFoundException(String message) {
		super(message);
	}
	
	public PolicyNotFoundException(String message, Throwable cause) {
		super(message, cause);
	}
}
