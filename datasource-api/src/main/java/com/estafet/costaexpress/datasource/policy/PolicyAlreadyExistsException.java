package com.estafet.costaexpress.datasource.policy;

import com.estafet.datasource.NodeAlreadyExistsException;

public class PolicyAlreadyExistsException extends NodeAlreadyExistsException {

	/**
	 * 
	 */
	private static final long serialVersionUID = -969971423658450394L;

	public PolicyAlreadyExistsException(String message) {
		super(message);
		
	}
	
	public PolicyAlreadyExistsException(String message, Throwable cause) {
		super(message, cause);
		
	}

}
