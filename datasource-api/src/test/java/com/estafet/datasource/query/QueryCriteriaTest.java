package com.estafet.datasource.query;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

public class QueryCriteriaTest {
	
	private static final String VALUE = "value";
	private static final String NAME = "name";
	private static final String CODE = "code";

	@Test
	public void newQueryCriteriaTest() {
		QueryCriteria criteria = new QueryCriteria();
		assertFalse(criteria.hasCriteria());
		criteria.addCriteria(CODE, CODE);
		criteria.addCriteria(NAME, VALUE);
		assertTrue(criteria.hasCriteria());
		assertTrue(criteria.getCriteria().containsKey(CODE));
		assertEquals(CODE, criteria.getCriteria().get(CODE));
		assertEquals(VALUE, criteria.getCriteria().get(NAME));
		assertFalse(criteria.isQuerySiblingEntities());
		criteria.setQueryForSiblingEntities();
		assertTrue(criteria.isQuerySiblingEntities());
		criteria.setQueryForSiblingEntities(false);
		assertFalse(criteria.isQuerySiblingEntities());
		criteria.setQueryForSiblingEntities(true);
		assertTrue(criteria.isQuerySiblingEntities());

	}
	
}
