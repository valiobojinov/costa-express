package com.estafet.datasource;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

import com.estafet.datasource.CrossReferenceKey;

public class CrossReferenceKeyTest {
	private CrossReferenceKey instance;
	
	private static final String TARGET_CODE = "TARGET_CODE";
	private static final String TARGET_NAME = "TARGET_NAME";
	private static final String TARGET_TYPE = "TARGET_TYPE";
	
	private static final String SOURCE_CODE = "SOURCE_CODE";
	private static final String SOURCE_NAME = "SOURCE_NAME";
	private static final String SOURCE_TYPE = "SOURCE_TYPE";
	
	@Before
	public void setup() {
		instance = new CrossReferenceKey();
	}
	
	@Test
	public void  crossReferenceKeyTest() {
		instance.setTargetCode(TARGET_CODE);
		instance.setTargetName(TARGET_NAME);
		instance.setTargetType(TARGET_TYPE);
		
		instance.setId(SOURCE_CODE);
		instance.setEntityName(SOURCE_NAME);
		instance.setEntityType(SOURCE_TYPE);
		
		assertEquals(TARGET_CODE, instance.getTargetCode());
		assertEquals(TARGET_NAME, instance.getTargetName());
		assertEquals(TARGET_TYPE, instance.getTargetType());
		
		instance.setId(SOURCE_CODE);
		instance.setEntityName(SOURCE_NAME);
		instance.setEntityType(SOURCE_TYPE);
		
		
	}
}
