package com.estafet.costaexpress.webapp.lifecycle;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.util.Properties;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.eclipse.jetty.server.Request;
import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.server.handler.ErrorHandler;
import org.eclipse.jetty.servlet.FilterHolder;
import org.eclipse.jetty.servlet.FilterMapping;
import org.eclipse.jetty.servlet.ServletContextHandler;
import org.eclipse.jetty.servlet.ServletHolder;
import org.eclipse.jetty.servlets.CrossOriginFilter;
import org.glassfish.jersey.server.ResourceConfig;
import org.glassfish.jersey.servlet.ServletContainer;

public class Main {

	private static final String PACKAGES = "com.estafet.costaexpress.rest";
	
	private static Server server;
	
	static class CustomErrorHandler extends ErrorHandler {
		
		@Override
		public void handle(String target, Request baseRequest, HttpServletRequest request, HttpServletResponse response)
				throws IOException {
			// TODO Auto-generated method stub
			super.handle(target, baseRequest, request, response);
		}
		
		@Override
		public void doError(String arg0, Request arg1, HttpServletRequest arg2, HttpServletResponse arg3)
				throws IOException {
			// TODO Auto-generated method stub
			super.doError(arg0, arg1, arg2, arg3);
		}
	}
	
	private static void configureErrorHandler(Server server, ServletContextHandler contextHandler) {
		ErrorHandler errorHandler = new CustomErrorHandler();
        errorHandler.setShowStacks(true);
        server.addBean(errorHandler);
		server.setErrorHandler(errorHandler);
		contextHandler.addBean(errorHandler);
		contextHandler.setErrorHandler(errorHandler);
	}
	
	private static void configureServlets(Server server, ServletContextHandler contextHandler) throws IOException {

		ResourceConfig config = new ResourceConfig();
		config.packages(PACKAGES);
		
		ServletHolder servletHolder = new ServletHolder(new ServletContainer(config)) {
			public void handle(Request arg0, javax.servlet.ServletRequest arg1, javax.servlet.ServletResponse arg2) throws javax.servlet.ServletException ,javax.servlet.UnavailableException ,IOException {
				super.handle(arg0, arg1, arg2);
			};
		};
		
		servletHolder.dump(System.out, "ID");
		contextHandler.addServlet(servletHolder, "/*");

		servletHolder.addLifeCycleListener(new DefaultLifeCycleListener(contextHandler.getServletContext()));

	}
	
	
	
	private static void configureFilters(ServletContextHandler contextHandler) {
		
		FilterHolder holder = new FilterHolder(CustomCorsFilter.class);
		holder.setInitParameter(CrossOriginFilter.ALLOWED_ORIGINS_PARAM, "*");
		holder.setInitParameter(CrossOriginFilter.ACCESS_CONTROL_ALLOW_ORIGIN_HEADER, "Access-Control-Allow-Origin");
		holder.setInitParameter(CrossOriginFilter.ALLOWED_HEADERS_PARAM, "Access-Control-Allow-Origin");
		holder.setInitParameter(CrossOriginFilter.ALLOWED_METHODS_PARAM, "GET,POST,HEAD");
		holder.setInitParameter(CrossOriginFilter.ALLOWED_HEADERS_PARAM, "X-Requested-With,Content-Type,Accept,Origin");
		holder.setName("cross-origin");
		FilterMapping fm = new FilterMapping();
		fm.setFilterName("cross-origin");
		fm.setPathSpec("*");
		contextHandler.getServletHandler().addFilter(holder, fm);
	}

	public static void main(String[] args) throws Exception {
		Properties defaultProperties = new Properties();
		defaultProperties.load(Main.class.getClassLoader().getResourceAsStream("config.properties"));
		
		int port = Integer.valueOf(defaultProperties.getProperty("webPort"));
		String hostname = defaultProperties.getProperty("webHostname");
		
		int dbPort = Integer.valueOf(defaultProperties.getProperty("neo4jPort"));
		String dbHostname = defaultProperties.getProperty("neo4jHostname");
		
		if (args.length > 0) {
			port = Integer.valueOf(args[0]);
		} 
		
		if (args.length >= 1) {
			hostname = args[1];
		}
		

		if (args.length > 2) {
			dbPort = Integer.valueOf(args[2]);
		} 
		
		if (args.length >= 3) {
			dbHostname = args[3];
		}
		
		DeploymentConfiguration config = new DeploymentConfiguration(hostname, port, dbHostname, dbPort);
		
		Server server = startServer(config, null);
		server.join();
	}

	public static Server startServer(DeploymentConfiguration configuration, Object mutex) throws Exception {
		server = new Server(configuration.getPort());
		ServletContextHandler contextHandler = new ServletContextHandler(server, "/*");	
		contextHandler.getServletContext().setAttribute(ContextEntries.APP_DEPLOYMENT_CONFIG, configuration);
		configureFilters(contextHandler);
		configureErrorHandler(server, contextHandler);
		configureServlets(server, contextHandler);
		server.start();
		try {
			
			System.out.println("========== Jetty Server is started =======");
			if (mutex != null) {				
				
				synchronized(mutex) {
					if (server.isStarted()) {
						mutex.notify();
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace(new PrintStream(System.out));
		}
		return server;
	}
	
}
