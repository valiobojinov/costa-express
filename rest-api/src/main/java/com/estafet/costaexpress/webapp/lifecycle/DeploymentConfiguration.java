package com.estafet.costaexpress.webapp.lifecycle;

public class DeploymentConfiguration {
	private final String hostname;
	private final Integer port;
	private final String dbHostname;
	private final Integer dbPort;
	
	public DeploymentConfiguration(String hostname, Integer port, String dbHostname, Integer dbPort) {
		this.hostname = hostname;
		this.port = port;
		this.dbHostname = dbHostname;
		this.dbPort = dbPort;
	}

	public String getHostname() {
		return hostname;
	}

	public Integer getPort() {
		return port;
	}

	public String getDbHostname() {
		return dbHostname;
	}

	public Integer getDbPort() {
		return dbPort;
	}
	
}
