package com.estafet.costaexpress.webapp.lifecycle;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;

import org.apache.log4j.Logger;
import org.eclipse.jetty.util.component.LifeCycle;
import org.eclipse.jetty.util.component.LifeCycle.Listener;

import com.estafet.costaexpress.datasource.neo4j.Neo4JEntityDataSource;
import com.estafet.costaexpress.datasource.neo4j.Neo4JPolicyDataSource;
import com.estafet.datasource.initializer.DataSourceNotInitializedException;
import com.estafet.datasource.neo4j.Neo4JDataSourceInitializer;

@WebListener
public class DefaultLifeCycleListener implements Listener, ServletContextListener {
	private static Logger LOGGER = Logger.getLogger(DefaultLifeCycleListener.class);
	
	
	private ServletContext context;

	public DefaultLifeCycleListener() {
	
	}

	public DefaultLifeCycleListener(ServletContext context) {
		this.context = context;
	}
	 
	@Override
	public void lifeCycleStarting(LifeCycle lifeCycle) {
		lifeCycleStarting(context);
	}
	
	private void lifeCycleStarting(ServletContext context){
		LOGGER.trace("================ Entering method lifeCycleStarting. ====================");
		DeploymentConfiguration deploymentConfig = (DeploymentConfiguration)context.getAttribute(ContextEntries.APP_DEPLOYMENT_CONFIG);
		if(deploymentConfig == null){
			deploymentConfig = newDeploymentConfiguration();
		}
		Neo4JDataSourceInitializer initializer = new Neo4JDataSourceInitializer(deploymentConfig.getDbPort(), deploymentConfig.getDbHostname(), "neo4j", "admin");
		Neo4JPolicyDataSource policyDataSource = new Neo4JPolicyDataSource(initializer);
		Neo4JEntityDataSource entityDataSource = new Neo4JEntityDataSource(initializer);
		LOGGER.info("Starting datasource initialisation");
		try {
			entityDataSource.init();
			context.setAttribute(ContextEntries.NEO_DATASOURCE_ENTITY, entityDataSource);
			LOGGER.info("Entity datasource successfully initialised");
			policyDataSource.init();			
			context.setAttribute(ContextEntries.NEO_DATASOURCE_POLICY, policyDataSource);
			LOGGER.info("Policy datasource successfully initialised");
			
		} catch (DataSourceNotInitializedException e) {
			LOGGER.fatal("Application initialisation failed");
			LOGGER.fatal(e.getMessage(), e);
			throw new RuntimeException(e);
		}
	}
	
	private DeploymentConfiguration newDeploymentConfiguration(){
		Properties defaultProperties = new Properties();
		Properties systemProperties = new Properties();
		try {
			defaultProperties.load(Main.class.getClassLoader().getResourceAsStream("config.properties"));
			if(System.getProperty("systemConfigPath") != null) {
				systemProperties.load(new FileInputStream(System.getProperty("systemConfigPath")));
			}else{
				systemProperties.load(Main.class.getClassLoader().getResourceAsStream("system.properties"));
			}
		} catch (IOException e) {
			
		}
		
		int port = Integer.valueOf(defaultProperties.getProperty("webPort"));
		String hostname = defaultProperties.getProperty("webHostname");
		
		int dbPort = Integer.valueOf(defaultProperties.getProperty("neo4jPort"));
		String dbHostname = defaultProperties.getProperty("neo4jHostname");
		
		if(systemProperties.getProperty("webPort") != null && !systemProperties.getProperty("webPort").isEmpty()){
			port = Integer.valueOf(systemProperties.getProperty("webPort"));
		}
		if(systemProperties.getProperty("webHostname") != null && !systemProperties.getProperty("webHostname").isEmpty()){
			hostname = systemProperties.getProperty("webHostname");
		}
		if(systemProperties.getProperty("neo4jPort") != null && !systemProperties.getProperty("neo4jPort").isEmpty()){
			dbPort = Integer.valueOf(systemProperties.getProperty("neo4jPort"));
		}
		if(systemProperties.getProperty("neo4jHostname") != null && !systemProperties.getProperty("neo4jHostname").isEmpty()){
			dbHostname = systemProperties.getProperty("neo4jHostname");
		}
		
		LOGGER.info("neo4jHostname :" + dbHostname);
		LOGGER.info("neo4jPort :" + dbPort);
		LOGGER.info("webHostname :" + hostname);
		LOGGER.info("webPort :" + port);

		return new DeploymentConfiguration(hostname, port, dbHostname, dbPort);
	}

	
	@Override
	public void lifeCycleFailure(LifeCycle lifeCycle, Throwable throwable) {
		LOGGER.fatal(throwable.getMessage(), throwable);
		
	}

	@Override
	public void lifeCycleStarted(LifeCycle lifeCycle) {
		// TODO Auto-generated method stub
		
	}


	@Override
	public void lifeCycleStopped(LifeCycle lifeCycle) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void lifeCycleStopping(LifeCycle lifeCycle) {
		lifeCycleStopping(context);
		LOGGER.info("Application is stopping");
	}
	
	private void lifeCycleStopping(ServletContext context){
		Neo4JEntityDataSource entityDataSource = (Neo4JEntityDataSource)context.getAttribute(ContextEntries.NEO_DATASOURCE_ENTITY);
		Neo4JPolicyDataSource policyDataSource = (Neo4JPolicyDataSource)context.getAttribute(ContextEntries.NEO_DATASOURCE_POLICY);
		entityDataSource.close();
		policyDataSource.close();
		context.removeAttribute(ContextEntries.NEO_DATASOURCE_ENTITY);
		context.removeAttribute(ContextEntries.NEO_DATASOURCE_POLICY);
	}

	@Override
	public void contextInitialized(ServletContextEvent sce) {		
		
		lifeCycleStarting(sce.getServletContext());
	}

	@Override
	public void contextDestroyed(ServletContextEvent sce) {
		this.context = sce.getServletContext();		
		lifeCycleStopping(context);
		LOGGER.info("Servlet context destroyed");
	}
}
