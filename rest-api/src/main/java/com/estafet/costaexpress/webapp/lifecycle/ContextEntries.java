package com.estafet.costaexpress.webapp.lifecycle;

public class ContextEntries {
	public static final String NEO_DATASOURCE_ENTITY = "NEO:DATASOURCE:ENTITY";
	public static final String NEO_DATASOURCE_POLICY = "NEO:DATASOURCE:POLICY";
	public static final String NEO_DRIVER = "NEO:DRIVER";
	public static final String XML_DATASOURCE = "XML:DATASOURCE";
	public static final String APP_DEPLOYMENT_CONFIG = "APP:DEPLOYMENT_CONFIG";
}
