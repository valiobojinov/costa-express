package com.estafet.costaexpress.rest.mapper;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

import com.estafet.costaexpress.rest.generated.entity.Entity;
import com.estafet.costaexpress.rest.generated.relation.OneToManyRelationType;
import com.estafet.costaexpress.rest.generated.relation.OneToOneRelationType;
import com.estafet.costaexpress.rest.mapper.impl.GroupRestfulEntityMapper;
import com.estafet.costaexpress.rest.mapper.impl.OneToManyRelationRestfulMapper;
import com.estafet.costaexpress.rest.mapper.impl.OneToOneRelationRestfulMapper;

public class RestfulMapperProvider {
	
	private final Map<String, IRestfulEntityMapper<?, ?>> type2Mapper;
	
	public RestfulMapperProvider() {
		type2Mapper = new HashMap<String, IRestfulEntityMapper<?, ?>>();
		type2Mapper.put(Entity.class.getCanonicalName(), new GroupRestfulEntityMapper());
		type2Mapper.put(OneToManyRelationType.class.getCanonicalName(), new OneToManyRelationRestfulMapper());
		type2Mapper.put(OneToOneRelationType.class.getCanonicalName(), new OneToOneRelationRestfulMapper());
	}
	
	public <R, E> Optional<IRestfulEntityMapper<R, E>> forEntityType(Class<R> targetClass) {
		String key = targetClass.getCanonicalName();
		if (type2Mapper.containsKey(key)) {
			@SuppressWarnings("unchecked")
			IRestfulEntityMapper<R, E> mapper = (IRestfulEntityMapper<R, E>) type2Mapper.get(key);
			return Optional.of(mapper);
		}
		return Optional.<IRestfulEntityMapper<R, E>>empty();
	}

	public Map<String, IRestfulEntityMapper<?, ?>> getType2Mapper() {
		return type2Mapper;
	}
	
	public <R,E> void addType2Mapper(Class<R> key, IRestfulEntityMapper<R, E> member){
		this.type2Mapper.put(key.getCanonicalName(), member);
	}
}
