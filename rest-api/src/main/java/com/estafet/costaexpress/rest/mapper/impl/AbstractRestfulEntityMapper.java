package com.estafet.costaexpress.rest.mapper.impl;

import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import javax.ws.rs.core.MultivaluedMap;

import com.estafet.costaexpress.rest.mapper.IRestfulEntityMapper;

public abstract class AbstractRestfulEntityMapper<R, E> implements IRestfulEntityMapper<R, E> {
	public abstract class Mapper {
		
		private R entity;
		
		public abstract void map(E enity) throws UnsupportedEncodingException;
		
		protected void setSource(R entity) {
			this.entity = entity;
		}
		
		protected R getSource() {
			return entity;
		}
	}
	
	private final Map<String, Mapper> field2Mapper = new HashMap<String, Mapper>();
	protected abstract R newEntity();
	
	
	@Override	
	public R toRestfulEntity(E modelEntity, MultivaluedMap<String,String> multivaluedMap) throws UnsupportedEncodingException {
		R restEntity = newEntity();
		Set<String> requestedFields = null;
		
		if(multivaluedMap != null && !multivaluedMap.isEmpty()){
			requestedFields = multivaluedMap.keySet();
			
		}else{
			requestedFields = field2Mapper.keySet();
		}
		
		for (String field : requestedFields) {
			Mapper mapper = field2Mapper.get(field);
			mapper.setSource(restEntity);
			mapper.map(modelEntity);
		}
		return restEntity;
	}
	
	protected void addMapper(String field, Mapper mapper) {
		field2Mapper.put(field, mapper);
	}

	
}
