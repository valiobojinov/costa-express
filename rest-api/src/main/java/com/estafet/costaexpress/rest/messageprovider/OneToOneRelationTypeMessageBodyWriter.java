package com.estafet.costaexpress.rest.messageprovider;

import java.io.IOException;
import java.io.OutputStream;
import java.lang.annotation.Annotation;
import java.lang.reflect.Type;

import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.ext.Provider;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;

import com.estafet.costaexpress.model.generated.pricelist.PriceTable;
import com.estafet.costaexpress.rest.generated.entity.Entity;
import com.estafet.costaexpress.rest.generated.relation.OneToOneRelationType;

@Provider
public class OneToOneRelationTypeMessageBodyWriter extends AbstractMessageBodyWriter<OneToOneRelationType> {

	@Override
	public long getSize(OneToOneRelationType arg0, Class<?> arg1, Type arg2, Annotation[] arg3, MediaType arg4) {
		return 0;
	}

	@Override
	public boolean isWriteable(Class<?> type, Type gennericType, Annotation[] annotations, MediaType mediaType) {
		return type == OneToOneRelationType.class;
	}

	@Override
	public void writeTo(OneToOneRelationType relation, Class<?> type, Type genericType, Annotation[] annotations,
			MediaType mediaType, MultivaluedMap<String, Object> headers, OutputStream entityStream)
			throws IOException, WebApplicationException {
		try {
			write(relation, mediaType.toString(), entityStream);
		} catch (JAXBException e) {
			throw new IOException(e.getMessage(), e);
		}

	}

	@Override
	protected JAXBContext getJAXBContext() throws JAXBException {
		return JAXBContext.newInstance(Entity.class, OneToOneRelationType.class,
				PriceTable.class);
	}

}
