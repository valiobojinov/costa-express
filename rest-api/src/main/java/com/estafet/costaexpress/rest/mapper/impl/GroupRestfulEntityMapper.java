package com.estafet.costaexpress.rest.mapper.impl;

import com.estafet.costaexpress.model.entity.IPolicyAwareEntity;
import com.estafet.costaexpress.rest.generated.entity.Entity;

public class GroupRestfulEntityMapper extends AbstractRestfulEntityMapper<Entity, IPolicyAwareEntity > {
	public static final String ID = "id";
	public static final String NAME = "name";
	public static final String TYPE = "type";
	public static final String OWNER = "owner";

	private class IdMapper extends Mapper {

		@Override
		public void map(IPolicyAwareEntity entity) {
			getSource().setId(entity.getId());			
		}		
	}
	
	private class NameMapper extends Mapper {

		@Override
		public void map(IPolicyAwareEntity entity) {
			getSource().setName(entity.getNodeName());			
		}		
	}
	
	private class TypeMapper extends Mapper {

		@Override
		public void map(IPolicyAwareEntity entity) {
			getSource().setType(entity.getNodeType());		
		}		
	}
	
	private class OwnerMapper extends Mapper {

		@Override
		public void map(IPolicyAwareEntity entity) {
			getSource().setOwner(entity.getOwner());		
		}		
	}
	
	public GroupRestfulEntityMapper() {
		addMapper(ID, new IdMapper());
		addMapper(NAME, new NameMapper());
		addMapper(TYPE, new TypeMapper());
		addMapper(OWNER, new OwnerMapper());
	}
	
    @Override
	protected Entity newEntity() {
		return new Entity();
	}

}

	

	
