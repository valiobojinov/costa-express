package com.estafet.costaexpress.rest;

import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

import javax.annotation.ManagedBean;
import javax.annotation.PostConstruct;
import javax.inject.Inject;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.core.UriInfo;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;

import org.apache.log4j.Logger;
import org.codehaus.jackson.map.ObjectMapper;
import org.eclipse.jetty.servlets.CrossOriginFilter;

import com.estafet.costaexpress.datasource.entity.EntityAlreadyExistsException;
import com.estafet.costaexpress.datasource.entity.EntityNotCreatedException;
import com.estafet.costaexpress.datasource.entity.EntityNotFoundException;
import com.estafet.costaexpress.datasource.neo4j.Neo4JEntityDataSource;
import com.estafet.costaexpress.datasource.neo4j.Neo4JPolicyDataSource;
import com.estafet.costaexpress.datasource.policy.IPolicyDataSource.PolicySearchAttributesEnum;
import com.estafet.costaexpress.datasource.policy.PolicyAlreadyExistsException;
import com.estafet.costaexpress.datasource.policy.PolicyNotCreatedException;
import com.estafet.costaexpress.datasource.policy.PolicyNotFoundException;
import com.estafet.costaexpress.model.entity.IPolicyAwareEntity;
import com.estafet.costaexpress.model.entity.factory.EntityFactoryProvider;
import com.estafet.costaexpress.model.entity.factory.IEntityFactory;
import com.estafet.costaexpress.model.entity.types.EntityTypesEnum;
import com.estafet.costaexpress.model.generated.pricelist.PriceTable;
import com.estafet.costaexpress.model.policy.IPolicy;
import com.estafet.costaexpress.model.policy.PolicyItem;
import com.estafet.costaexpress.model.policy.factory.impl.PriceListPolicyFactory;
import com.estafet.costaexpress.model.policy.types.PolicyTypesEnum;
import com.estafet.costaexpress.model.relation.OneToManyRelation;
import com.estafet.costaexpress.model.relation.OneToOneRelation;
import com.estafet.costaexpress.rest.filter.KeyValueFilter;
import com.estafet.costaexpress.rest.generated.entity.Entity;
import com.estafet.costaexpress.rest.generated.relation.AnyTypeElement;
import com.estafet.costaexpress.rest.generated.relation.AnyTypeElements;
import com.estafet.costaexpress.rest.generated.relation.ObjectFactory;
import com.estafet.costaexpress.rest.generated.relation.OneToManyRelationType;
import com.estafet.costaexpress.rest.generated.relation.OneToOneRelationType;
import com.estafet.costaexpress.rest.mapper.IRestfulEntityMapper;
import com.estafet.costaexpress.rest.mapper.impl.GroupRestfulEntityMapper;
import com.estafet.costaexpress.rest.mapper.impl.OneToManyRelationRestfulMapper;
import com.estafet.costaexpress.rest.mapper.impl.OneToOneRelationRestfulMapper;
import com.estafet.costaexpress.webapp.lifecycle.ContextEntries;
import com.estafet.datasource.DataSourceFailureException;
import com.estafet.datasource.query.QueryCriteria;


@Path("/")
@ManagedBean
public class CostaExpressRestfulService {

	private static Logger LOGGER = Logger.getLogger(CostaExpressRestfulService.class);

	@Inject
	private ServletContext context;

	private EntityFactoryProvider factoryProvider;
	private Neo4JEntityDataSource entityDataSource;
	private Neo4JPolicyDataSource policyDataSource;

	public CostaExpressRestfulService() {

	}

	@PostConstruct
	public void init() {
		LOGGER.info("================= Starting method init(). PostConstruct annotation works! =================");
		entityDataSource = (Neo4JEntityDataSource) context.getAttribute(ContextEntries.NEO_DATASOURCE_ENTITY);
		policyDataSource = (Neo4JPolicyDataSource) context.getAttribute(ContextEntries.NEO_DATASOURCE_POLICY);
		factoryProvider = new EntityFactoryProvider(entityDataSource);
	}

	/**
	 * Creates a new Group entity
	 * 
	 * @param group
	 * @return
	 */
	@Consumes(MediaType.APPLICATION_XML)
	@Path("/group")
	@POST
	public Response createGroup(Entity group, @Context UriInfo uriInfo) {
		return createEntity(group, uriInfo);
	}

	/**
	 * Creates a new group sibling to the group identified by the
	 * {@code groupId} parameter
	 * 
	 * @param groupId
	 * @return
	 */

	@Consumes(MediaType.APPLICATION_XML)
	@Path("/group/{fromNodeId}")
	@POST
	public Response createGroupSibling(Entity entity, @PathParam("fromNodeId") String fromNodeId, @Context UriInfo uriInfo) {
		return createSiblingEntity(entity, EntityTypesEnum.GROUP, fromNodeId, uriInfo);
	}

	/**
	 * Retrieves a group identified by the {@code groupId} parameter
	 * 
	 * @param groupId
	 * @return
	 */
	
	
	@Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
	@Path("/group/{groupId}/{test}")
	@GET
	public Response findGroupWithBody(@PathParam("groupId") String groupId, @HeaderParam("Accept") String acceptHeader, @HeaderParam("Content-Type") String contentType, @Context HttpServletRequest request) {
			
		try {
			KeyValueFilter filter = parseRequestFilter(contentType, request.getInputStream());
			return Response.ok(filter).build();
		} catch (IOException e) {
			return Response.status(Status.BAD_REQUEST).build();
		}

		
	}
	
	@Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
	@Path("/group/{groupId}")
	@GET
	public Response findGroup(@PathParam("groupId") String groupId, @HeaderParam("Accept") String acceptHeader, @Context UriInfo uriInfo) {	
		
		return findEntity(EntityTypesEnum.GROUP, groupId, acceptHeader, uriInfo);
	}

	@Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
	@Path("/group/{groupId}/sibling")
	@GET
	public Response findGroupSibling(@PathParam("groupId") String groupId, @HeaderParam("Accept") String acceptHeader, @Context UriInfo uriInfo) {		
		return findSiblingEntities(EntityTypesEnum.GROUP, groupId, acceptHeader, uriInfo);
	}
	/**
	 * Finds the pricelist that is applied to the group with groupId
	 * @param groupId
	 * @return
	 */
	
	@Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})	
	@Path("/group/{groupId}/pricelist")
	@GET
	public Response findGroupPricelist(@PathParam("groupId") String groupId, @HeaderParam("Accept") String acceptHeader, @Context UriInfo uriInfo) {
		return findPolicyForEntity(EntityTypesEnum.GROUP, groupId, PolicyTypesEnum.PRICELIST, acceptHeader, uriInfo);
	}


	/**
	 * Deletes a group identified by the {@code groupId} parameter
	 * 
	 * @param groupId
	 * @return
	 */
	@Path("/group/{groupId}")
	@DELETE
	public Response deleteGroup(@PathParam("groupId") String groupId) {
		try {
			entityDataSource.delete(groupId, EntityTypesEnum.GROUP.value(), false);
			return Response.noContent().build();
		} catch (DataSourceFailureException e) {
			return Response.status(Status.INTERNAL_SERVER_ERROR).build();
		} catch (EntityNotFoundException e) {
			return Response.status(Status.NOT_FOUND).build();
		}
	}

	/**
	 * Establishes a connection between two groups, using {@code type} parameter
	 * to define the type of the connection, e.g. manages, has, appliesTo, etc
	 * 
	 * @param fromNodeId
	 * @param toNodeId
	 * @param type
	 * @return
	 */
	@Consumes(MediaType.APPLICATION_XML)
	@Path("/group/{fromNodeId}/{type}/{toNodeId}")
	@POST
	public Response linkToGroup(@PathParam("fromNodeId") String fromNodeId, @PathParam("toNodeId") String toNodeId,
			@PathParam("type") String type) {
		return relatateEntities(fromNodeId, EntityTypesEnum.GROUP.value(), toNodeId, type);
	}
	
	/**
	 * 
	 * @param siteId
	 * @return
	 */
	
	@Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
	@Path("/site/{siteId}")
	@GET
	public Response findSite(@PathParam("siteId") String siteId, @HeaderParam("Accept") String acceptHeader, @Context UriInfo uriInfo) {
		return findEntity(EntityTypesEnum.SITE, siteId, acceptHeader, uriInfo);
	}
	
	@Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
	@Path("/site/{siteId}/sibling")
	@GET
	public Response findSiteSibling(@PathParam("siteId") String groupId, @HeaderParam("Accept") String acceptHeader, @Context UriInfo uriInfo) {		
		return findSiblingEntities(EntityTypesEnum.SITE, groupId, acceptHeader, uriInfo);
	}
	
	/**
	 * 
	 * @param siteId
	 * @return
	 */
	@Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
	@Path("/site/{siteId}/pricelist")
	@GET
	public Response findSitePricelist(@PathParam("siteId") String siteId, @HeaderParam("Accept") String acceptHeader, @Context UriInfo uriInfo) {
		return findPolicyForEntity(EntityTypesEnum.SITE, siteId, PolicyTypesEnum.PRICELIST, acceptHeader, uriInfo);
	}

	@Consumes(MediaType.APPLICATION_XML)
	@Path("/site/{fromNodeId}/{type}/{toNodeId}")
	@POST
	public Response linkToSite(@PathParam("fromNodeId") String fromNodeId, @PathParam("toNodeId") String toNodeId,
			@PathParam("type") String type) {
		return relatateEntities(fromNodeId, EntityTypesEnum.SITE.value(), toNodeId, type);
	}

	/**
	 * Creates a sibling entity to the site identified by the {@code fromNodeId}
	 * parameter. The operation will create a has relation between the parent
	 * and the posted entity
	 * 
	 * @param entity
	 * @param fromNodeId
	 * @return
	 */
	@Consumes(MediaType.APPLICATION_XML)
	@Path("/site/{fromNodeId}")
	@POST
	public Response createSiteSibling(Entity entity, @PathParam("fromNodeId") String fromNodeId, @Context UriInfo uriInfo) {
		return createSiblingEntity(entity, EntityTypesEnum.SITE, fromNodeId, uriInfo);
	}

	/**
	 * Deletes a site identified by the {@code siteId} parameter
	 * 
	 * @param siteId
	 * @return
	 */
	@Path("/site/{siteId}")
	@DELETE
	public Response deleteSite(@PathParam("siteId") String siteId) {
		try {
			entityDataSource.delete(siteId, EntityTypesEnum.SITE.value(), false);
			return Response.noContent().build();
		} catch (DataSourceFailureException e) {
			return Response.status(Status.INTERNAL_SERVER_ERROR).build();
		} catch (EntityNotFoundException e) {
			return Response.status(Status.NOT_FOUND).build();
		}
	}
	
	/**
	 * Retrieves a machine identified by the {@code groupId} parameter
	 * 
	 * @param groupId
	 * @return
	 */
	@Consumes(MediaType.APPLICATION_XML)
	@Path("/machine/{machineId}")
	@GET
	public Response findMachine(@PathParam("machineId") String machineId, @HeaderParam("Accept") String acceptHeader, @Context UriInfo uriInfo) {
		return findEntity(EntityTypesEnum.MACHINE, machineId, acceptHeader, uriInfo);
	}
	
	/**
	 * Creates a new Machine entity
	 * 
	 * @param group
	 * @return
	 */
	@Consumes(MediaType.APPLICATION_XML)
	@Path("/machine")
	@POST
	public Response createMachine(Entity machine, @Context UriInfo uriInfo) {
		return createEntity(machine, uriInfo);
	}

	@Path("/machine/{machineId}")
	@DELETE
	public Response deleteMachine(@PathParam("machineId") String machineId) {
		try {
			entityDataSource.delete(machineId, EntityTypesEnum.MACHINE.value(), false);
			return Response.noContent().build();
		} catch (DataSourceFailureException e) {
			return Response.status(Status.INTERNAL_SERVER_ERROR).build();
		} catch (EntityNotFoundException e) {
			return Response.status(Status.NOT_FOUND).build();
		}
	}
	
	@GET
	@Path("/pricelist/{identifier}/entities")
	@Produces(MediaType.APPLICATION_XML)
	public Response findEntitiesUnderPriceList(@PathParam("identifier") String identifier, @Context UriInfo uriInfo) {
		return findEntitiesUnderPolicy(identifier, PolicyTypesEnum.PRICELIST, uriInfo);
	}//marker
	
	@GET
	@Path("/pricelist/{identifier}/entities/{type}")
	@Produces(MediaType.APPLICATION_XML)
	public Response filterEntitiesUnderPriceList(@PathParam("identifier") String identifier, @PathParam("type") String type, @Context UriInfo uriInfo) {
		Optional<EntityTypesEnum> entityType = EntityTypesEnum.fromValue(type);
		if (entityType.isPresent()) {
			return filterEntitiesUnderPolicy(identifier, PolicyTypesEnum.PRICELIST, entityType.get(), uriInfo);
		}
		return Response.status(Status.NOT_FOUND).build();
	}
	
	
	@GET
	@Path("/pricelist/{identifier}")
	@Produces(MediaType.APPLICATION_XML)
	public Response findPriceList(@PathParam("identifier") String identifier) {		
		return findPolicy(PolicyTypesEnum.PRICELIST, identifier);
	}

	/**
	 * Creates a pricetable entity
	 * 
	 * @param entity
	 * @return
	 */
	@Consumes(MediaType.APPLICATION_XML)
	@Path("/pricelist/{identifier}")
	@POST
	public Response createPriceList(@PathParam("identifier") String identifier, PriceTable priceTable, @Context UriInfo uriInfo) {
		
		PriceListPolicyFactory factory = new PriceListPolicyFactory(policyDataSource);
		
		try {
			IPolicy policy = factory.createPolicy(identifier, identifier, new PolicyItem<PriceTable>(priceTable));
			policyDataSource.create(policy);
		} catch (DataSourceFailureException e) {
			return Response.status(Status.INTERNAL_SERVER_ERROR).build();
		} catch (PolicyAlreadyExistsException e) {
			return Response.status(Status.CONFLICT).build();
		} catch (PolicyNotCreatedException e) {			
			return Response.status(Status.BAD_REQUEST).build();
		}
		
		try {
			return Response.created(getResourceLocation(PolicyTypesEnum.PRICELIST.value(), identifier, uriInfo)).entity(priceTable).build();
		} catch (URISyntaxException e) {
			/*
			 * XXX if for whatever reason URI for the created resource can't be
			 * built return 200 OK
			 */
			return Response.ok(priceTable).build();
		}
		
	}

	/**
	 * Deletes a price table identified by the {@code pricetableId parameter}
	 * 
	 * @param priceTableId
	 * @return
	 */
	@Path("/pricelist/{pricetableId}")
	@DELETE
	public Response deletePriceList(@PathParam("pricetableId") String priceTableId) {
		try {
			policyDataSource.delete(priceTableId, PolicyTypesEnum.PRICELIST.value());
			return Response.noContent().build();
		} catch (DataSourceFailureException e) {
			return Response.status(Status.INTERNAL_SERVER_ERROR).build();
		} catch (PolicyNotFoundException e) {
			return Response.status(Status.NOT_FOUND).build();
		}
	}

	/**
	 * Applies the pricetable identified by the {@code pricetableId} parameter
	 * to the entity identified by the {@code entityId} parameter. The operation
	 * will create an apply to the relation from the pricetable to the entity
	 * 
	 * @param pricetableId
	 * @param entityId
	 * @return
	 */
	@Path("/pricelist/{pricetableid}/{entityId}")
	@POST
	public Response applyPriceTable(@PathParam("pricetableid") String pricetableId,
			@PathParam("entityId") String entityId) {
		try {
			policyDataSource.applyPolicy(pricetableId, PolicyTypesEnum.PRICELIST.value(), entityId);
			return Response.ok().build();
		} catch (DataSourceFailureException e) {
			return Response.status(Status.INTERNAL_SERVER_ERROR).build();
		} catch (PolicyNotFoundException e) {
			return Response.status(Status.NOT_FOUND).build();

		}
	}
	
	@Path("/pricelist/{pricetableId}/entities")
	public Response findPriceListEntities(@PathParam("pricetableId") String priceTableId) {
		return Response.ok().build();
		//TODO should be deleted
	}
	


	private Response createEntity(Entity restEntity, UriInfo uriInfo) {
		Optional<EntityTypesEnum> entityType = EntityTypesEnum.fromValue(restEntity.getType());
		if (entityType.isPresent()) {
			EntityTypesEnum type = entityType.get();
			IEntityFactory<IPolicyAwareEntity> factory = factoryProvider.<IPolicyAwareEntity>getFactory(type);
			try {
				IPolicyAwareEntity entity = factory.createEntity(restEntity.getName(), restEntity.getId());
				entityDataSource.create(entity);
				return Response.created(getResourceLocation(restEntity, uriInfo)).entity(restEntity).build();

			} catch (EntityNotCreatedException e) {
				LOGGER.error(e.getMessage(), e);
				return Response.status(Status.BAD_REQUEST).build();
			} catch (URISyntaxException e) {
				LOGGER.warn(e.getMessage(), e);
				/*
				 * XXX if for whatever reason URI for the created resource can't
				 * be built return 200 OK
				 */
				return Response.ok(restEntity).build();
			} catch (DataSourceFailureException e) {
				return Response.status(Status.INTERNAL_SERVER_ERROR).build();
			} catch (EntityAlreadyExistsException e) {				
				return Response.status(Status.CONFLICT).build();
			}
		} else {
			return Response.status(Status.BAD_REQUEST).build();
		}
	}

	private Response relatateEntities(String fromNodeId, String fromNodeType, String toNodeId, String type) {
		try {
			entityDataSource.createConnection(fromNodeId, fromNodeType, toNodeId, type);
			return Response.noContent().build();
		} catch (DataSourceFailureException e) {
			return Response.status(Status.INTERNAL_SERVER_ERROR).build();
		}		
	}

	private Response createSiblingEntity(Entity restEntity, EntityTypesEnum parentType, String fromNodeId, UriInfo uriInfo) {
		Optional<EntityTypesEnum> entityType = EntityTypesEnum.fromValue(restEntity.getType());
		if (entityType.isPresent()) {
			IEntityFactory<IPolicyAwareEntity> factory = factoryProvider
					.<IPolicyAwareEntity>getFactory(entityType.get());
			try {
				IPolicyAwareEntity entity = factory.createEntity(restEntity.getName(), restEntity.getId());// why createEntity not consume restEntity object?
				entityDataSource.createSibling(entity, fromNodeId, parentType.value());
				return Response.created(getResourceLocation(restEntity, uriInfo)).entity(restEntity).build();

			} catch (DataSourceFailureException e) {
				return Response.status(Status.INTERNAL_SERVER_ERROR).build();
			} catch (URISyntaxException e) {
				/*
				 * XXX if for whatever reason URI for the created resource can't
				 * be built return 200 OK
				 */
				return Response.ok(restEntity).build();
			} catch (EntityNotCreatedException e) {
				LOGGER.error(e.getMessage(), e);
				return Response.status(Status.BAD_REQUEST).build();
			} catch (EntityAlreadyExistsException e) {
				return Response.status(Status.CONFLICT).build();
			}
		} else {
			// cannot create entity out of unknown type return bad request
			return Response.status(Status.BAD_REQUEST).build();
		}
	}

	private Response findEntity(EntityTypesEnum type, String nodeId, String mediaType, UriInfo uriInfo) {
		
		String acceptedMediaType = acceptedMediaType(mediaType);
		try {

			QueryCriteria criteria = new QueryCriteria();
			criteria.addCriteria(PolicySearchAttributesEnum.CODE.value(), nodeId);
			criteria.addCriteria(PolicySearchAttributesEnum.TYPE.value(), type.value());
			Optional<IPolicyAwareEntity> result = entityDataSource.find(criteria);
			if (result.isPresent()) {
				IRestfulEntityMapper<Entity, IPolicyAwareEntity> mapper = new GroupRestfulEntityMapper();
				Entity restEntity = mapper.toRestfulEntity(result.get(), uriInfo.getQueryParameters());

				return Response.ok(restEntity, acceptedMediaType).header(CrossOriginFilter.ACCESS_CONTROL_ALLOW_ORIGIN_HEADER, "*").build(); 
			}
			return Response.status(Status.NOT_FOUND).build();
		} catch (DataSourceFailureException e) {

			return Response.status(Status.INTERNAL_SERVER_ERROR).build();
		} catch (UnsupportedEncodingException e) {
			return Response.status(Status.INTERNAL_SERVER_ERROR).build();
		}
	}

	private String acceptedMediaType(String mediaType) {
		if (mediaType == null || "".equals(mediaType)) {
			return MediaType.APPLICATION_XML;
		} else if (mediaType.equals("*/*")) {
			return MediaType.APPLICATION_JSON;
		} else {
			if (mediaType.contains(MediaType.APPLICATION_XML)) {
				return MediaType.APPLICATION_XML;
			} else if (mediaType.contains(MediaType.APPLICATION_JSON)) {
				return MediaType.APPLICATION_JSON;
			}
		}
		return MediaType.APPLICATION_XML;
	}
	
	private Response findSiblingEntities(EntityTypesEnum type, String nodeId, String mediaType, UriInfo uriInfo) {
		
		String acceptedMediaType = acceptedMediaType(mediaType);

		QueryCriteria criteria = new QueryCriteria();
		criteria.addCriteria(PolicySearchAttributesEnum.CODE.value(), nodeId);
		criteria.addCriteria(PolicySearchAttributesEnum.TYPE.value(), type.value());
		//criteria.setQueryForSiblingEntities(true);

		try {
			List<IPolicyAwareEntity> list = entityDataSource.findSiblingEntities(criteria);
			AnyTypeElements entities = new AnyTypeElements();
			IRestfulEntityMapper<Entity, IPolicyAwareEntity> mapper = new GroupRestfulEntityMapper();
			for (IPolicyAwareEntity entity : list) {
				AnyTypeElement restEntity = new AnyTypeElement();
				Entity mappedEntity = mapper.toRestfulEntity(entity, uriInfo.getQueryParameters());
				restEntity.setAny(mappedEntity);
				entities.getElements().add(restEntity);
			}

			return Response.ok(entities, acceptedMediaType).header(CrossOriginFilter.ACCESS_CONTROL_ALLOW_ORIGIN_HEADER, "*").build();

		} catch (DataSourceFailureException e) {
			return Response.status(Status.INTERNAL_SERVER_ERROR).build();
		} catch (UnsupportedEncodingException e) {
			return Response.status(Status.INTERNAL_SERVER_ERROR).build();
		}
	}

	private Response findPolicy(PolicyTypesEnum type, String nodeId) {
		try {
			QueryCriteria criteria = new QueryCriteria();
			criteria.addCriteria(PolicySearchAttributesEnum.CODE.value(), nodeId);
			criteria.addCriteria(PolicySearchAttributesEnum.TYPE.value(), type.value());
			Optional<IPolicy> result = policyDataSource.find(criteria);
			if (result.isPresent()) {
				return Response.ok(result.get().getPolicyItem().getItem()).build();
			}
			return Response.status(Status.NOT_FOUND).build();
		} catch (DataSourceFailureException e) {

			return Response.status(Status.INTERNAL_SERVER_ERROR).build();
		}
	}
	
	private Response findEntitiesUnderPolicy(String code, PolicyTypesEnum policyType, UriInfo uriInfo) {
		QueryCriteria criteria = new QueryCriteria();
		criteria.addCriteria(PolicySearchAttributesEnum.TYPE.value(), policyType.value());
		criteria.addCriteria(PolicySearchAttributesEnum.CODE.value(), code);
		
		try {
			Optional<OneToManyRelation> result = entityDataSource.findEntitiesUnderPolicy(criteria, Optional.of(policyDataSource));
			if (result.isPresent()) {
				IRestfulEntityMapper<OneToManyRelationType, OneToManyRelation> mapper = new OneToManyRelationRestfulMapper();
				
				return Response.ok().entity(mapper.toRestfulEntity(result.get(), uriInfo.getQueryParameters())).build();
			}
			return Response.status(Status.NOT_FOUND).build();
		} catch (DataSourceFailureException e) {
			return Response.status(Status.INTERNAL_SERVER_ERROR).build();
		} catch (UnsupportedEncodingException e) {
			return Response.status(Status.INTERNAL_SERVER_ERROR).build();
		}
	}
	
	private Response filterEntitiesUnderPolicy(String code, PolicyTypesEnum policyType, EntityTypesEnum entityType, UriInfo uriInfo) {
		QueryCriteria criteria = new QueryCriteria();
		criteria.addCriteria(PolicySearchAttributesEnum.TYPE.value(), policyType.value());
		criteria.addCriteria(PolicySearchAttributesEnum.CODE.value(), code);
		
		try {
			Optional<OneToManyRelation> result = entityDataSource.findEntitiesUnderPolicy(criteria, Optional.of(policyDataSource));
			if (result.isPresent()) {
				return Response.ok().entity(filterOneToManyRelation(result.get(), entityType, uriInfo)).build();
			}
			return Response.status(Status.NOT_FOUND).build();
		} catch (DataSourceFailureException e) {
			return Response.status(Status.INTERNAL_SERVER_ERROR).build();
		} catch (UnsupportedEncodingException e) {
			return Response.status(Status.INTERNAL_SERVER_ERROR).build();
		}
	}
	
	private Response findPolicyForEntity(EntityTypesEnum entityType, String code, PolicyTypesEnum policyType, String acceptHeader, UriInfo uriInfo)  {
		QueryCriteria criteria = new QueryCriteria();
		criteria.addCriteria(PolicySearchAttributesEnum.TYPE.value(), entityType.value());
		criteria.addCriteria(PolicySearchAttributesEnum.CODE.value(), code);
		
		try {
			Optional<OneToOneRelation> result = policyDataSource.findPolicyForEntity(criteria, entityDataSource);
			if (result.isPresent()) {
				IRestfulEntityMapper<OneToOneRelationType, OneToOneRelation> mapper = new OneToOneRelationRestfulMapper();
				return Response.ok().entity(mapper.toRestfulEntity(result.get(), uriInfo.getQueryParameters())).build();
			}
			return Response.status(Status.NOT_FOUND).build();
		} catch (DataSourceFailureException e) {
			return Response.status(Status.INTERNAL_SERVER_ERROR).build();
		} catch (UnsupportedEncodingException e) {
			return Response.status(Status.INTERNAL_SERVER_ERROR).build();
		} 
		
	}
	
	private KeyValueFilter parseRequestFilter(String contentType, InputStream filterStream) throws IOException {
		
		if (contentType == null || "".equals(contentType)) {
			try {
				JAXBContext jaxbContext = JAXBContext.newInstance(KeyValueFilter.class);				
				return (KeyValueFilter)jaxbContext.createUnmarshaller().unmarshal(filterStream);
			} catch (JAXBException e) {
				throw new IOException(e.getMessage(), e);
			}
		} else if (contentType.startsWith(MediaType.APPLICATION_JSON)) {
			ObjectMapper mapper = new ObjectMapper();			
			try {
				return mapper.readValue(filterStream, KeyValueFilter.class);				
			} catch (IOException e) {
				throw e;
			}
		} else {
			try {
				JAXBContext jaxbContext = JAXBContext.newInstance(KeyValueFilter.class);				
				return (KeyValueFilter)jaxbContext.createUnmarshaller().unmarshal(filterStream);
			} catch (JAXBException e) {
				throw new IOException(e.getMessage(), e);
			}

		}
	}

	URI getResourceLocation(Entity entity, UriInfo uriInfo) throws URISyntaxException {
		return uriInfo.getRequestUriBuilder().uri(entity.getType()).path(entity.getId()).build();
	}
	
	URI getResourceLocation(String type, String identifier, UriInfo uriInfo) throws URISyntaxException {
		return uriInfo.getRequestUriBuilder().uri(type).path(identifier).build();
	}
	
	
	private OneToManyRelationType filterOneToManyRelation(OneToManyRelation relation, EntityTypesEnum type, UriInfo uriInfo) throws UnsupportedEncodingException {
		OneToManyRelationType relationType = new ObjectFactory().createOneToManyRelationType();
		AnyTypeElement source = new AnyTypeElement();
		source.setAny(relation.getSource().getPolicyItem().getItem());
		relationType.setSource(source);
		IRestfulEntityMapper<Entity, IPolicyAwareEntity> mapper = new GroupRestfulEntityMapper();
		for (IPolicyAwareEntity entity : relation.getTarget()) {
			if (type.equals(entity.getType())) {
				AnyTypeElement target = new AnyTypeElement();
				Entity restEntity = mapper.toRestfulEntity(entity, uriInfo.getQueryParameters());
				target.setAny(restEntity);
				relationType.getTarget().add(target);
			}
		}
		return relationType;
	}
	


}
