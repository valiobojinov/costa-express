package com.estafet.costaexpress.rest.messageprovider;

import java.io.IOException;
import java.io.OutputStream;
import java.lang.annotation.Annotation;
import java.lang.reflect.Type;

import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.ext.Provider;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;

import com.estafet.costaexpress.rest.filter.KeyValueFilter;
@Provider
public class FilterMessageBodyWriter extends AbstractMessageBodyWriter<KeyValueFilter> {

	@Override
	public long getSize(KeyValueFilter arg0, Class<?> arg1, Type arg2, Annotation[] arg3, MediaType arg4) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public boolean isWriteable(Class<?> type, Type arg1, Annotation[] arg2, MediaType arg3) {
		return type == KeyValueFilter.class;
	}

	public void writeTo(KeyValueFilter filter, Class<?> type, Type genericType, Annotation[] annotations,
			MediaType mediaType, MultivaluedMap<String, Object> headers, OutputStream entityStream) throws IOException, WebApplicationException {
		try {
			write(filter, mediaType.toString(), entityStream);
		} catch (JAXBException e) {
			throw new IOException(e.getMessage(), e);
		}
	}

	@Override
	protected JAXBContext getJAXBContext() throws JAXBException {
		return  JAXBContext.newInstance(KeyValueFilter.class);
	}

}
