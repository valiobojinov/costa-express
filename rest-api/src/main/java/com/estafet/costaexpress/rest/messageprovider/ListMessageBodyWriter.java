package com.estafet.costaexpress.rest.messageprovider;

import java.io.IOException;
import java.io.OutputStream;
import java.lang.annotation.Annotation;
import java.lang.reflect.Type;

import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.ext.Provider;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;

import com.estafet.costaexpress.rest.generated.entity.Entity;
import com.estafet.costaexpress.rest.generated.relation.AnyTypeElement;
import com.estafet.costaexpress.rest.generated.relation.AnyTypeElements;

@Provider
public class ListMessageBodyWriter extends AbstractMessageBodyWriter<AnyTypeElements> {

	

	@Override
	protected JAXBContext getJAXBContext() throws JAXBException {
		return JAXBContext.newInstance(AnyTypeElements.class, AnyTypeElement.class, Entity.class);
	}

	@Override
	public long getSize(AnyTypeElements arg0, Class<?> arg1, Type arg2, Annotation[] arg3, MediaType arg4) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public boolean isWriteable(Class<?> type, Type arg1, Annotation[] arg2, MediaType arg3) {
		return type == AnyTypeElements.class;
	}

	@Override
	public void writeTo(AnyTypeElements elements, Class<?> type, Type genericType, Annotation[] annotations,
			MediaType mediaType, MultivaluedMap<String, Object> headers, OutputStream entityStream) throws IOException, WebApplicationException {
		try {
			write(elements, mediaType.toString(), entityStream);
		} catch (JAXBException e) {
			throw new IOException(e.getMessage(), e);
		}
		
	}

}
