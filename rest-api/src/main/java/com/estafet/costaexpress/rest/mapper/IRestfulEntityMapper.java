package com.estafet.costaexpress.rest.mapper;

import java.io.UnsupportedEncodingException;

import javax.ws.rs.core.MultivaluedMap;

public interface IRestfulEntityMapper<R, E> {
	R toRestfulEntity(E modelEntity, MultivaluedMap<String,String> multivaluedMap) throws UnsupportedEncodingException;
}
