package com.estafet.costaexpress.rest.mapper.impl;

import java.io.UnsupportedEncodingException;

import javax.ws.rs.core.MultivaluedMap;

import com.estafet.costaexpress.model.entity.IPolicyAwareEntity;
import com.estafet.costaexpress.model.relation.OneToManyRelation;
import com.estafet.costaexpress.rest.generated.entity.Entity;
import com.estafet.costaexpress.rest.generated.relation.AnyTypeElement;
import com.estafet.costaexpress.rest.generated.relation.ObjectFactory;
import com.estafet.costaexpress.rest.generated.relation.OneToManyRelationType;

public class OneToManyRelationRestfulMapper extends AbstractRestfulEntityMapper<OneToManyRelationType, OneToManyRelation> {
	
	public OneToManyRelationType toRestfulEntity(OneToManyRelation relation, MultivaluedMap<String,String> multivaluedMap) throws UnsupportedEncodingException {
		OneToManyRelationType relationType = newEntity();
		AnyTypeElement source = new AnyTypeElement();
		source.setAny(relation.getSource().getPolicyItem().getItem());
		relationType.setSource(source);
		
		for (IPolicyAwareEntity entity : relation.getTarget()) {
			AnyTypeElement target = new AnyTypeElement();
			GroupRestfulEntityMapper mapper = new GroupRestfulEntityMapper();
			Entity restEntity = mapper.toRestfulEntity(entity, multivaluedMap);
			target.setAny(restEntity);
			relationType.getTarget().add(target);
		}
		return relationType;
	}
	
	@Override
	protected OneToManyRelationType newEntity() {
		return new ObjectFactory().createOneToManyRelationType();
	}

}
