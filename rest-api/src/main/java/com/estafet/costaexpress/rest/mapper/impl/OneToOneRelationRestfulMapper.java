package com.estafet.costaexpress.rest.mapper.impl;

import java.io.UnsupportedEncodingException;

import javax.ws.rs.core.MultivaluedMap;

import com.estafet.costaexpress.model.relation.OneToOneRelation;
import com.estafet.costaexpress.rest.generated.entity.Entity;
import com.estafet.costaexpress.rest.generated.relation.AnyTypeElement;
import com.estafet.costaexpress.rest.generated.relation.ObjectFactory;
import com.estafet.costaexpress.rest.generated.relation.OneToOneRelationType;

public class OneToOneRelationRestfulMapper extends AbstractRestfulEntityMapper<OneToOneRelationType, OneToOneRelation> {

	public OneToOneRelationType toRestfulEntity(OneToOneRelation relation, MultivaluedMap<String,String> multivaluedMap) throws UnsupportedEncodingException {
		OneToOneRelationType relationType = newEntity();
		AnyTypeElement source = new AnyTypeElement();
		GroupRestfulEntityMapper mapper = new GroupRestfulEntityMapper();
		Entity restEntity = mapper.toRestfulEntity(relation.getSource(), multivaluedMap);
		source.setAny(restEntity);
		AnyTypeElement target = new AnyTypeElement();
		target.setAny(relation.getTarget().getPolicyItem().getItem());
		relationType.setSource(source);
		relationType.setTarget(target);
		return relationType;
	}
	
	@Override
	protected OneToOneRelationType newEntity() {
		return new ObjectFactory().createOneToOneRelationType();
	}

}
