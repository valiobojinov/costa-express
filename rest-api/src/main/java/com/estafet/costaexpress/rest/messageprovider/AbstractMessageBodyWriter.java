package com.estafet.costaexpress.rest.messageprovider;

import java.io.IOException;
import java.io.OutputStream;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.ext.MessageBodyWriter;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;

import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.map.ObjectMapper;

import com.estafet.costaexpress.rest.generated.entity.Entity;

public abstract class AbstractMessageBodyWriter<T> implements MessageBodyWriter<T> {

	protected void write (T entity, String mediaType, OutputStream entityStream) throws JAXBException, JsonGenerationException, IOException {
				
			if (MediaType.APPLICATION_XML.equals(mediaType.toString())) {
				JAXBContext jaxbContext = getJAXBContext();
				jaxbContext.createMarshaller().marshal(entity, entityStream);	
			} else if (MediaType.APPLICATION_JSON.equals(mediaType.toString())) {
				ObjectMapper mapper = new ObjectMapper();
				mapper.writeValue(entityStream, entity);
			} else {
				JAXBContext jaxbContext = JAXBContext.newInstance(Entity.class);
				jaxbContext.createMarshaller().marshal(entity, entityStream);
			}

		}
	protected abstract JAXBContext getJAXBContext() throws JAXBException;
		
	
}
