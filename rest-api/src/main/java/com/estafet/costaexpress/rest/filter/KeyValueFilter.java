package com.estafet.costaexpress.rest.filter;

import java.util.HashMap;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "Filter")
@XmlAccessorType(XmlAccessType.FIELD)
public class KeyValueFilter extends HashMap<String, String> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1362164280591542736L;

}
