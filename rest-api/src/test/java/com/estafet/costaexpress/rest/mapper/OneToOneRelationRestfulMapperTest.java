package com.estafet.costaexpress.rest.mapper;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.io.UnsupportedEncodingException;
import java.math.BigDecimal;

import javax.ws.rs.core.MultivaluedHashMap;
import javax.ws.rs.core.MultivaluedMap;

import org.junit.Before;
import org.junit.Test;

import com.estafet.costaexpress.model.entity.IPolicyAwareEntity;
import com.estafet.costaexpress.model.entity.impl.GroupEntity;
import com.estafet.costaexpress.model.generated.pricelist.PriceTable;
import com.estafet.costaexpress.model.generated.pricelist.PriceTable.Price;
import com.estafet.costaexpress.model.policy.IPolicy;
import com.estafet.costaexpress.model.policy.impl.PriceListPolicy;
import com.estafet.costaexpress.model.relation.OneToOneRelation;
import com.estafet.costaexpress.rest.generated.entity.Entity;
import com.estafet.costaexpress.rest.generated.relation.OneToOneRelationType;
import com.estafet.costaexpress.rest.mapper.impl.OneToOneRelationRestfulMapper;

public class OneToOneRelationRestfulMapperTest {
	OneToOneRelationRestfulMapper instance;
	MultivaluedMap<String, String> requestParams;
	
	@Before
	public void setup() {
		instance = new OneToOneRelationRestfulMapper();
		requestParams = new MultivaluedHashMap<>();
	}
	
	@Test
	public void toRestfulEntityTest() throws UnsupportedEncodingException{
		
		PriceTable priceTable = new PriceTable();
		Price price = new Price();
		price.setDoubleShotPrice(new BigDecimal("10.4"));
		price.setSyrupPrice(new BigDecimal("41.2"));
		priceTable.getPrice().add(price);
		IPolicyAwareEntity group = new GroupEntity("Entity Test Name", "Entity Test ID");
		IPolicy policy = new PriceListPolicy("Policy test name", "Policy test ID", priceTable);
		
		OneToOneRelation source = new OneToOneRelation(group, policy);
		
		OneToOneRelationType result = instance.toRestfulEntity(source, requestParams);
		assertNotNull(result);
		assertNotNull(result.getTarget());
		assertNotNull(result.getTarget().getAny());
		assertTrue(result.getTarget().getAny() instanceof PriceTable);
		PriceTable targetPriceTable = (PriceTable) result.getTarget().getAny();
		assertEquals(priceTable.getPrice(), targetPriceTable.getPrice());

		assertNotNull(result.getSource());
		assertNotNull(result.getSource().getAny());
		assertTrue(result.getSource().getAny() instanceof Entity);
		assertEquals(((Entity) result.getSource().getAny()).getName(), group.getNodeName());
	}
}
