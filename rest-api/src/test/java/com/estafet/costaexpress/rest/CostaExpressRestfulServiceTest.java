package com.estafet.costaexpress.rest;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintStream;
import java.net.URI;
import java.net.URISyntaxException;

import org.eclipse.jetty.server.Server;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import com.estafet.costaexpress.datasource.neo4j.Neo4JEntityDataSource;
import com.estafet.costaexpress.datasource.neo4j.Neo4JPolicyDataSource;
import com.estafet.costaexpress.test.tools.EmbeddedNeo4JDataSource;
import com.estafet.costaexpress.webapp.lifecycle.DeploymentConfiguration;
import com.estafet.costaexpress.webapp.lifecycle.Main;

import io.restassured.RestAssured;
import io.restassured.http.ContentType;
//TODO investigate random failure probably race condition
public class CostaExpressRestfulServiceTest {
	
	private static EmbeddedNeo4JDataSource<Neo4JEntityDataSource, Neo4JPolicyDataSource> dataSource = new EmbeddedNeo4JDataSource<>();
	private static Server jettyServer;
	
	private static class ServerRunnable implements Runnable {
		private final Object mutex;
		
		ServerRunnable(Object mutex) {
			this.mutex = mutex;
		}
		
		@Override
		public void run() {

			DeploymentConfiguration config = new DeploymentConfiguration("localhost", 8282, "localhost", EmbeddedNeo4JDataSource.DB_PORT);
			try {
				
				jettyServer = Main.startServer(config, mutex);
				
			} catch (IOException | IllegalStateException e) {
				e.printStackTrace(new PrintStream(System.out));
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		public boolean isStarted() {
			return jettyServer != null && jettyServer.isStarted();
		}
		
		
	}		
	
	
	
	
	private static Thread thread;
	
	@BeforeClass
	public static void startServer() throws IOException {
		Object mutex = new Object();
		dataSource.startDatabase();
		ServerRunnable embeddedJetty = new ServerRunnable(mutex);
		thread = new Thread(embeddedJetty);
		thread.start();
		synchronized(mutex) {
			try {	
				while(!embeddedJetty.isStarted()) {
					mutex.wait();				
				}
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		System.out.println("========== Before Class is finished =======");
	}

	
	@Test
	public void getRequestWithBodyTest() throws URISyntaxException, IOException {
		URI ukGroupId = new URI("http://localhost:8282/group/test/1234");
		InputStream entityStream = getClass().getClassLoader().getResourceAsStream("entity/filter.json");
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		int data = -1;
		while ((data = entityStream.read()) != -1) {
			baos.write(data);
		}
		
		RestAssured.given().contentType(ContentType.JSON)
			.body(baos.toByteArray())
			.when().get(ukGroupId);			

	}
	
	@Test
	public void restGetPostDeleteGroupTest() throws URISyntaxException, IOException {
		URI ukGroupId = new URI("http://localhost:8282/group/id6667");
		RestAssured.when().get(ukGroupId).then().statusCode(404);	
		URI groupUrl = new URI("http://localhost:8282/group/");
		InputStream entityStream = getClass().getClassLoader().getResourceAsStream("entity/group.xml");
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		int data = -1;
		while ((data = entityStream.read()) != -1) {
			baos.write(data);
		}
		
		RestAssured.given().contentType(ContentType.XML)
			.body(baos.toByteArray())
			.when().post(groupUrl)			
			.then().statusCode(201);
			///
		
		RestAssured.when().get(ukGroupId).then().statusCode(200);	
		System.out.println("========== Tests finished ==========");
	}
	
	@AfterClass
	public static void afterTestMethod() throws IOException{
		try {
			jettyServer.stop();
			jettyServer.destroy();
			System.out.println("========== Jetty Server is destroyed =======");
		} catch (Exception e) {
			e.printStackTrace();
		}
		dataSource.shutdownDatabase();
		System.out.println("========== After class is finished =======");
	}
}
