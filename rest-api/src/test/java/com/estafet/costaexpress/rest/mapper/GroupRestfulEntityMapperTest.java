package com.estafet.costaexpress.rest.mapper;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.io.UnsupportedEncodingException;

import javax.ws.rs.core.MultivaluedHashMap;
import javax.ws.rs.core.MultivaluedMap;

import org.junit.Before;
import org.junit.Test;

import com.estafet.costaexpress.model.entity.impl.GroupEntity;
import com.estafet.costaexpress.rest.generated.entity.Entity;
import com.estafet.costaexpress.rest.mapper.impl.GroupRestfulEntityMapper;

public class GroupRestfulEntityMapperTest {
	
	GroupRestfulEntityMapper instance;
	MultivaluedMap<String, String> requestParams;
	
	@Before
	public void setup() {
		instance = new GroupRestfulEntityMapper();
		requestParams = new MultivaluedHashMap<>();
	}
	
	@Test
	public void toRestfulEntityTest() throws UnsupportedEncodingException{
		
		GroupEntity source = new GroupEntity("Entity Test Name", "Entity Test ID");
		
		requestParams.add(GroupRestfulEntityMapper.ID, "dummy value");
		Entity restEntity = instance.toRestfulEntity(source, requestParams);
		assertNotNull(restEntity.getId());
		assertEquals(source.getId(), restEntity.getId());
		
		requestParams.add(GroupRestfulEntityMapper.NAME, "dummy value");
		restEntity = instance.toRestfulEntity(source, requestParams);
		assertNotNull(restEntity.getName());
		assertEquals(source.getName(), restEntity.getName());
		requestParams.clear();
		
		requestParams.add(GroupRestfulEntityMapper.TYPE, "dummy value");
		restEntity = instance.toRestfulEntity(source, requestParams);
		assertNotNull(restEntity.getType());
		assertEquals(source.getNodeType(), restEntity.getType());
		
		requestParams.add(GroupRestfulEntityMapper.OWNER, "dummy value");
		restEntity = instance.toRestfulEntity(source, requestParams);
		assertNotNull(restEntity.getOwner());
		assertEquals(source.getOwner(), restEntity.getOwner());
		
		requestParams.clear();
		restEntity = instance.toRestfulEntity(source, requestParams);
		assertEquals(source.getId(), restEntity.getId());
		assertEquals(source.getName(), restEntity.getName());
		assertEquals(source.getNodeType(), restEntity.getType());
		assertEquals(source.getOwner(), restEntity.getOwner());
	}
	
}
