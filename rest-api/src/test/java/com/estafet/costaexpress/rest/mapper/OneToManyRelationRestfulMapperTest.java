package com.estafet.costaexpress.rest.mapper;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.io.UnsupportedEncodingException;
import java.math.BigDecimal;

import javax.ws.rs.core.MultivaluedHashMap;
import javax.ws.rs.core.MultivaluedMap;

import org.junit.Before;
import org.junit.Test;

import com.estafet.costaexpress.model.entity.IPolicyAwareEntity;
import com.estafet.costaexpress.model.entity.impl.GroupEntity;
import com.estafet.costaexpress.model.entity.impl.SiteEntity;
import com.estafet.costaexpress.model.generated.pricelist.PriceTable;
import com.estafet.costaexpress.model.generated.pricelist.PriceTable.Price;
import com.estafet.costaexpress.model.policy.IPolicy;
import com.estafet.costaexpress.model.policy.impl.PriceListPolicy;
import com.estafet.costaexpress.model.relation.OneToManyRelation;
import com.estafet.costaexpress.rest.generated.relation.OneToManyRelationType;
import com.estafet.costaexpress.rest.mapper.impl.OneToManyRelationRestfulMapper;

public class OneToManyRelationRestfulMapperTest {
	OneToManyRelationRestfulMapper instance;
	MultivaluedMap<String, String> requestParams;
	
	@Before
	public void setup() {
		instance = new OneToManyRelationRestfulMapper();
		requestParams = new MultivaluedHashMap<>();
	}
	
	@Test
	public void toRestfulEntityTest() throws UnsupportedEncodingException{
		
		OneToManyRelation source = new OneToManyRelation();
		
		PriceTable priceTable = new PriceTable();
		Price price = new Price();
		price.setDoubleShotPrice(new BigDecimal("10.4"));
		price.setSyrupPrice(new BigDecimal("41.2"));
		priceTable.getPrice().add(price);
		IPolicyAwareEntity group = new GroupEntity("Entity Test Name", "Entity Test ID");
		IPolicyAwareEntity site = new SiteEntity("Site test name", "Site test ID");
		IPolicy policy = new PriceListPolicy("Policy test name", "Policy test ID", priceTable);
		
		source.getTarget().add(group);
		source.getTarget().add(site);
		source.setSource(policy);
		
		OneToManyRelationType result = instance.toRestfulEntity(source, requestParams);
		assertNotNull(result);
		assertNotNull(result.getSource());
		assertNotNull(result.getSource().getAny());
		assertTrue(result.getSource().getAny() instanceof PriceTable);
		PriceTable targetPriceTable = (PriceTable) result.getSource().getAny();
		assertEquals(priceTable.getPrice(), targetPriceTable.getPrice());

		assertNotNull(result.getTarget());
		assertEquals(2, result.getTarget().size());
		assertNotNull(result.getTarget().get(0).getAny());
	}
}
