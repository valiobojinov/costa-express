package com.estafet.costaexpress.rest.mapper;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.io.UnsupportedEncodingException;
import java.util.Optional;

import org.junit.Before;
import org.junit.Test;

import com.estafet.costaexpress.model.entity.IPolicyAwareEntity;
import com.estafet.costaexpress.rest.generated.entity.Entity;
import com.estafet.costaexpress.rest.mapper.impl.GroupRestfulEntityMapper;

public class RestfulMapperProviderTest {
	RestfulMapperProvider instance;
	
	@Before
	public void setup() {
		instance = new RestfulMapperProvider();
	}
	
	@Test
	public void mapperTest() throws UnsupportedEncodingException{
		IRestfulEntityMapper<Entity, IPolicyAwareEntity> source = new GroupRestfulEntityMapper();
		instance.addType2Mapper(Entity.class, source);
		
		Optional<IRestfulEntityMapper<Entity, IPolicyAwareEntity>> target = instance.forEntityType(Entity.class);
		assertTrue(target.isPresent());
		assertTrue(target.get() instanceof GroupRestfulEntityMapper);
		assertEquals(source, target.get());
		
		assertFalse(instance.forEntityType(GroupRestfulEntityMapper.class).isPresent());
		
		@SuppressWarnings("unchecked")
		IRestfulEntityMapper<Entity, IPolicyAwareEntity> mapper = (IRestfulEntityMapper<Entity, IPolicyAwareEntity>) 
				instance.getType2Mapper().get(Entity.class.getCanonicalName());
		assertNotNull(mapper);
		assertTrue(mapper instanceof GroupRestfulEntityMapper);
		assertEquals(source, mapper);
	}
}
