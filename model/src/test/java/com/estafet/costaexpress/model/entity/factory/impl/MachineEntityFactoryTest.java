package com.estafet.costaexpress.model.entity.factory.impl;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import com.estafet.costaexpress.datasource.entity.EntityNotCreatedException;
import com.estafet.costaexpress.datasource.entity.IEntityDataSource;
import com.estafet.costaexpress.model.entity.IPolicyAwareEntity;
import com.estafet.costaexpress.model.entity.impl.MachineEntity;
import com.estafet.datasource.DataSourceFailureException;


public class MachineEntityFactoryTest {
	
	private MachineEntityFactory instance;
	private IEntityDataSource<IPolicyAwareEntity, ?, ?> dataSource;
	
	private static final String NAME = "MachineName";
	private static final String ID = "MachineID";
	
	@SuppressWarnings("unchecked")
	@Before
	public void setup() {
		dataSource = Mockito.mock(IEntityDataSource.class);
		instance = new MachineEntityFactory(dataSource);
	}
	
	@Test
	public void createEntityTest() throws EntityNotCreatedException {
		MachineEntity machine = instance.createEntity(NAME, ID);
		assertEquals(ID, machine.getId());
		assertEquals(NAME, machine.getName());
		assertEquals(NAME, machine.getNodeName());
		assertFalse(machine.isComposite());

	}
	
	@Test
	public void createFromDataSouceTest() throws EntityNotCreatedException, DataSourceFailureException {
		
		MachineEntity machine = instance.createFromDataSource(NAME, ID);
		assertEquals(ID, machine.getId());
		assertEquals(NAME, machine.getName());
		assertEquals(NAME, machine.getNodeName());	
		assertFalse(machine.isComposite());		
	}
	
	
}
