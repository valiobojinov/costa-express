package com.estafet.costaexpress.model.policy.types;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.Optional;

import org.junit.Test;

public class PolicyTypesEnumTest {
	@Test
	public void fromValueTest() {
		for (PolicyTypesEnum value : PolicyTypesEnum.values()) {
			assertTrue(PolicyTypesEnum.fromValue(value.value()).isPresent());
			assertEquals(value, PolicyTypesEnum.fromValue(value.value()).get());
		}
	}
	
	@Test
	public void fromValueNegativeTest() {
		Optional<PolicyTypesEnum> result = PolicyTypesEnum.fromValue("ANY");
		assertFalse(result.isPresent());
	}
}
