package com.estafet.costaexpress.model.entity.factory.impl;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import com.estafet.costaexpress.datasource.entity.EntityNotCreatedException;
import com.estafet.costaexpress.datasource.entity.IEntityDataSource;
import com.estafet.costaexpress.model.entity.IPolicyAwareEntity;
import com.estafet.costaexpress.model.entity.impl.SiteEntity;
import com.estafet.datasource.DataSourceFailureException;
import com.estafet.datasource.query.QueryCriteria;


public class SiteEntityFactoryTest {
	
	private SiteEntityFactory instance;
	private IEntityDataSource<IPolicyAwareEntity, ?, ?> dataSource;
	
	private static final String NAME = "MachineName";
	private static final String ID = "MachineID";
	
	@SuppressWarnings("unchecked")
	@Before
	public void setup() {
		dataSource = Mockito.mock(IEntityDataSource.class);
		instance = new SiteEntityFactory(dataSource);
	}
	
	@Test
	public void createEntityTest() throws EntityNotCreatedException {
		SiteEntity site = instance.createEntity(NAME, ID);
		assertEquals(ID, site.getId());
		assertEquals(NAME, site.getName());
		assertEquals(NAME, site.getNodeName());
		assertTrue(site.isComposite());
	}
	
	@Test
	public void createFromDataSouceTest() throws EntityNotCreatedException, DataSourceFailureException {
		
		List<IPolicyAwareEntity> siblings = new ArrayList<IPolicyAwareEntity>();
		siblings.add(Mockito.mock(IPolicyAwareEntity.class));
		
		Mockito.when(dataSource.findSiblingEntities(Mockito.any(QueryCriteria.class))).thenReturn(siblings);
		
		SiteEntity site = instance.createFromDataSource(NAME, ID);
		assertEquals(ID, site.getId());
		assertEquals(NAME, site.getName());
		assertEquals(NAME, site.getNodeName());
		assertTrue(site.isComposite());
		
		assertEquals(1, site.getSiblingEntities().size());

		
	}
	
	@Test(expected = EntityNotCreatedException.class)
	public void createFromDataSouceFailedTest() throws EntityNotCreatedException, DataSourceFailureException {
		
		List<IPolicyAwareEntity> siblings = new ArrayList<IPolicyAwareEntity>();
		siblings.add(Mockito.mock(IPolicyAwareEntity.class));
		
		Mockito.when(dataSource.findSiblingEntities(Mockito.any(QueryCriteria.class))).thenThrow(new DataSourceFailureException("forced"));
		
		instance.createFromDataSource(NAME, ID);

		
	}
}
