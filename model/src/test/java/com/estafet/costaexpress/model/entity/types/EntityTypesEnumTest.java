package com.estafet.costaexpress.model.entity.types;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.Optional;

import org.junit.Test;

public class EntityTypesEnumTest {
	@Test
	public void fromValueTest() {
		for (EntityTypesEnum value : EntityTypesEnum.values()) {
			assertTrue(EntityTypesEnum.fromValue(value.value()).isPresent());
			assertEquals(value, EntityTypesEnum.fromValue(value.value()).get());
		}
	}
	
	@Test
	public void fromValueNegativeTest() {
		Optional<EntityTypesEnum> result = EntityTypesEnum.fromValue("ANY");
		assertFalse(result.isPresent());
	}
}
