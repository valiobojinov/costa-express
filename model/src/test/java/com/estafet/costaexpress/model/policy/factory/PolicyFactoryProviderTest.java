package com.estafet.costaexpress.model.policy.factory;

import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import com.estafet.costaexpress.datasource.policy.IPolicyDataSource;
import com.estafet.costaexpress.model.policy.IPolicy;
import com.estafet.costaexpress.model.policy.factory.impl.PriceListPolicyFactory;
import com.estafet.costaexpress.model.policy.types.PolicyTypesEnum;
import com.estafet.costaexpress.model.relation.OneToOneRelation;

public class PolicyFactoryProviderTest {

private PolicyFactoryProvider instance;
	
	
	@Before
	public void setup() {
		@SuppressWarnings("unchecked")
		IPolicyDataSource<IPolicy, OneToOneRelation, ?> dataSource = Mockito.mock(IPolicyDataSource.class);
		instance = new PolicyFactoryProvider(dataSource);		
	}
	
	@Test 
	public void factoryRegistrationTest() {
			IPolicyFactory<?, ?> priceListFactory = instance.getFactory(PolicyTypesEnum.PRICELIST);
			assertTrue(priceListFactory instanceof PriceListPolicyFactory);
	}

	
	@Test(expected=IllegalArgumentException.class) 
	public void getUnregisterFactoryTest() {
		instance.unregisterFactory(PolicyTypesEnum.RECIPIE);
		instance.getFactory(PolicyTypesEnum.RECIPIE);
	}
	
}
