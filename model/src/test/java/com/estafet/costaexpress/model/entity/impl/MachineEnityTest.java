package com.estafet.costaexpress.model.entity.impl;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;

import com.estafet.costaexpress.model.entity.types.EntityTypesEnum;
import com.estafet.costaexpress.model.generated.pricelist.PriceTable;
import com.estafet.costaexpress.model.policy.IPolicy;
import com.estafet.costaexpress.model.policy.IPolicyRules;
import com.estafet.costaexpress.model.policy.PolicyItem;
import com.estafet.costaexpress.model.policy.impl.PriceListPolicy;
import com.estafet.costaexpress.model.policy.types.PolicyTypesEnum;

public class MachineEnityTest {
	private static final String NAME = "MachineName";
	private static final String ID = "MachineID";
	
	private MachineEntity machine;
	private IPolicy dummyPolicy = new IPolicy() {

		@Override
		public String getId() {
			return ID;
		}

		@Override
		public String getNodeName() {
			return NAME;
		}

		@Override
		public String getNodeType() {
			return PolicyTypesEnum.RECIPIE.value();
		}

		@Override
		public IPolicyRules getRules() {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public PolicyItem<?> getPolicyItem() {
			return null;
		}

		@Override
		public PolicyTypesEnum getPolicyType() {
			return PolicyTypesEnum.RECIPIE;
		}
		
	};
	
	@Before
	public void setup() {
		machine = new MachineEntity(NAME, ID);
	}
	
	@Test
	public void isPolcyApplicableTest() {
		assertFalse(machine.isApplicable(dummyPolicy));
		machine.apply(dummyPolicy);
		assertNull(machine.getPriceTable());
		PriceTable priceTable = new PriceTable();
		PriceListPolicy priceListPolicy = new PriceListPolicy("name", "id", priceTable);
		assertTrue(machine.isApplicable(priceListPolicy));
		machine.apply(priceListPolicy);
		assertEquals(priceTable, machine.getPriceTable());
		assertEquals(EntityTypesEnum.MACHINE, machine.getType());
		assertEquals(EntityTypesEnum.MACHINE.value(), machine.getNodeType());
		
	}
}
