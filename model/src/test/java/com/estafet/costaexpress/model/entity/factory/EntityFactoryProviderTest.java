package com.estafet.costaexpress.model.entity.factory;

import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import com.estafet.costaexpress.datasource.entity.IEntityDataSource;
import com.estafet.costaexpress.model.entity.IPolicyAwareEntity;
import com.estafet.costaexpress.model.entity.factory.EntityFactoryProvider;
import com.estafet.costaexpress.model.entity.factory.IEntityFactory;
import com.estafet.costaexpress.model.entity.factory.impl.GroupEntityFactory;
import com.estafet.costaexpress.model.entity.factory.impl.MachineEntityFactory;
import com.estafet.costaexpress.model.entity.factory.impl.SiteEntityFactory;
import com.estafet.costaexpress.model.entity.types.EntityTypesEnum;

public class EntityFactoryProviderTest {

	private EntityFactoryProvider instance;
	
	
	@Before
	public void setup() {
		@SuppressWarnings("unchecked")
		IEntityDataSource<IPolicyAwareEntity, ?, ?> dataSource = Mockito.mock(IEntityDataSource.class);
		instance = new EntityFactoryProvider(dataSource);		
	}
	
	@Test 
	public void factoryRegistrationTest() {
			IEntityFactory<?> groupFactory = instance.getFactory(EntityTypesEnum.GROUP);
			assertTrue(groupFactory instanceof GroupEntityFactory);
			
			IEntityFactory<?> machineFactory = instance.getFactory(EntityTypesEnum.MACHINE);
			assertTrue(machineFactory instanceof MachineEntityFactory);
			
			IEntityFactory<?> siteFactory = instance.getFactory(EntityTypesEnum.SITE);
			assertTrue(siteFactory instanceof SiteEntityFactory);
	}

	
	@Test(expected=IllegalArgumentException.class) 
	public void getUnregisterFactoryTest() {
		
		instance.unregisterFactory(EntityTypesEnum.MACHINE);
		instance.getFactory(EntityTypesEnum.MACHINE);
	}
}
