package com.estafet.costaexpress.model.entity.connections;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertFalse;

import java.util.Optional;

import org.junit.Test;

public class ConnectionTypesEnumTest {
	@Test
	public void fromValueTest() {
		for (ConnectionTypesEnum value : ConnectionTypesEnum.values()) {
			assertTrue(ConnectionTypesEnum.fromType(value.type()).isPresent());
			assertEquals(value, ConnectionTypesEnum.fromType(value.type()).get());
		}
	}
	
	@Test
	public void fromValueNegativeTest() {
		Optional<ConnectionTypesEnum> result = ConnectionTypesEnum.fromType("ANY");
		assertFalse(result.isPresent());
	}
}
