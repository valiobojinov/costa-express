package com.estafet.costaexpress.model.relation;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.util.Arrays;

import org.junit.Before;
import org.junit.Test;

import com.estafet.costaexpress.model.entity.IPolicyAwareEntity;
import com.estafet.costaexpress.model.entity.impl.GroupEntity;
import com.estafet.costaexpress.model.generated.pricelist.PriceTable;
import com.estafet.costaexpress.model.policy.IPolicy;
import com.estafet.costaexpress.model.policy.impl.PriceListPolicy;

public class OneToManyRelationTest {
	private static final String ENTITY_NAME_1 = "entity:name:1";
	private static final String ENTITY_ID_1 = "entity:id:1";
	private static final String ENTITY_NAME_2 = "entity:name:2";
	private static final String ENTITY_ID_2 = "entity:id:2";
	private static final String POLICY_NAME = "policy:name";
	private static final String POLICY_ID = "policy:id";
	private OneToManyRelation relation;
	private IPolicyAwareEntity policyAwareEntity;
	private IPolicy policy;
	
	@Before
	public void setup() {
		policyAwareEntity = new GroupEntity(ENTITY_NAME_1, ENTITY_ID_1);
		policy = new PriceListPolicy(POLICY_NAME, POLICY_ID, new PriceTable());
		relation = new OneToManyRelation();
	}
	
	@Test
	public void relationTest() {
		assertNull(relation.getSource());
		assertTrue(relation.getTarget().isEmpty());
		relation.setSource(policy);
		relation.getTarget().add(policyAwareEntity);
		assertEquals(policy, relation.getSource());
		assertEquals(policyAwareEntity, relation.getTarget().get(0));
		assertEquals(1, relation.getTarget().size());		
		relation.setTarget(Arrays.<IPolicyAwareEntity>asList(new IPolicyAwareEntity[]{new GroupEntity(ENTITY_NAME_2, ENTITY_ID_2), policyAwareEntity}));
		assertEquals(2, relation.getTarget().size());
	}

	

}
