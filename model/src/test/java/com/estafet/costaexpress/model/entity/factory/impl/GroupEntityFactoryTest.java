package com.estafet.costaexpress.model.entity.factory.impl;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import com.estafet.costaexpress.datasource.entity.EntityNotCreatedException;
import com.estafet.costaexpress.datasource.entity.IEntityDataSource;
import com.estafet.costaexpress.model.entity.IPolicyAwareEntity;
import com.estafet.costaexpress.model.entity.impl.GroupEntity;
import com.estafet.costaexpress.model.entity.types.EntityTypesEnum;
import com.estafet.datasource.DataSourceFailureException;
import com.estafet.datasource.query.QueryCriteria;


public class GroupEntityFactoryTest {
	
	private GroupEntityFactory instance;
	private IEntityDataSource<IPolicyAwareEntity, ?, ?> dataSource;
	
	private static final String NAME = "GroupName";
	private static final String ID = "GroupID";
	
	@SuppressWarnings("unchecked")
	@Before
	public void setup() {
		dataSource = Mockito.mock(IEntityDataSource.class);
		instance = new GroupEntityFactory(dataSource);
	}
	
	@Test
	public void createEntityTest() throws EntityNotCreatedException {
		GroupEntity group = instance.createEntity(NAME, ID);
		assertEquals(ID, group.getId());
		assertEquals(NAME, group.getName());
		assertEquals(NAME, group.getNodeName());
		assertEquals(EntityTypesEnum.GROUP.value(), group.getNodeType());
		assertEquals(EntityTypesEnum.GROUP, group.getType());
		assertTrue(group.isComposite());
	}
	
	@Test
	public void createFromDataSouceTest() throws EntityNotCreatedException, DataSourceFailureException {
		
		List<IPolicyAwareEntity> siblings = new ArrayList<IPolicyAwareEntity>();
		siblings.add(Mockito.mock(IPolicyAwareEntity.class));
		
		Mockito.when(dataSource.findSiblingEntities(Mockito.any(QueryCriteria.class))).thenReturn(siblings);
		
		GroupEntity group = instance.createFromDataSource(NAME, ID);
		assertEquals(ID, group.getId());
		assertEquals(NAME, group.getName());
		assertEquals(NAME, group.getNodeName());	
		assertEquals(EntityTypesEnum.GROUP.value(), group.getNodeType());
		assertEquals(EntityTypesEnum.GROUP, group.getType());
		assertTrue(group.isComposite());

		assertEquals(1, group.getSiblingEntities().size());
		
	}
	
	@Test(expected = EntityNotCreatedException.class)
	public void createFromDataSouceFailedTest() throws EntityNotCreatedException, DataSourceFailureException {
		
		List<IPolicyAwareEntity> siblings = new ArrayList<IPolicyAwareEntity>();
		siblings.add(Mockito.mock(IPolicyAwareEntity.class));
		Mockito.when(dataSource.findSiblingEntities(Mockito.any(QueryCriteria.class))).thenThrow(new DataSourceFailureException("forced"));
		instance.createFromDataSource(NAME, ID);
	}
}
