package com.estafet.costaexpress.model.policy.factory.impl;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import com.estafet.costaexpress.datasource.entity.EntityNotCreatedException;
import com.estafet.costaexpress.datasource.policy.IPolicyDataSource;
import com.estafet.costaexpress.datasource.policy.PolicyNotCreatedException;
import com.estafet.costaexpress.model.entity.IPolicyAwareEntity;
import com.estafet.costaexpress.model.generated.pricelist.PriceTable;
import com.estafet.costaexpress.model.policy.IPolicy;
import com.estafet.costaexpress.model.policy.PolicyItem;
import com.estafet.costaexpress.model.policy.impl.PriceListPolicy;
import com.estafet.costaexpress.model.policy.types.PolicyTypesEnum;
import com.estafet.datasource.DataSourceFailureException;
import com.estafet.datasource.query.QueryCriteria;


public class PriceListPolicyFactoryTest {
	
	private PriceListPolicyFactory instance;
	private IPolicyDataSource<IPolicy, ?, ?> dataSource;
	
	private static final String NAME = "PriceListName";
	private static final String ID = "PriceListIDID";
	
	@SuppressWarnings("unchecked")
	@Before
	public void setup() {
		dataSource = Mockito.mock(IPolicyDataSource.class);
		instance = new PriceListPolicyFactory(dataSource);
	}
	
	@Test
	public void createPolicyTest() throws PolicyNotCreatedException {
		
		PriceListPolicy policy = instance.createPolicy(NAME, ID, new PolicyItem<PriceTable>(new PriceTable()));
		assertEquals(ID, policy.getId());
		assertEquals(NAME, policy.getNodeName());
		assertNotNull(policy.getPolicyItem().getItem());
		assertEquals(PolicyTypesEnum.PRICELIST, policy.getPolicyType());
		assertEquals(PolicyTypesEnum.PRICELIST.value(), policy.getNodeType());
	}
	
	@Test(expected=PolicyNotCreatedException.class)
	public void createFromDataSourceNegativeTest() throws DataSourceFailureException, PolicyNotCreatedException {
		
		Mockito.when(dataSource.find(Mockito.any(QueryCriteria.class))).thenReturn(Optional.<IPolicy>empty());
		instance.createFromDataSource(NAME, ID);
	}
	
	@Test
	public void createFromDataSouceTest() throws DataSourceFailureException, PolicyNotCreatedException {
		
		List<IPolicyAwareEntity> siblings = new ArrayList<IPolicyAwareEntity>();
		siblings.add(Mockito.mock(IPolicyAwareEntity.class));
		PriceListPolicy priceList = new PriceListPolicy(NAME, ID, new PriceTable());
		Mockito.when(dataSource.find(Mockito.any(QueryCriteria.class))).thenReturn(Optional.of(priceList));
		
		PriceListPolicy policy = instance.createFromDataSource(NAME, ID);
		
		assertEquals(ID, policy.getId());
		assertEquals(NAME, policy.getNodeName());
		assertNotNull(policy.getPolicyItem().getItem());
		assertEquals(PolicyTypesEnum.PRICELIST, policy.getPolicyType());
		assertEquals(PolicyTypesEnum.PRICELIST.value(), policy.getNodeType());
	}	
	
	
	@Test(expected = PolicyNotCreatedException.class)
	public void createFromDataSouceFailedTest() throws EntityNotCreatedException, DataSourceFailureException, PolicyNotCreatedException {
		
		List<IPolicyAwareEntity> siblings = new ArrayList<IPolicyAwareEntity>();
		siblings.add(Mockito.mock(IPolicyAwareEntity.class));		
		Mockito.when(dataSource.find(Mockito.any(QueryCriteria.class))).thenThrow(new DataSourceFailureException("forced"));
		instance.findPriceTable("NaNID");
		
	}
}
