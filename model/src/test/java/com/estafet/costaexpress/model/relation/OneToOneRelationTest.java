package com.estafet.costaexpress.model.relation;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import org.junit.Before;
import org.junit.Test;

import com.estafet.costaexpress.model.entity.IPolicyAwareEntity;
import com.estafet.costaexpress.model.entity.impl.GroupEntity;
import com.estafet.costaexpress.model.generated.pricelist.PriceTable;
import com.estafet.costaexpress.model.policy.IPolicy;
import com.estafet.costaexpress.model.policy.impl.PriceListPolicy;

public class OneToOneRelationTest {
	private static final String ENTITY_NAME = "entity:name";
	private static final String ENTITY_ID = "entity:id";
	private static final String POLICY_NAME = "policy:name";
	private static final String POLICY_ID = "policy:id";
	private OneToOneRelation relation;
	private IPolicyAwareEntity policyAwareEntity;
	private IPolicy policy;
	
	@Before
	public void setup() {
		policyAwareEntity = new GroupEntity(ENTITY_NAME, ENTITY_ID);
		policy = new PriceListPolicy(POLICY_NAME, POLICY_ID, new PriceTable());
		relation = new OneToOneRelation(policyAwareEntity, policy);
	}
	
	@Test
	public void relationTest() {
		assertEquals(policyAwareEntity, relation.getSource());
		assertEquals(policy, relation.getTarget());
		assertNotNull(relation.getTarget().getPolicyItem().getItem());
	}
}
