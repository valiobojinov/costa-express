package com.estafet.costaexpress.model.relation;

import java.util.ArrayList;
import java.util.List;

import com.estafet.costaexpress.model.entity.IPolicyAwareEntity;
import com.estafet.costaexpress.model.policy.IPolicy;

public class OneToManyRelation {
	private List<IPolicyAwareEntity> entities = new ArrayList<IPolicyAwareEntity>();
	private IPolicy policy;
	
	public OneToManyRelation() {
		
	}

	public IPolicy getSource() {
		return policy;
	}

	public void setSource(IPolicy policy) {
		this.policy = policy;
	}
	
	public List<IPolicyAwareEntity> getTarget() {
		return entities;
	}
	
	public void setTarget(List<IPolicyAwareEntity> entities) {
		this.entities = entities;
	}
}
