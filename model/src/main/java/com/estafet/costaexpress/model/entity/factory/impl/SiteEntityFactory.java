package com.estafet.costaexpress.model.entity.factory.impl;

import java.util.List;

import com.estafet.costaexpress.datasource.entity.EntityNotCreatedException;
import com.estafet.costaexpress.datasource.entity.IEntityDataSource;
import com.estafet.costaexpress.model.entity.IPolicyAwareEntity;
import com.estafet.costaexpress.model.entity.impl.SiteEntity;

public class SiteEntityFactory extends AbstractEntityFactory<SiteEntity>{
	public SiteEntityFactory(IEntityDataSource<IPolicyAwareEntity, ?, ?> dataSource) {
		super(dataSource);
	}
	
	public SiteEntity createEntity(String name, String id) throws EntityNotCreatedException {
		return new SiteEntity(name, id);
	}

	@Override
	public SiteEntity createFromDataSource(String name, String id) throws EntityNotCreatedException {
		SiteEntity site = createEntity(name, id);

		List<IPolicyAwareEntity> siblingEntities = findSiblingEntities(site);
		site.getSiblingEntities().addAll(siblingEntities);

		return site;		
	}
}
