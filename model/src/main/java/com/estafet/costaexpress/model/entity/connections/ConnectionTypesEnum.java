package com.estafet.costaexpress.model.entity.connections;

import java.util.Optional;

public enum ConnectionTypesEnum {
	MANAGES("manages"), 
	HAS("has"),
	APPLIES_TO("appliesTo");
	
	private final String type;
	
	ConnectionTypesEnum(String type) {
		this.type = type;
	}
	
	public String type() {
		return type;
	}
	
	public static Optional<ConnectionTypesEnum> fromType(String type) {
		for (ConnectionTypesEnum value : ConnectionTypesEnum.values()) {
			if (value.type().equals(type)) {
				return Optional.of(value);
			}
		}
		return Optional.empty();
	}
}
