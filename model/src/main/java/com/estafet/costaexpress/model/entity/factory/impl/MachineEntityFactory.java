package com.estafet.costaexpress.model.entity.factory.impl;

import com.estafet.costaexpress.datasource.entity.EntityNotCreatedException;
import com.estafet.costaexpress.datasource.entity.IEntityDataSource;
import com.estafet.costaexpress.model.entity.IPolicyAwareEntity;
import com.estafet.costaexpress.model.entity.impl.MachineEntity;

public class MachineEntityFactory extends AbstractEntityFactory<MachineEntity> {

	public MachineEntityFactory(IEntityDataSource<IPolicyAwareEntity, ?, ?> dataSource) {
		super(dataSource);

	}

	@Override
	public MachineEntity createEntity(String name, String id) throws EntityNotCreatedException {
		MachineEntity machine = new MachineEntity(name, id);
		//machine.enablePolicyChangedNotifications(new PolicyChangedNotifier(getPolicyChangedListener()));
		return machine;
	}

	@Override
	public MachineEntity createFromDataSource(String name, String id) throws EntityNotCreatedException {
		return createEntity(name, id);
	}
}
