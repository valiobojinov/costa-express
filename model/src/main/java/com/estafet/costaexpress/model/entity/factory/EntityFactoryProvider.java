package com.estafet.costaexpress.model.entity.factory;

import java.text.MessageFormat;
import java.util.HashMap;
import java.util.Map;

import com.estafet.costaexpress.datasource.entity.IEntityDataSource;
import com.estafet.costaexpress.model.entity.IPolicyAwareEntity;
import com.estafet.costaexpress.model.entity.factory.impl.GroupEntityFactory;
import com.estafet.costaexpress.model.entity.factory.impl.MachineEntityFactory;
import com.estafet.costaexpress.model.entity.factory.impl.SiteEntityFactory;
import com.estafet.costaexpress.model.entity.types.EntityTypesEnum;

public class EntityFactoryProvider {	
	
	private final Map<EntityTypesEnum, IEntityFactory<? extends IPolicyAwareEntity>> factories = new HashMap<EntityTypesEnum, IEntityFactory<? extends IPolicyAwareEntity>>();
	
	public EntityFactoryProvider(IEntityDataSource<IPolicyAwareEntity, ?, ?> dataSource) {
		registerFactory(EntityTypesEnum.GROUP, new GroupEntityFactory(dataSource));
		registerFactory(EntityTypesEnum.SITE, new SiteEntityFactory(dataSource));
		registerFactory(EntityTypesEnum.MACHINE, new MachineEntityFactory(dataSource));
	}
	
	@SuppressWarnings("unchecked")
	public <E extends IPolicyAwareEntity> IEntityFactory<E> getFactory(EntityTypesEnum type) {
		if (factories.containsKey(type)) {
			return (IEntityFactory<E>) factories.get(type);
		}
		
		throw new IllegalArgumentException(MessageFormat.format("Factory for type: {0} is not available", type.value()));
		
	}	
	
	public <E extends IPolicyAwareEntity> void registerFactory(EntityTypesEnum type, IEntityFactory<E> factory) {
		factories.putIfAbsent(type, factory);
	}
	
	public boolean unregisterFactory(EntityTypesEnum type) {
		return factories.remove(type) != null;
	}	
}
