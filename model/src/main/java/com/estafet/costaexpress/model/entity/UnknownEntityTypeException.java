package com.estafet.costaexpress.model.entity;

public class UnknownEntityTypeException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = -42449711375711865L;
	
	public UnknownEntityTypeException(String message) {
		super(message);
	}

}
