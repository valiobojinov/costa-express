package com.estafet.costaexpress.model.policy.factory.impl;

import java.util.Optional;

import com.estafet.costaexpress.datasource.policy.IPolicyDataSource;
import com.estafet.costaexpress.datasource.policy.IPolicyDataSource.PolicySearchAttributesEnum;
import com.estafet.costaexpress.datasource.policy.PolicyNotCreatedException;
import com.estafet.costaexpress.model.generated.pricelist.PriceTable;
import com.estafet.costaexpress.model.policy.IPolicy;
import com.estafet.costaexpress.model.policy.PolicyItem;
import com.estafet.costaexpress.model.policy.impl.PriceListPolicy;
import com.estafet.costaexpress.model.policy.types.PolicyTypesEnum;
import com.estafet.datasource.DataSourceFailureException;
import com.estafet.datasource.query.QueryCriteria;

public class PriceListPolicyFactory extends AbstractPolicyFactory<PriceListPolicy, PriceTable> {

	public PriceListPolicyFactory(IPolicyDataSource<IPolicy, ?, ?> dataSource) {
		super(dataSource);
	}
	

	@Override
	public PriceListPolicy createFromDataSource(String name, String id) throws PolicyNotCreatedException {
		PriceTable priceTable = findPriceTable(id);		
		PriceListPolicy priceListPolicy = createPolicy(name, id, new PolicyItem<PriceTable>(priceTable));
		return priceListPolicy;
	}


	@Override
	public PriceListPolicy createPolicy(String name, String id, PolicyItem<PriceTable> item) throws PolicyNotCreatedException {
		return new PriceListPolicy(name, id, item.getItem());
		
	}

	PriceTable findPriceTable(String id) throws PolicyNotCreatedException {
		QueryCriteria criteria = new QueryCriteria();
		criteria.addCriteria(PolicySearchAttributesEnum.CODE.value(), id);
		criteria.addCriteria(PolicySearchAttributesEnum.TYPE.value(), PolicyTypesEnum.PRICELIST.value());
		try {
			Optional<IPolicy> result = getDataSource().find(criteria);
			if (result.isPresent()) {
				return ((PriceListPolicy)result.get()).getPolicyItem().getItem();
			}
		} catch (DataSourceFailureException e) {
			throw new PolicyNotCreatedException(e.getMessage(), e);
		}
		
		throw new PolicyNotCreatedException("Unable to find price list policy with id:" + id);

	}
}
