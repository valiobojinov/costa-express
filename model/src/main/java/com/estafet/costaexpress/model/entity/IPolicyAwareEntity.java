package com.estafet.costaexpress.model.entity;

import com.estafet.costaexpress.model.entity.listener.PolicyChangedNotifier;
import com.estafet.costaexpress.model.policy.IPolicy;

public interface IPolicyAwareEntity extends IEntity {
	void applyPolicy(IPolicy policy);
	void enablePolicyChangedNotifications(PolicyChangedNotifier policyChangedNotifier);
	void disablePolicyChangedNotifications();
}
