package com.estafet.costaexpress.model.entity.factory.impl;

import java.util.List;

import com.estafet.costaexpress.datasource.entity.EntityNotCreatedException;
import com.estafet.costaexpress.datasource.entity.IEntityDataSource;
import com.estafet.costaexpress.datasource.entity.IEntityDataSource.EntitySearchAttributesEnum;
import com.estafet.costaexpress.model.entity.IPolicyAwareEntity;
import com.estafet.costaexpress.model.entity.factory.IEntityFactory;
import com.estafet.costaexpress.model.entity.listener.IPolicyChangedListener;
import com.estafet.datasource.node.IDataSourceNode;
import com.estafet.datasource.query.QueryCriteria;

public abstract class AbstractEntityFactory<E extends IPolicyAwareEntity> implements IEntityFactory<E> {

	private final IEntityDataSource<? extends IDataSourceNode, ?, ? extends IDataSourceNode> dataSource;

	public AbstractEntityFactory(IEntityDataSource<? extends IDataSourceNode, ?, ? extends IDataSourceNode> dataSource) {
		this.dataSource = dataSource;

	}

	protected void find() throws Exception {

	}

	protected List<IPolicyAwareEntity> findSiblingEntities(IPolicyAwareEntity entity) throws EntityNotCreatedException {
		try {
			QueryCriteria criteria = new QueryCriteria();			
			criteria.setQueryForSiblingEntities(true);
			criteria.addCriteria(EntitySearchAttributesEnum.CODE.value(), entity.getId());
			criteria.addCriteria(EntitySearchAttributesEnum.TYPE.value(), entity.getType().value());
			return getDataSource().findSiblingEntities(criteria);
		} catch (com.estafet.datasource.DataSourceFailureException e) {
			throw new EntityNotCreatedException("Failed to create entity", e);
		}
	}

	@SuppressWarnings("unchecked")
	private IEntityDataSource<IPolicyAwareEntity, ?, ?> getDataSource() {
		return (IEntityDataSource<IPolicyAwareEntity, ?, ?>) dataSource;
	}

	protected IPolicyChangedListener getPolicyChangedListener() {
		return (IPolicyChangedListener) dataSource;
	}

}
