package com.estafet.costaexpress.model.entity.listener;

import com.estafet.costaexpress.model.entity.IPolicyAwareEntity;

public interface IPolicyChangedListener {
	void onPolicyChangedEvent(IPolicyAwareEntity entity);
	
}
