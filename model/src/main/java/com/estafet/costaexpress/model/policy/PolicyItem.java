package com.estafet.costaexpress.model.policy;

public class PolicyItem<I> {

	private final I item;

	public PolicyItem(I item) {
		this.item = item;
	}

	public I getItem() {
		return item;
	}
}
