package com.estafet.costaexpress.model.entity.impl;


import com.estafet.costaexpress.model.entity.IPolicyAwareEntity;
import com.estafet.costaexpress.model.entity.listener.PolicyChangedNotifier;
import com.estafet.costaexpress.model.entity.types.EntityTypesEnum;
import com.estafet.costaexpress.model.policy.IPolicy;

public abstract class AbstractPolicyAwareEntity implements IPolicyAwareEntity {

	private final String name;
	private final String id;
	private final EntityTypesEnum type;	
	private final String owner;
	
	private PolicyChangedNotifier policyChangedNotifier;
	
	

	public AbstractPolicyAwareEntity(String name, String id, EntityTypesEnum type) {
		this.name = name;
		this.type = type;
		this.id = id;
		this.owner = "core";
	}

	public String getName() {
		return name;
	}

	public EntityTypesEnum getType() {
		return type;
	}

	@Override
	public void applyPolicy(IPolicy policy) {
		if (isApplicable(policy)) {
			apply(policy);
			
			if (policyChangedNotificationsEnabled()) {
				policyChangedNotifier.notifyPolicyChangedEvent(this);
			}
		}
	}

	@Override
	public boolean isComposite() {
		return false;
	}
	
	@Override
	public String getNodeName() {
		return name;
	}
	
	@Override
	public String getNodeType() {
		return type.value();
	}

	protected abstract void apply(IPolicy policy);

	protected boolean isApplicable(IPolicy policy) {
		return true;
	};
	
	public void enablePolicyChangedNotifications(PolicyChangedNotifier policyChangedNotifier) {
		this.policyChangedNotifier = policyChangedNotifier;
	}
	
	public void disablePolicyChangedNotifications() {
		policyChangedNotifier = null;
	}
	
	protected boolean policyChangedNotificationsEnabled() {
		return policyChangedNotifier != null;
	}

	public String getId() {
		return id;
	}

	public String getOwner() {
		return owner;
	}
	

}
