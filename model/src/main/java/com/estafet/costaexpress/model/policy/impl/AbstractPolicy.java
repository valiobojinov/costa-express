package com.estafet.costaexpress.model.policy.impl;

import com.estafet.costaexpress.model.policy.IPolicy;

public abstract class AbstractPolicy implements IPolicy {

	private final String name;
	private final String id;

	protected AbstractPolicy(String name, String id) {
		
		this.name = name;
		this.id = id;
	}
	
	@Override
	public String getNodeName() {
		return name;
	}
	
	@Override
	public String getId() {
		return id;
	}


}
