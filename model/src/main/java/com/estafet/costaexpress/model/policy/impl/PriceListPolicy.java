package com.estafet.costaexpress.model.policy.impl;

import com.estafet.costaexpress.model.generated.pricelist.PriceTable;
import com.estafet.costaexpress.model.policy.IPolicyRules;
import com.estafet.costaexpress.model.policy.PolicyItem;
import com.estafet.costaexpress.model.policy.types.PolicyTypesEnum;

public class PriceListPolicy extends AbstractPolicy {

	private final PolicyItem<PriceTable> policyItem;
	
	public PriceListPolicy(String name, String identifier, PriceTable priceTable) {
		super(name, identifier);
		policyItem = new PolicyItem<PriceTable>(priceTable);
	}
	
	@Override
	public IPolicyRules getRules() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public PolicyItem<PriceTable> getPolicyItem() {
		return policyItem;
	}

	@Override
	public PolicyTypesEnum getPolicyType() {
		return PolicyTypesEnum.PRICELIST;
	}


	@Override
	public String getNodeType() {
		return PolicyTypesEnum.PRICELIST.value();
	}

	
	
	

	

}
