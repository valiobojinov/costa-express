package com.estafet.costaexpress.model.entity.impl;

import com.estafet.costaexpress.model.entity.types.EntityTypesEnum;

public class SiteEntity extends AbstractCompositeEntity {

	public SiteEntity(String name, String id) {
		super(name, id, EntityTypesEnum.SITE);

	}

	
}
