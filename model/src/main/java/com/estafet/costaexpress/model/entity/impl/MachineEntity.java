package com.estafet.costaexpress.model.entity.impl;

import com.estafet.costaexpress.model.entity.types.EntityTypesEnum;
import com.estafet.costaexpress.model.policy.IPolicy;
import com.estafet.costaexpress.model.policy.impl.PriceListPolicy;
import com.estafet.costaexpress.model.policy.types.PolicyTypesEnum;
import com.estafet.costaexpress.model.generated.pricelist.PriceTable;

public class MachineEntity extends AbstractPolicyAwareEntity {
	
	private PriceTable priceTable;	

	public MachineEntity(String name, String id) {		
		super(name, id, EntityTypesEnum.MACHINE);
		
	}
	
	@Override
	public boolean isComposite() {
		return false;
	}

	@Override
	protected void apply(IPolicy policy) {	
		if (isApplicable(policy)) {
			applyPriceListPolicy((PriceListPolicy)policy);
		}
	}
	
	private void applyPriceListPolicy(PriceListPolicy policy) {
		priceTable = policy.getPolicyItem().getItem();		
	}

	@Override
	protected boolean isApplicable(IPolicy policy) {
		return PolicyTypesEnum.PRICELIST.equals(policy.getPolicyType());
	}
	
	public PriceTable getPriceTable() {
		return priceTable;
	}

	
	


	

	

	
	
	
	

	

	
}
