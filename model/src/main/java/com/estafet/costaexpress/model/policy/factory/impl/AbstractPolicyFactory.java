package com.estafet.costaexpress.model.policy.factory.impl;

import com.estafet.costaexpress.datasource.policy.IPolicyDataSource;
import com.estafet.costaexpress.model.policy.IPolicy;
import com.estafet.costaexpress.model.policy.factory.IPolicyFactory;

public abstract class AbstractPolicyFactory<P extends IPolicy, I> implements IPolicyFactory<P, I> {
	
	private final IPolicyDataSource<? extends IPolicy, ?, ?> dataSource;
	
	protected AbstractPolicyFactory(IPolicyDataSource<? extends IPolicy, ?, ?> dataSource) {
		this.dataSource = dataSource;
	}

	@SuppressWarnings("unchecked")
	protected IPolicyDataSource<IPolicy, ?, ?> getDataSource() {
		return  (IPolicyDataSource<IPolicy, ?, ?>)dataSource;
	}

}
