package com.estafet.costaexpress.model.entity.types;

import java.util.Optional;

public enum EntityTypesEnum {		
	
	GROUP ("group"),
	SITE ("site"),
	MACHINE ("machine");
	
	private final String value;
	
	EntityTypesEnum(String value) {
		this.value = value;
	};
	
	public String value() {
		return value;
	}
	
	public static Optional<EntityTypesEnum> fromValue(String value) {
		for (EntityTypesEnum type : EntityTypesEnum.values()) {
			if (type.value().equals(value)) {
				return Optional.of(type);
			}
		}
		return Optional.empty();
	}
	
	
	
}
