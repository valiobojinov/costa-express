package com.estafet.costaexpress.model.entity.factory;

import com.estafet.costaexpress.datasource.entity.EntityNotCreatedException;
import com.estafet.costaexpress.model.entity.IEntity;

public interface IEntityFactory<E extends IEntity> {
	E createEntity(String name, String id) throws EntityNotCreatedException;
	E createFromDataSource(String name, String id) throws EntityNotCreatedException;
}
