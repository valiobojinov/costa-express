package com.estafet.costaexpress.model.policy.factory;

import java.text.MessageFormat;
import java.util.HashMap;
import java.util.Map;

import com.estafet.costaexpress.datasource.policy.IPolicyDataSource;
import com.estafet.costaexpress.model.policy.IPolicy;
import com.estafet.costaexpress.model.policy.factory.impl.PriceListPolicyFactory;
import com.estafet.costaexpress.model.policy.types.PolicyTypesEnum;
import com.estafet.costaexpress.model.relation.OneToOneRelation;

public class PolicyFactoryProvider {
	Map<PolicyTypesEnum, IPolicyFactory<? extends IPolicy, ?>> factories = new HashMap<PolicyTypesEnum, IPolicyFactory<? extends IPolicy, ?>>();

	public PolicyFactoryProvider(IPolicyDataSource<IPolicy, OneToOneRelation, ?> dataSource) {
		registerFactory(PolicyTypesEnum.PRICELIST, new PriceListPolicyFactory(dataSource));
		
	}
	
	@SuppressWarnings("unchecked")
	public <P extends IPolicy> IPolicyFactory<P, ?> getFactory(PolicyTypesEnum type) {
		if (factories.containsKey(type)) {
			return (IPolicyFactory<P, ?>) factories.get(type);
		}

		throw new IllegalArgumentException(
				MessageFormat.format("Factory for type: {0} is not available", type.value()));
	}
	
	public <P extends IPolicy> void registerFactory(PolicyTypesEnum type, IPolicyFactory<P, ?> factory) {
		factories.putIfAbsent(type, factory);
	}
	
	public boolean unregisterFactory(PolicyTypesEnum type) {
		return factories.remove(type) != null;
	}	
	

}
