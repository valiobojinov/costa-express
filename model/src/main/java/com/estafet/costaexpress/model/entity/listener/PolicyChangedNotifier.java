package com.estafet.costaexpress.model.entity.listener;


import com.estafet.costaexpress.model.entity.IPolicyAwareEntity;

public class PolicyChangedNotifier {
	public PolicyChangedNotifier(IPolicyChangedListener policyChangedListener) {
		this.policyChangedListener = policyChangedListener;
	}
	private final IPolicyChangedListener policyChangedListener;
	
	public void notifyPolicyChangedEvent(IPolicyAwareEntity entity) {
		if (notifyChanges()) {
			policyChangedListener.onPolicyChangedEvent(entity);
		}
	}
	
	private boolean notifyChanges() {
		return policyChangedListener != null;
	}
}
