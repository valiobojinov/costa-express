package com.estafet.costaexpress.model.relation;

import com.estafet.costaexpress.model.entity.IPolicyAwareEntity;
import com.estafet.costaexpress.model.policy.IPolicy;
public class OneToOneRelation {
	
	private final IPolicyAwareEntity source;
	private final IPolicy target;

	public OneToOneRelation(IPolicyAwareEntity source, IPolicy target) {
		this.source = source;
		this.target = target;
	}
	
	public IPolicyAwareEntity getSource() {
		return source;
	}
	
	public IPolicy getTarget() {
		return target;
	}
}
