package com.estafet.costaexpress.model.policy;

import com.estafet.costaexpress.model.policy.types.PolicyTypesEnum;
import com.estafet.datasource.node.IDataSourceNode;

public interface IPolicy extends IDataSourceNode{
	IPolicyRules getRules();
	PolicyItem<?> getPolicyItem();
	PolicyTypesEnum getPolicyType();
	
}
