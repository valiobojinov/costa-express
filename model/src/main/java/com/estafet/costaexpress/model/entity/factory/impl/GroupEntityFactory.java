package com.estafet.costaexpress.model.entity.factory.impl;

import java.util.List;

import com.estafet.costaexpress.datasource.entity.EntityNotCreatedException;
import com.estafet.costaexpress.datasource.entity.IEntityDataSource;
import com.estafet.costaexpress.model.entity.IPolicyAwareEntity;
import com.estafet.costaexpress.model.entity.impl.GroupEntity;

public class GroupEntityFactory extends AbstractEntityFactory<GroupEntity> {

	public GroupEntityFactory(IEntityDataSource<IPolicyAwareEntity, ?, ?> dataSource) {
		super(dataSource);
	}

	public GroupEntity createEntity(String name, String id) throws EntityNotCreatedException {
		return new GroupEntity(name, id);

	}

	@Override
	public GroupEntity createFromDataSource(String name, String id) throws EntityNotCreatedException {
		GroupEntity group = createEntity(name, id);

		List<IPolicyAwareEntity> siblingEntities = findSiblingEntities(group);
		group.getSiblingEntities().addAll(siblingEntities);

		return group;
	}

}
