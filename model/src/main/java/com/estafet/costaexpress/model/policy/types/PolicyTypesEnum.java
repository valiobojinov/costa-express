package com.estafet.costaexpress.model.policy.types;

import java.util.Optional;

public enum PolicyTypesEnum {
	PRICELIST("pricelist"),
	RECIPIE("recipie");
	
	private final String value;
	
	private PolicyTypesEnum(String value) {
		this.value = value;
	} 
	
	public String value() {
		return value;
	}
	
	public static Optional<PolicyTypesEnum> fromValue(String value) {
		for (PolicyTypesEnum type : PolicyTypesEnum.values()) {
			if (type.value().equals(value)) {
				return Optional.of(type);
			}
		}
		return Optional.empty();
	}
}
