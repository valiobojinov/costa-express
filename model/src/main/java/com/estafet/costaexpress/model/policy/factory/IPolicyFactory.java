package com.estafet.costaexpress.model.policy.factory;


import com.estafet.costaexpress.datasource.policy.PolicyNotCreatedException;
import com.estafet.costaexpress.model.policy.IPolicy;
import com.estafet.costaexpress.model.policy.PolicyItem;

public interface IPolicyFactory<P extends IPolicy, I> {	
	P createPolicy(String name, String id, PolicyItem<I> item) throws PolicyNotCreatedException;
	P createFromDataSource(String name, String id) throws PolicyNotCreatedException;
}
