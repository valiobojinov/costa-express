package com.estafet.costaexpress.model.entity;

import com.estafet.costaexpress.model.entity.types.EntityTypesEnum;
import com.estafet.datasource.node.IDataSourceNode;

public interface IEntity extends IDataSourceNode {
	EntityTypesEnum getType();
	String getName();	
	String getId();
	String getOwner();
	boolean isComposite();
}
