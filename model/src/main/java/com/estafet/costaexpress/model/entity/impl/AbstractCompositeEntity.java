package com.estafet.costaexpress.model.entity.impl;

import java.util.ArrayList;
import java.util.List;

import com.estafet.costaexpress.model.entity.IPolicyAwareEntity;
import com.estafet.costaexpress.model.entity.types.EntityTypesEnum;
import com.estafet.costaexpress.model.policy.IPolicy;

public abstract class AbstractCompositeEntity extends AbstractPolicyAwareEntity {

	private final List<IPolicyAwareEntity> siblingEntities;

	public AbstractCompositeEntity(String name, String id, EntityTypesEnum type) {
		super(name, id, type);

		siblingEntities = new ArrayList<IPolicyAwareEntity>();
	}

	public boolean isComposite() {
		return true;
	}

	@Override
	protected boolean isApplicable(IPolicy policy) {

		return true;
	}

	public List<IPolicyAwareEntity> getSiblingEntities() {
		return siblingEntities;
	}
	
	@Override
	protected void apply(IPolicy policy) {
		for (IPolicyAwareEntity entity : getSiblingEntities()) {
			entity.applyPolicy(policy);
		}
		
	}
}
